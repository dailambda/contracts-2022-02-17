#!/bin/bash
set -e

# Disable the disclaimer message of tezos-node
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y 

# Where am I?
SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)")"

# Optional: tezos-client
TEZOS_CLIENT=`which tezos-client || true`
if [ -n "$TEZOS_CLIENT" ]; then
    export TEZOS_CLIENT="$TEZOS_CLIENT --protocol PtHangz2aRng --mode mockup --base-dir _mock"

# Prepare mockup
    if [ ! -d _mock ]; then
	echo Preparing mockup  $TEZOS_CLIENT
	$TEZOS_CLIENT create mockup --protocol-constants protocol-constants.json
	$TEZOS_CLIENT config init
    fi
else
    echo no tezos-client in PATH
    exit 1
fi

for i in $*
do
    echo $i
    if $TEZOS_CLIENT typecheck script $i -G 100000; then
	echo $i: ok
    else
	echo $i: error
	exit 1
    fi
done

