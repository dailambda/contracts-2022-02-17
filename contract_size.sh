#!/bin/bash

set -e

for i in uniq_contracts/*tz
do
    (cd ../scaml; eval `opam env`; dune exec tools/contract_size/contract_size.exe ../new-contracts/$i)
done

