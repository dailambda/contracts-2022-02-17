for i in *.tz
do
    sz1=`stat -f %z $i`
    sz2=`stat -f %z optimized/$i`
    echo -n $i,$sz1,$sz2,
    a1=`grep 'Gas remaining' optimized/tycheck-$i | sed -e 's/Gas remaining: //' -e 's/ units remaining//'`
    echo $a1 | sed -e 's/ /,/'
done
