#!/bin/sh
set -e

TIMEOUT=60s

(cd ../scaml; dune build)

if [ ! -d optimized ]; then mkdir optimized; fi

for i in $*
do
    if [ ! -f optimized/$i ]; then
	echo $i
	(cd ../scaml; OPTZ_DEBUG=y timeout $TIMEOUT time dune exec tools/optz/optz.exe ../new-contracts/$i > ../new-contracts/optimized/$i.tmp) 2>&1 | tee optimized/log-$i
	STATUS=${PIPESTATUS[0]}
	if
	    [ $STATUS = "0" ]
	then
	    echo Optimized
	    if ./tycheck.sh $i > optimized/tycheck-$i; then
		echo Original typechecked
	    else
		echo Original typecheck failed
		exit 1
	    fi
	    if ./tycheck.sh optimized/$i.tmp >> optimized/tycheck-$i; then
		echo Typechecked
    		mv optimized/$i.tmp optimized/$i
	    else
		echo Typecheck failed
		exit 1
	    fi
	else
	    if [ $STATUS = "124" ]; then
		echo Optimization timeout
		rm -f optimized/$i optimized/$i.tmp
	    else
		exit 1
	    fi
	fi
    fi
done
