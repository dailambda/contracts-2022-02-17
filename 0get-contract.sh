#!/bin/sh

set -e

for kt in `cat new-kt1-list-2022-02-17`
do
    if [ ! -f contracts/$kt.tz ]; then
        echo $kt
        tezos-client rpc get /chains/main/blocks/head/context/contracts/$kt/script | jq .code > contracts/$kt.tz.tmp
        tezos-client convert script contracts/$kt.tz.tmp from json to michelson > contracts/$kt.tz.tmp2
        mv contracts/$kt.tz.tmp2 contracts/$kt.tz
        rm contracts/$kt.tz.tmp
        echo $kt done
    fi
done




