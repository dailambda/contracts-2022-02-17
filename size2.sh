#!/bin/bash

set -e

for i in uniq_contracts/*tz
do
    (cd ../scaml; eval `opan env`; dune exec tools/contract-size/size2.exe ../new-contracts/$i)
done

