#!/bin/sh

for i in `seq 0 133017`
do
    if [ ! -f big_maps/$i ]; then
        echo $i
        $HOME/tezos-mainnet-full/tezos-client rpc get /chains/main/blocks/head/context/big_maps/$i > big_maps/$i.tmp
        mv big_maps/$i.tmp big_maps/$i
    fi
done
