{ parameter
    (or (or (pair %create_ref (nat %min_unlock_sec) (pair (string %short) (string %long)))
            (nat %escape))
        (or (nat %unvote)
            (pair %vote
               (nat %ref_id)
               (pair (option %choice_id nat)
                     (pair (option %currency_id (pair (address %fa2_address) (option %token_id nat)))
                           (pair (nat %unlock_sec) (nat %amt))))))) ;
  storage
    (pair (pair %admin (address %admin) (option %pending_admin address))
          (pair (big_map %referendums
                   nat
                   (pair (nat %min_unlock_sec) (pair (string %short) (string %long))))
                (pair (nat %next_ref_id)
                      (pair (nat %next_nonce_id)
                            (pair (big_map %vote_ledger
                                     nat
                                     (pair (address %user)
                                           (pair (nat %ref_id)
                                                 (pair (option %choice_id nat)
                                                       (pair (option %currency_id (pair (address %fa2_address) (option %token_id nat)))
                                                             (pair (nat %nonce)
                                                                   (pair (nat %staked)
                                                                         (pair (nat %multiplier) (pair (nat %total_voting_power) (timestamp %unlock_time))))))))))
                                  (pair (pair %vote_currency (address %fa2_address) (nat %token_id))
                                        (big_map %metadata string bytes))))))) ;
  code { LAMBDA
           (pair address (option address))
           unit
           { CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair address address) (pair nat (pair address nat)))
           operation
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             NIL (pair nat nat) ;
             DIG 3 ;
             CDR ;
             DIG 3 ;
             SWAP ;
             PAIR ;
             CONS ;
             SWAP ;
             PAIR ;
             DUP ;
             CDR ;
             MAP { DUP 4 ;
                   SWAP ;
                   DUP ;
                   DUG 2 ;
                   CAR ;
                   DIG 2 ;
                   CDR ;
                   SWAP ;
                   PAIR ;
                   SWAP ;
                   PAIR } ;
             DIG 3 ;
             DROP ;
             SWAP ;
             CAR ;
             CONTRACT %transfer
               (list (pair (address %from_)
                           (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
             IF_NONE { PUSH string "Invalid FA2 Address" ; FAILWITH } {} ;
             PUSH mutez 0 ;
             NIL (pair address (list (pair address (pair nat nat)))) ;
             DIG 4 ;
             DIG 4 ;
             SWAP ;
             PAIR ;
             CONS ;
             TRANSFER_TOKENS } ;
         LAMBDA
           (pair bool string)
           unit
           { UNPAIR ; NOT ; IF { FAILWITH } { DROP ; UNIT } } ;
         DIG 3 ;
         UNPAIR ;
         SELF_ADDRESS ;
         SWAP ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DIG 4 ;
                 DROP 2 ;
                 PUSH string "Amount must be 0mutez" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 EQ ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUP ;
                 DUG 3 ;
                 GET 3 ;
                 DIG 2 ;
                 SOME ;
                 DUP 4 ;
                 GET 5 ;
                 UPDATE ;
                 UPDATE 3 ;
                 PUSH nat 1 ;
                 DIG 2 ;
                 GET 5 ;
                 ADD ;
                 UPDATE 5 ;
                 NIL operation ;
                 PAIR }
               { SWAP ;
                 DUG 2 ;
                 PUSH string "Amount must be 0mutez" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 EQ ;
                 PAIR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 9 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH string "NO_VOTE" ; FAILWITH } {} ;
                 DUP 3 ;
                 GET 11 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 11 ;
                 PAIR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DIG 5 ;
                 PAIR ;
                 PAIR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DIG 3 ;
                 DUP ;
                 GET 9 ;
                 DIG 3 ;
                 PUSH nat 0 ;
                 UPDATE 11 ;
                 PUSH nat 0 ;
                 UPDATE 13 ;
                 PUSH nat 0 ;
                 UPDATE 15 ;
                 SOME ;
                 DIG 4 ;
                 UPDATE ;
                 UPDATE 9 ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR } }
           { DIG 5 ;
             DROP ;
             IF_LEFT
               { SWAP ;
                 DUG 2 ;
                 PUSH string "Amount must be 0mutez" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 EQ ;
                 PAIR ;
                 DUP 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 9 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH string "NO_VOTE" ; FAILWITH } {} ;
                 PUSH string "WRONG_USER" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 PAIR ;
                 DUP 6 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH string "VOTES_LOCKED" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 16 ;
                 NOW ;
                 COMPARE ;
                 GT ;
                 PAIR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP 3 ;
                 GET 11 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 11 ;
                 PAIR ;
                 SENDER ;
                 DIG 5 ;
                 PAIR ;
                 PAIR ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DIG 3 ;
                 DUP ;
                 GET 9 ;
                 DIG 3 ;
                 PUSH nat 0 ;
                 UPDATE 11 ;
                 PUSH nat 0 ;
                 UPDATE 13 ;
                 PUSH nat 0 ;
                 UPDATE 15 ;
                 SOME ;
                 DIG 4 ;
                 UPDATE ;
                 UPDATE 9 ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR }
               { SWAP ;
                 DUG 2 ;
                 PUSH string "AMOUNT_TOO_LOW" ;
                 PUSH nat 0 ;
                 DUP 3 ;
                 GET 8 ;
                 COMPARE ;
                 GT ;
                 PAIR ;
                 DUP 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH string "Amount must be 0mutez" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 EQ ;
                 PAIR ;
                 DUP 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SWAP ;
                 GET 3 ;
                 SWAP ;
                 GET ;
                 IF_NONE { PUSH string "INVALID_REF" ; FAILWITH } {} ;
                 PUSH string "LOCK_TIME_TOO_LOW" ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DUP 4 ;
                 GET 7 ;
                 COMPARE ;
                 GE ;
                 PAIR ;
                 DUP 6 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH string "LOCK_TIME_TOO_HIGH" ;
                 PUSH nat 6 ;
                 DUP 3 ;
                 CAR ;
                 MUL ;
                 DUP 4 ;
                 GET 7 ;
                 COMPARE ;
                 LE ;
                 PAIR ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH nat 1000000 ;
                 DUP 3 ;
                 GET 8 ;
                 DIG 2 ;
                 CAR ;
                 DUP 3 ;
                 DUP 5 ;
                 GET 7 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 PUSH int 0 ;
                 DUP 4 ;
                 DUP 3 ;
                 SUB ;
                 COMPARE ;
                 GE ;
                 IF { DUP 3 ; SWAP ; SUB ; ABS ; PUSH nat 20 ; MUL }
                    { DROP ; PUSH string "fatal_ratio" ; FAILWITH } ;
                 DUP 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DIG 3 ;
                 PUSH nat 100 ;
                 DIG 3 ;
                 DUP 5 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP 3 ;
                 ADD ;
                 SENDER ;
                 DUP 5 ;
                 CAR ;
                 DUP 6 ;
                 GET 3 ;
                 DUP 7 ;
                 GET 5 ;
                 DUP 9 ;
                 GET 7 ;
                 DIG 7 ;
                 DIG 7 ;
                 DIG 7 ;
                 DUP 9 ;
                 GET 7 ;
                 INT ;
                 NOW ;
                 ADD ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 DUP 3 ;
                 GET 11 ;
                 DIG 2 ;
                 GET 8 ;
                 PAIR ;
                 DIG 3 ;
                 SENDER ;
                 PAIR ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DIG 2 ;
                 DUP ;
                 DUP ;
                 DUG 4 ;
                 GET 9 ;
                 DIG 3 ;
                 SOME ;
                 DUP 5 ;
                 GET 7 ;
                 UPDATE ;
                 UPDATE 9 ;
                 PUSH nat 1 ;
                 DIG 3 ;
                 GET 7 ;
                 ADD ;
                 UPDATE 7 ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR } } } }
