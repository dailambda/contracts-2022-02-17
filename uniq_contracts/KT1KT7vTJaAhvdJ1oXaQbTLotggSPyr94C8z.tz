{ parameter
    (or (or (or (unit %deposit) (unit %harvest))
            (or (pair %rescue address nat) (address %updateAdmin)))
        (or (pair %updateIfo
               (pair %offeringToken
                  (address %address)
                  (pair (nat %tokenId) (or %tokenType (unit %fa1) (unit %fa2))))
               (pair (mutez %raisingGoal)
                     (pair (nat %offeringSupply)
                           (pair (mutez %minAmount)
                                 (pair (mutez %maxAmount)
                                       (pair (timestamp %startTime)
                                             (pair (timestamp %endTime) (pair (timestamp %harvestTime) (int %harvestDuration)))))))))
            (address %withdraw))) ;
  storage
    (pair (big_map %metadata string bytes)
          (pair (pair %ifo
                   (pair %offeringToken
                      (address %address)
                      (pair (nat %tokenId) (or %tokenType (unit %fa1) (unit %fa2))))
                   (pair (mutez %raisingGoal)
                         (pair (mutez %totalRaised)
                               (pair (nat %offeringSupply)
                                     (pair (mutez %minAmount)
                                           (pair (mutez %maxAmount)
                                                 (pair (timestamp %startTime)
                                                       (pair (timestamp %endTime) (pair (timestamp %harvestTime) (int %harvestDuration))))))))))
                (pair (big_map %ledger
                         address
                         (pair (mutez %amount) (pair (mutez %amountHarvested) (timestamp %lastHarvest))))
                      (address %admin)))) ;
  code { LAMBDA
           (pair (pair address address) (pair (pair address (pair nat (or unit unit))) nat))
           operation
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DUP ;
             GET 4 ;
             IF_LEFT
               { DROP ;
                 CAR ;
                 CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                 IF_NONE { PUSH string "CANNOT_INVOKE_FA1_TRANSFER" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 4 ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 TRANSFER_TOKENS }
               { DROP ;
                 DUP ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE { PUSH string "CANNOT_INVOKE_FA2_TRANSFER" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 DIG 5 ;
                 NIL (pair address (pair nat nat)) ;
                 DIG 7 ;
                 DIG 6 ;
                 GET 3 ;
                 DIG 7 ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR ;
                 CONS ;
                 SWAP ;
                 PAIR ;
                 CONS ;
                 TRANSFER_TOKENS } } ;
         LAMBDA
           mutez
           nat
           { PUSH mutez 1 ;
             SWAP ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         LAMBDA
           (pair address mutez)
           operation
           { UNPAIR ;
             CONTRACT unit ;
             IF_NONE { PUSH string "INVALID_TEZ_DESTINATION" ; FAILWITH } {} ;
             SWAP ;
             UNIT ;
             TRANSFER_TOKENS } ;
         NIL operation ;
         PUSH nat 1000000000000000000 ;
         LAMBDA
           (pair (big_map string bytes)
                 (pair (pair (pair address (pair nat (or unit unit)))
                             (pair mutez
                                   (pair mutez
                                         (pair nat (pair mutez (pair mutez (pair timestamp (pair timestamp (pair timestamp int)))))))))
                       (pair (big_map address (pair mutez (pair mutez timestamp))) address)))
           unit
           { GET 6 ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "INVALID_ACCESS" ; FAILWITH } { PUSH unit Unit } } ;
         LAMBDA
           (pair address
                 (pair (big_map string bytes)
                       (pair (pair (pair address (pair nat (or unit unit)))
                                   (pair mutez
                                         (pair mutez
                                               (pair nat (pair mutez (pair mutez (pair timestamp (pair timestamp (pair timestamp int)))))))))
                             (pair (big_map address (pair mutez (pair mutez timestamp))) address))))
           (pair mutez (pair mutez timestamp))
           { UNPAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 5 ;
             SWAP ;
             GET ;
             IF_NONE
               { PUSH mutez 0 ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 GET 3 ;
                 GET 17 ;
                 SWAP ;
                 PAIR ;
                 SWAP ;
                 PAIR }
               { SWAP ; DROP } } ;
         DUP 6 ;
         DUP 4 ;
         PAIR ;
         LAMBDA
           (pair (pair nat (lambda mutez nat))
                 (pair mutez
                       (pair (big_map string bytes)
                             (pair (pair (pair address (pair nat (or unit unit)))
                                         (pair mutez
                                               (pair mutez
                                                     (pair nat (pair mutez (pair mutez (pair timestamp (pair timestamp (pair timestamp int)))))))))
                                   (pair (big_map address (pair mutez (pair mutez timestamp))) address)))))
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 3 ;
             GET 3 ;
             DIG 2 ;
             GET 3 ;
             GET 5 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             COMPARE ;
             GT ;
             IF { SWAP ; DROP } { DROP } ;
             DUP 4 ;
             SWAP ;
             EXEC ;
             DUG 2 ;
             DIG 3 ;
             SWAP ;
             EXEC ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         SWAP ;
         APPLY ;
         DUP 4 ;
         SWAP ;
         DUP ;
         DUG 2 ;
         PAIR ;
         LAMBDA
           (pair (pair (lambda
                          (pair mutez
                                (pair (big_map string bytes)
                                      (pair (pair (pair address (pair nat (or unit unit)))
                                                  (pair mutez
                                                        (pair mutez
                                                              (pair nat (pair mutez (pair mutez (pair timestamp (pair timestamp (pair timestamp int)))))))))
                                            (pair (big_map address (pair mutez (pair mutez timestamp))) address))))
                          nat)
                       nat)
                 (pair mutez
                       (pair (big_map string bytes)
                             (pair (pair (pair address (pair nat (or unit unit)))
                                         (pair mutez
                                               (pair mutez
                                                     (pair nat (pair mutez (pair mutez (pair timestamp (pair timestamp (pair timestamp int)))))))))
                                   (pair (big_map address (pair mutez (pair mutez timestamp))) address)))))
           nat
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             PAIR ;
             DIG 2 ;
             SWAP ;
             EXEC ;
             DUG 2 ;
             GET 3 ;
             GET 7 ;
             DIG 2 ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR } ;
         SWAP ;
         APPLY ;
         DIG 9 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 5 ;
                 DROP ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 5 ;
                     DIG 7 ;
                     DIG 8 ;
                     DIG 9 ;
                     DROP 7 ;
                     DUP ;
                     GET 3 ;
                     GET 13 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "IFO_NOT_STARTED" ; FAILWITH } {} ;
                     DUP ;
                     GET 3 ;
                     GET 15 ;
                     NOW ;
                     COMPARE ;
                     GT ;
                     IF { PUSH string "IFO_ENDED" ; FAILWITH } {} ;
                     PUSH mutez 1 ;
                     AMOUNT ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "INVALID_AMOUNT" ; FAILWITH } {} ;
                     DUP ;
                     SENDER ;
                     PAIR ;
                     DIG 2 ;
                     SWAP ;
                     EXEC ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 9 ;
                     AMOUNT ;
                     DUP 3 ;
                     CAR ;
                     ADD ;
                     COMPARE ;
                     LT ;
                     PUSH mutez 1 ;
                     DUP 4 ;
                     GET 3 ;
                     GET 9 ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "MIN_AMOUNT_NOT_MET" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 11 ;
                     AMOUNT ;
                     DUP 3 ;
                     CAR ;
                     ADD ;
                     COMPARE ;
                     GT ;
                     PUSH mutez 1 ;
                     DUP 4 ;
                     GET 3 ;
                     GET 11 ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "MAX_AMOUNT_EXCEEDED" ; FAILWITH } {} ;
                     DUP ;
                     AMOUNT ;
                     DIG 2 ;
                     CAR ;
                     ADD ;
                     UPDATE 1 ;
                     SWAP ;
                     DUP ;
                     DUP ;
                     DUG 3 ;
                     GET 3 ;
                     AMOUNT ;
                     DIG 4 ;
                     GET 3 ;
                     GET 5 ;
                     ADD ;
                     UPDATE 5 ;
                     UPDATE 3 ;
                     DUP ;
                     GET 5 ;
                     DIG 2 ;
                     SOME ;
                     SENDER ;
                     UPDATE ;
                     UPDATE 5 ;
                     SWAP ;
                     PAIR }
                   { DIG 6 ;
                     DROP 2 ;
                     DUP ;
                     GET 3 ;
                     GET 17 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 15 ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     OR ;
                     IF { PUSH string "NOT_HARVEST_TIME" ; FAILWITH } {} ;
                     DUP ;
                     SENDER ;
                     PAIR ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     PUSH mutez 1 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "NOTHING_TO_HARVEST" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 18 ;
                     DUP 3 ;
                     GET 3 ;
                     GET 17 ;
                     ADD ;
                     NOW ;
                     COMPARE ;
                     LT ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 3 ;
                          GET 18 ;
                          ABS ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          EDIV ;
                          IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                          CAR ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 4 ;
                          NOW ;
                          SUB ;
                          ABS ;
                          SWAP ;
                          MUL }
                        { DUP ; CAR } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     DUP 4 ;
                     GET 3 ;
                     COMPARE ;
                     GT ;
                     IF { SWAP ; DROP ; SWAP ; DUP ; DUG 2 ; GET 3 } { SWAP } ;
                     SUB ;
                     PUSH mutez 1 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "NOTHING_TO_HARVEST" ; FAILWITH } {} ;
                     NIL operation ;
                     DUP 4 ;
                     DUP 3 ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     PUSH nat 0 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     GT ;
                     IF { DUP 5 ;
                          GET 3 ;
                          CAR ;
                          PAIR ;
                          SENDER ;
                          SELF_ADDRESS ;
                          PAIR ;
                          PAIR ;
                          DIG 9 ;
                          SWAP ;
                          EXEC ;
                          CONS }
                        { DIG 9 ; DROP 2 } ;
                     PUSH mutez 0 ;
                     DUP 4 ;
                     GET 3 ;
                     COMPARE ;
                     EQ ;
                     IF { DUP 4 ;
                          DUP 4 ;
                          CAR ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 3 ;
                          GET 3 ;
                          DUP 3 ;
                          GET 3 ;
                          GET 5 ;
                          COMPARE ;
                          GT ;
                          IF { SWAP ;
                               DUP ;
                               DUG 2 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               PAIR ;
                               DIG 7 ;
                               SWAP ;
                               EXEC ;
                               DIG 7 ;
                               DIG 3 ;
                               GET 3 ;
                               GET 3 ;
                               DIG 9 ;
                               SWAP ;
                               EXEC ;
                               DIG 2 ;
                               MUL ;
                               EDIV ;
                               IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                               CAR ;
                               PUSH mutez 1 ;
                               SWAP ;
                               MUL ;
                               SWAP ;
                               SUB }
                             { SWAP ; DIG 6 ; DIG 7 ; DIG 9 ; DROP 5 ; PUSH mutez 0 } ;
                          PUSH mutez 0 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          COMPARE ;
                          GT ;
                          IF { SENDER ; PAIR ; DIG 5 ; SWAP ; EXEC ; CONS } { DIG 5 ; DROP 2 } }
                        { DIG 4 ; DIG 5 ; DIG 6 ; DIG 7 ; DROP 4 } ;
                     DUP 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     GET 3 ;
                     ADD ;
                     UPDATE 3 ;
                     NOW ;
                     UPDATE 4 ;
                     DIG 2 ;
                     DUP ;
                     GET 5 ;
                     DIG 2 ;
                     SOME ;
                     SENDER ;
                     UPDATE ;
                     UPDATE 5 ;
                     SWAP ;
                     PAIR } }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 6 ;
                 DIG 9 ;
                 DROP 5 ;
                 IF_LEFT
                   { DIG 3 ;
                     DROP ;
                     UNPAIR ;
                     DUP 3 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     NIL operation ;
                     PUSH nat 0 ;
                     DUP 4 ;
                     COMPARE ;
                     GT ;
                     IF { DIG 2 ;
                          DUP 4 ;
                          GET 3 ;
                          CAR ;
                          PAIR ;
                          DUP 3 ;
                          SELF_ADDRESS ;
                          PAIR ;
                          PAIR ;
                          DIG 5 ;
                          SWAP ;
                          EXEC ;
                          CONS }
                        { DIG 2 ; DIG 5 ; DROP 2 } ;
                     BALANCE ;
                     DIG 2 ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     CONS ;
                     PAIR }
                   { DIG 4 ;
                     DIG 5 ;
                     DROP 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     UPDATE 6 ;
                     SWAP ;
                     PAIR } } }
           { DIG 3 ;
             DIG 4 ;
             DIG 6 ;
             DIG 9 ;
             DROP 4 ;
             IF_LEFT
               { DIG 2 ;
                 DIG 5 ;
                 DIG 6 ;
                 DROP 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 GET 3 ;
                 DUP 3 ;
                 CAR ;
                 UPDATE 1 ;
                 DUP 3 ;
                 GET 3 ;
                 UPDATE 3 ;
                 DUP 3 ;
                 GET 5 ;
                 UPDATE 7 ;
                 DUP 3 ;
                 GET 7 ;
                 UPDATE 9 ;
                 DUP 3 ;
                 GET 9 ;
                 UPDATE 11 ;
                 DUP 3 ;
                 GET 11 ;
                 UPDATE 13 ;
                 DUP 3 ;
                 GET 13 ;
                 UPDATE 15 ;
                 DUP 3 ;
                 GET 15 ;
                 UPDATE 17 ;
                 DIG 2 ;
                 GET 16 ;
                 UPDATE 18 ;
                 UPDATE 3 ;
                 SWAP ;
                 PAIR }
               { DIG 4 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 GET 15 ;
                 NOW ;
                 COMPARE ;
                 LT ;
                 IF { PUSH string "IFO_NOT_ENDED" ; FAILWITH } {} ;
                 NIL operation ;
                 DUP 3 ;
                 GET 3 ;
                 GET 3 ;
                 DUP 4 ;
                 GET 3 ;
                 GET 5 ;
                 COMPARE ;
                 LT ;
                 IF { DIG 2 ;
                      DUP ;
                      DUP ;
                      DUG 4 ;
                      GET 3 ;
                      GET 5 ;
                      PAIR ;
                      DIG 4 ;
                      SWAP ;
                      EXEC ;
                      DUP 4 ;
                      GET 3 ;
                      GET 7 ;
                      SUB ;
                      ABS ;
                      PUSH nat 0 ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      COMPARE ;
                      GT ;
                      IF { DUP 4 ;
                           GET 3 ;
                           CAR ;
                           PAIR ;
                           DUP 3 ;
                           SELF_ADDRESS ;
                           PAIR ;
                           PAIR ;
                           DIG 5 ;
                           SWAP ;
                           EXEC ;
                           CONS }
                         { DIG 5 ; DROP 2 } }
                    { DIG 3 ; DIG 5 ; DROP 2 } ;
                 DUP 3 ;
                 GET 3 ;
                 GET 3 ;
                 DUP 4 ;
                 GET 3 ;
                 GET 5 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 COMPARE ;
                 LT ;
                 IF { SWAP ; DROP } { DROP } ;
                 DIG 2 ;
                 PAIR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 CONS ;
                 PAIR } } } }
