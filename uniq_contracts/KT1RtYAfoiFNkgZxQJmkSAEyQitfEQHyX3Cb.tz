{ storage
    (pair (pair (nat %counter) (pair (nat %expiration_time) (big_map %metadata string bytes)))
          (pair (pair (nat %minimum_votes)
                      (big_map %proposals
                         nat
                         (pair (or %kind
                                  (or (or (unit %add_user) (unit %expiration_time))
                                      (or (unit %lambda_function) (unit %minimum_votes)))
                                  (or (or (unit %remove_user) (unit %text))
                                      (or (unit %transfer_mutez) (unit %transfer_token))))
                               (pair (bool %executed)
                                     (pair (address %issuer)
                                           (pair (timestamp %timestamp)
                                                 (pair (option %text bytes)
                                                       (pair (nat %positive_votes)
                                                             (pair (option %mutez_transfers (list (pair (mutez %amount) (address %destination))))
                                                                   (pair (option %token_transfers
                                                                            (pair (address %fa2)
                                                                                  (pair (nat %token_id) (list %distribution (pair (nat %amount) (address %destination))))))
                                                                         (pair (option %minimum_votes nat)
                                                                               (pair (option %expiration_time nat)
                                                                                     (pair (option %user address) (option %lambda_function (lambda unit (list operation))))))))))))))))
                (pair (set %users address) (big_map %votes (pair nat address) bool)))) ;
  parameter
    (or (or (or (address %add_user_proposal) (unit %default))
            (or (nat %execute_proposal)
                (or (nat %expiration_time_proposal)
                    (lambda %lambda_function_proposal unit (list operation)))))
        (or (or (nat %minimum_votes_proposal)
                (or (address %remove_user_proposal) (bytes %text_proposal)))
            (or (list %transfer_mutez_proposal (pair (mutez %amount) (address %destination)))
                (or (pair %transfer_token_proposal
                       (address %fa2)
                       (pair (nat %token_id) (list %distribution (pair (nat %amount) (address %destination)))))
                    (pair %vote_proposal (nat %proposal_id) (bool %approval)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SENDER ;
                     MEM ;
                     IF {}
                        { PUSH string "This can only be executed by one of the wallet users" ;
                          FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     MEM ;
                     IF { PUSH string "The proposed address is in the users list" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     NONE (lambda unit (list operation)) ;
                     DUP 6 ;
                     SOME ;
                     NONE nat ;
                     NONE nat ;
                     NONE (pair address (pair nat (list (pair nat address)))) ;
                     NONE (list (pair mutez address)) ;
                     PUSH nat 0 ;
                     NONE bytes ;
                     NOW ;
                     SENDER ;
                     PUSH bool False ;
                     PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                          (Left (Left (Left Unit))) ;
                     PAIR 12 ;
                     DIG 5 ;
                     DROP ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     PAIR }
                   { DROP } ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SENDER ;
                     MEM ;
                     IF {}
                        { PUSH string "This can only be executed by one of the wallet users" ;
                          FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     MEM ;
                     IF {} { PUSH string "The proposal doesn't exist" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                     GET 3 ;
                     IF { PUSH string "The proposal has been executed" ; FAILWITH } {} ;
                     PUSH int 60 ;
                     DUP ;
                     PUSH int 24 ;
                     DUP 5 ;
                     CAR ;
                     GET 3 ;
                     INT ;
                     MUL ;
                     MUL ;
                     MUL ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                     GET 7 ;
                     ADD ;
                     NOW ;
                     COMPARE ;
                     GT ;
                     IF { PUSH string "The proposal has expired" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     GET 11 ;
                     COMPARE ;
                     GE ;
                     IF {}
                        { PUSH string "The proposal didn't receive enough positive votes" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     DUP ;
                     DUP 6 ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 372 ; FAILWITH } {} ;
                     PUSH bool True ;
                     UPDATE 3 ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { DROP ; PUSH bool False }
                       { IF_LEFT
                           { DROP ; PUSH bool False }
                           { IF_LEFT { DROP ; PUSH bool True } { DROP ; PUSH bool False } } } ;
                     IF { NIL operation ;
                          DUP 3 ;
                          GET 3 ;
                          CDR ;
                          DUP 3 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 13 ;
                          IF_NONE { PUSH int 375 ; FAILWITH } {} ;
                          ITER { DUP ;
                                 DUG 2 ;
                                 CDR ;
                                 CONTRACT unit ;
                                 IF_NONE { PUSH int 376 ; FAILWITH } {} ;
                                 DIG 2 ;
                                 CAR ;
                                 UNIT ;
                                 TRANSFER_TOKENS ;
                                 CONS } }
                        { NIL operation } ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { DROP ; PUSH bool False }
                       { IF_LEFT
                           { DROP ; PUSH bool False }
                           { IF_LEFT { DROP ; PUSH bool False } { DROP ; PUSH bool True } } } ;
                     IF { NIL (pair address (pair nat nat)) ;
                          DUP 4 ;
                          GET 3 ;
                          CDR ;
                          DUP 4 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 15 ;
                          IF_NONE { PUSH int 380 ; FAILWITH } {} ;
                          GET 4 ;
                          ITER { DUP ;
                                 DUG 2 ;
                                 CAR ;
                                 DUP 6 ;
                                 GET 3 ;
                                 CDR ;
                                 DUP 6 ;
                                 GET ;
                                 IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                                 GET 15 ;
                                 IF_NONE { PUSH int 380 ; FAILWITH } {} ;
                                 GET 3 ;
                                 DIG 3 ;
                                 CDR ;
                                 PAIR 3 ;
                                 CONS } ;
                          SWAP ;
                          DUP 4 ;
                          GET 3 ;
                          CDR ;
                          DUP 4 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 15 ;
                          IF_NONE { PUSH int 380 ; FAILWITH } {} ;
                          CAR ;
                          CONTRACT %transfer
                            (list (pair (address %from_)
                                        (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                          IF_NONE { PUSH int 419 ; FAILWITH } {} ;
                          PUSH mutez 0 ;
                          NIL (pair address (list (pair address (pair nat nat)))) ;
                          DIG 4 ;
                          SELF_ADDRESS ;
                          PAIR ;
                          CONS ;
                          TRANSFER_TOKENS ;
                          CONS }
                        {} ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { IF_LEFT
                           { DROP ; PUSH bool False }
                           { IF_LEFT { DROP ; PUSH bool False } { DROP ; PUSH bool True } } }
                       { DROP ; PUSH bool False } ;
                     IF { NIL address ;
                          DUP 4 ;
                          GET 5 ;
                          ITER { CONS } ;
                          NIL address ;
                          SWAP ;
                          ITER { CONS } ;
                          SIZE ;
                          DUP 4 ;
                          GET 3 ;
                          CDR ;
                          DUP 4 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 17 ;
                          IF_NONE { PUSH int 391 ; FAILWITH } {} ;
                          COMPARE ;
                          LE ;
                          IF {}
                             { PUSH string
                                    "The minimum_votes parameter cannot be higher than the number of users" ;
                               FAILWITH } ;
                          DUP 3 ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          CDR ;
                          DIG 5 ;
                          GET 3 ;
                          CDR ;
                          DUP 6 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 17 ;
                          IF_NONE { PUSH int 391 ; FAILWITH } {} ;
                          PAIR ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          DUG 2 }
                        {} ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { IF_LEFT
                           { IF_LEFT { DROP ; PUSH bool False } { DROP ; PUSH bool True } }
                           { DROP ; PUSH bool False } }
                       { DROP ; PUSH bool False } ;
                     IF { DUP 3 ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          CDR ;
                          DIG 5 ;
                          GET 3 ;
                          CDR ;
                          DUP 6 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 19 ;
                          IF_NONE { PUSH int 396 ; FAILWITH } {} ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          DUG 2 }
                        {} ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { IF_LEFT
                           { IF_LEFT { DROP ; PUSH bool True } { DROP ; PUSH bool False } }
                           { DROP ; PUSH bool False } }
                       { DROP ; PUSH bool False } ;
                     IF { DUP 3 ;
                          DUP ;
                          GET 5 ;
                          PUSH bool True ;
                          DIG 5 ;
                          GET 3 ;
                          CDR ;
                          DUP 6 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 21 ;
                          IF_NONE { PUSH int 399 ; FAILWITH } {} ;
                          UPDATE ;
                          UPDATE 5 ;
                          DUG 2 }
                        {} ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { DROP ; PUSH bool False }
                       { IF_LEFT
                           { IF_LEFT { DROP ; PUSH bool True } { DROP ; PUSH bool False } }
                           { DROP ; PUSH bool False } } ;
                     IF { PUSH nat 1 ;
                          NIL address ;
                          DUP 5 ;
                          GET 5 ;
                          ITER { CONS } ;
                          NIL address ;
                          SWAP ;
                          ITER { CONS } ;
                          SIZE ;
                          COMPARE ;
                          GT ;
                          IF {} { PUSH string "The last user cannot be removed" ; FAILWITH } ;
                          DUP 3 ;
                          DUP ;
                          GET 5 ;
                          PUSH bool False ;
                          DIG 5 ;
                          GET 3 ;
                          CDR ;
                          DUP 6 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 21 ;
                          IF_NONE { PUSH int 399 ; FAILWITH } {} ;
                          UPDATE ;
                          UPDATE 5 ;
                          DUG 2 ;
                          NIL address ;
                          DUP 4 ;
                          GET 5 ;
                          ITER { CONS } ;
                          NIL address ;
                          SWAP ;
                          ITER { CONS } ;
                          SIZE ;
                          DUP 4 ;
                          GET 3 ;
                          CAR ;
                          COMPARE ;
                          GT ;
                          IF { DUP 3 ;
                               UNPAIR ;
                               SWAP ;
                               UNPAIR ;
                               CDR ;
                               NIL address ;
                               DIG 6 ;
                               GET 5 ;
                               ITER { CONS } ;
                               NIL address ;
                               SWAP ;
                               ITER { CONS } ;
                               SIZE ;
                               PAIR ;
                               PAIR ;
                               SWAP ;
                               PAIR ;
                               DUG 2 }
                             {} }
                        {} ;
                     DUP 3 ;
                     GET 3 ;
                     CDR ;
                     DUP 3 ;
                     GET ;
                     IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                     CAR ;
                     IF_LEFT
                       { IF_LEFT
                           { DROP ; PUSH bool False }
                           { IF_LEFT { DROP ; PUSH bool True } { DROP ; PUSH bool False } } }
                       { DROP ; PUSH bool False } ;
                     IF { DUP 3 ;
                          GET 3 ;
                          CDR ;
                          DIG 2 ;
                          GET ;
                          IF_NONE { PUSH int 367 ; FAILWITH } {} ;
                          GET 22 ;
                          IF_NONE { PUSH int 411 ; FAILWITH } {} ;
                          UNIT ;
                          EXEC ;
                          ITER { CONS } }
                        { SWAP ; DROP } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         DUP ;
                         PUSH nat 1 ;
                         SWAP ;
                         COMPARE ;
                         GE ;
                         IF {}
                            { PUSH string "The expiration_time parameter cannot be smaller than 1 day" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         NONE (lambda unit (list operation)) ;
                         NONE address ;
                         DUP 7 ;
                         SOME ;
                         NONE nat ;
                         NONE (pair address (pair nat (list (pair nat address)))) ;
                         NONE (list (pair mutez address)) ;
                         PUSH nat 0 ;
                         NONE bytes ;
                         NOW ;
                         SENDER ;
                         PUSH bool False ;
                         PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                              (Left (Left (Right Unit))) ;
                         PAIR 12 ;
                         DIG 5 ;
                         DROP ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         DUP 5 ;
                         SOME ;
                         NONE address ;
                         NONE nat ;
                         NONE nat ;
                         NONE (pair address (pair nat (list (pair nat address)))) ;
                         NONE (list (pair mutez address)) ;
                         PUSH nat 0 ;
                         NONE bytes ;
                         NOW ;
                         SENDER ;
                         PUSH bool False ;
                         PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                              (Left (Right (Left Unit))) ;
                         PAIR 12 ;
                         DIG 5 ;
                         DROP ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR } ;
                     NIL operation } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SENDER ;
                     MEM ;
                     IF {}
                        { PUSH string "This can only be executed by one of the wallet users" ;
                          FAILWITH } ;
                     DUP ;
                     PUSH nat 1 ;
                     SWAP ;
                     COMPARE ;
                     GE ;
                     IF {}
                        { PUSH string "The minimum_votes parameter cannot be smaller than 1" ;
                          FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     NONE (lambda unit (list operation)) ;
                     NONE address ;
                     NONE nat ;
                     DUP 8 ;
                     SOME ;
                     NONE (pair address (pair nat (list (pair nat address)))) ;
                     NONE (list (pair mutez address)) ;
                     PUSH nat 0 ;
                     NONE bytes ;
                     NOW ;
                     SENDER ;
                     PUSH bool False ;
                     PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                          (Left (Right (Right Unit))) ;
                     PAIR 12 ;
                     DIG 5 ;
                     DROP ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF {}
                            { PUSH string "The proposed address is not in the users list" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         NONE (lambda unit (list operation)) ;
                         DUP 6 ;
                         SOME ;
                         NONE nat ;
                         NONE nat ;
                         NONE (pair address (pair nat (list (pair nat address)))) ;
                         NONE (list (pair mutez address)) ;
                         PUSH nat 0 ;
                         NONE bytes ;
                         NOW ;
                         SENDER ;
                         PUSH bool False ;
                         PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                              (Right (Left (Left Unit))) ;
                         PAIR 12 ;
                         DIG 5 ;
                         DROP ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         NONE (lambda unit (list operation)) ;
                         NONE address ;
                         NONE nat ;
                         NONE nat ;
                         NONE (pair address (pair nat (list (pair nat address)))) ;
                         NONE (list (pair mutez address)) ;
                         PUSH nat 0 ;
                         DUP 12 ;
                         SOME ;
                         NOW ;
                         SENDER ;
                         PUSH bool False ;
                         PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                              (Right (Left (Right Unit))) ;
                         PAIR 12 ;
                         DIG 5 ;
                         DROP ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SENDER ;
                     MEM ;
                     IF {}
                        { PUSH string "This can only be executed by one of the wallet users" ;
                          FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     NONE (lambda unit (list operation)) ;
                     NONE address ;
                     NONE nat ;
                     NONE nat ;
                     NONE (pair address (pair nat (list (pair nat address)))) ;
                     DUP 10 ;
                     SOME ;
                     PUSH nat 0 ;
                     NONE bytes ;
                     NOW ;
                     SENDER ;
                     PUSH bool False ;
                     PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                          (Right (Right (Left Unit))) ;
                     PAIR 12 ;
                     DIG 5 ;
                     DROP ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH nat 1 ;
                     ADD ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         NONE (lambda unit (list operation)) ;
                         NONE address ;
                         NONE nat ;
                         NONE nat ;
                         DUP 9 ;
                         SOME ;
                         NONE (list (pair mutez address)) ;
                         PUSH nat 0 ;
                         NONE bytes ;
                         NOW ;
                         SENDER ;
                         PUSH bool False ;
                         PUSH (or (or (or unit unit) (or unit unit)) (or (or unit unit) (or unit unit)))
                              (Right (Right (Right Unit))) ;
                         PAIR 12 ;
                         DIG 5 ;
                         DROP ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         UNPAIR ;
                         PUSH nat 1 ;
                         ADD ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SENDER ;
                         MEM ;
                         IF {}
                            { PUSH string "This can only be executed by one of the wallet users" ;
                              FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         MEM ;
                         IF {} { PUSH string "The proposal doesn't exist" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                         GET 3 ;
                         IF { PUSH string "The proposal has been executed" ; FAILWITH } {} ;
                         PUSH int 60 ;
                         DUP ;
                         PUSH int 24 ;
                         DUP 5 ;
                         CAR ;
                         GET 3 ;
                         INT ;
                         MUL ;
                         MUL ;
                         MUL ;
                         DUP 3 ;
                         GET 3 ;
                         CDR ;
                         DUP 3 ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                         GET 7 ;
                         ADD ;
                         NOW ;
                         COMPARE ;
                         GT ;
                         IF { PUSH string "The proposal has expired" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         SENDER ;
                         DUP 3 ;
                         CAR ;
                         PAIR ;
                         GET ;
                         IF_NONE { PUSH bool False } {} ;
                         IF { SWAP ;
                              DUP ;
                              DUG 2 ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              DUP ;
                              DUP 6 ;
                              CAR ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 343 ; FAILWITH } {} ;
                              PUSH nat 1 ;
                              DIG 8 ;
                              GET 3 ;
                              CDR ;
                              DUP 9 ;
                              CAR ;
                              GET ;
                              IF_NONE { PUSH int 340 ; FAILWITH } {} ;
                              GET 11 ;
                              SUB ;
                              ISNAT ;
                              IF_NONE { PUSH int 343 ; FAILWITH } {} ;
                              UPDATE 11 ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              SWAP }
                            {} ;
                         DUP ;
                         CDR ;
                         IF { SWAP ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              DUP ;
                              DUP 6 ;
                              CAR ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 347 ; FAILWITH } {} ;
                              DUP ;
                              GET 11 ;
                              PUSH nat 1 ;
                              ADD ;
                              UPDATE 11 ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              SWAP }
                            {} ;
                         SWAP ;
                         DUP ;
                         GET 6 ;
                         DUP 3 ;
                         CDR ;
                         SOME ;
                         SENDER ;
                         DIG 4 ;
                         CAR ;
                         PAIR ;
                         UPDATE ;
                         UPDATE 6 } } } ;
             NIL operation } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
