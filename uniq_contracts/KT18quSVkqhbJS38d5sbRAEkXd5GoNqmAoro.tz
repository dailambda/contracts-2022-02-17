{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (or %assets
               (or (pair %balance_of
                      (list %requests (pair (address %owner) (nat %token_id)))
                      (contract %callback
                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                   (list %transfer
                      (pair (address %from_)
                            (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
               (list %update_operators
                  (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                      (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))
        (or %tokens
           (or (list %burn_tokens (pair (address %owner) (pair (nat %token_id) (nat %amount))))
               (pair %create_token (nat %token_id) (map %token_info string bytes)))
           (list %mint_tokens (pair (address %owner) (pair (nat %token_id) (nat %amount)))))) ;
  storage
    (pair (pair (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))
                (pair %assets
                   (pair (big_map %ledger (pair address nat) nat)
                         (big_map %operators (pair address (pair address nat)) unit))
                   (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                         (big_map %token_total_supply nat nat))))
          (big_map %metadata string bytes)) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         LAMBDA
           (pair (pair address nat) (big_map (pair address nat) nat))
           nat
           { { { DUP ; CAR ; DIP { CDR } } } ;
             GET ;
             IF_NONE { PUSH nat 0 } {} } ;
         DUP ;
         LAMBDA
           (pair (lambda (pair (pair address nat) (big_map (pair address nat) nat)) nat)
                 (pair (pair address nat) (pair nat (big_map (pair address nat) nat))))
           (big_map (pair address nat) nat)
           { { { DUP ; CAR ; DIP { CDR } } } ;
             SWAP ;
             DUP ;
             GET 4 ;
             SWAP ;
             DUP ;
             CAR ;
             CDR ;
             SWAP ;
             DUP ;
             DUG 3 ;
             CAR ;
             CAR ;
             PAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             PAIR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DIG 3 ;
             GET 3 ;
             ADD ;
             DUP ;
             PUSH nat 0 ;
             COMPARE ;
             EQ ;
             IF { DROP ; NONE nat ; SWAP ; UPDATE }
                { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } ;
         SWAP ;
         APPLY ;
         { DIP 2 { DUP } ; DIG 3 } ;
         { DIP 2 { DUP } ; DIG 3 } ;
         PAIR ;
         LAMBDA
           (pair (pair (lambda (pair (pair address nat) (big_map (pair address nat) nat)) nat) string)
                 (pair (pair address nat) (pair nat (big_map (pair address nat) nat))))
           (big_map (pair address nat) nat)
           { { { DUP ; CAR ; DIP { CDR } } } ;
             { { DUP ; CAR ; DIP { CDR } } } ;
             DIG 2 ;
             DUP ;
             GET 4 ;
             SWAP ;
             DUP ;
             CAR ;
             CDR ;
             SWAP ;
             DUP ;
             DUG 3 ;
             CAR ;
             CAR ;
             PAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             PAIR ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DIG 3 ;
             GET 3 ;
             SWAP ;
             SUB ;
             ISNAT ;
             IF_NONE
               { DROP 2 ; FAILWITH }
               { DIG 3 ;
                 DROP ;
                 DUP ;
                 PUSH nat 0 ;
                 COMPARE ;
                 EQ ;
                 IF { DROP ; NONE nat ; SWAP ; UPDATE }
                    { DIG 2 ; SWAP ; SOME ; DIG 2 ; UPDATE } } } ;
         SWAP ;
         APPLY ;
         LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } {} ;
             UNIT } ;
         DIG 6 ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { DIG 6 ;
             DROP ;
             IF_LEFT
               { DIG 3 ;
                 DROP ;
                 DIG 3 ;
                 DROP ;
                 DIG 3 ;
                 DROP ;
                 DIG 3 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         DIG 2 ;
                         DROP ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             COMPARE ;
                             EQ ;
                             IF { CAR ; CDR ; NONE address ; SWAP ; SENDER ; PAIR ; PAIR }
                                { PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PAIR ;
                         DUP ;
                         CDR ;
                         DUP ;
                         CDR ;
                         DIG 2 ;
                         CAR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     PAIR ;
                     DUP ;
                     CAR ;
                     SOME ;
                     SWAP ;
                     GET 3 ;
                     PAIR ;
                     NIL operation ;
                     PAIR } ;
                 { { DUP ; CAR ; DIP { CDR } } } ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR }
               { DIG 2 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CDR ;
                 IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         GET 3 ;
                         SWAP ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         CAR ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         CAR ;
                         CAR ;
                         DUP ;
                         CAR ;
                         MAP { { DIP 2 { DUP } ; DIG 3 } ;
                               CDR ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               MEM ;
                               IF { { DIP 2 { DUP } ; DIG 3 } ;
                                    CAR ;
                                    CDR ;
                                    SWAP ;
                                    DUP ;
                                    DUG 2 ;
                                    PAIR ;
                                    { DIP 6 { DUP } ; DIG 7 } ;
                                    SWAP ;
                                    EXEC ;
                                    SWAP ;
                                    PAIR }
                                  { DROP ; { DIP 5 { DUP } ; DIG 6 } ; FAILWITH } } ;
                         DIG 2 ;
                         DROP ;
                         DIG 4 ;
                         DROP ;
                         DIG 4 ;
                         DROP ;
                         SWAP ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR }
                       { DIG 5 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         LAMBDA
                           (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                           unit
                           { DUP ;
                             CAR ;
                             CAR ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CDR ;
                             DUP ;
                             { DIP 2 { DUP } ; DIG 3 } ;
                             COMPARE ;
                             EQ ;
                             IF { DROP 3 }
                                { DIG 2 ;
                                  DUP ;
                                  GET 4 ;
                                  SWAP ;
                                  GET 3 ;
                                  DIG 2 ;
                                  PAIR ;
                                  DIG 2 ;
                                  PAIR ;
                                  MEM ;
                                  IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } ;
                             UNIT } ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         DUP ;
                         CDR ;
                         DUP ;
                         CAR ;
                         CAR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CAR ;
                         ITER { DUP ;
                                DUG 2 ;
                                CDR ;
                                ITER { SWAP ;
                                       PAIR ;
                                       DUP ;
                                       CDR ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       GET 3 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF { { DIP 3 { DUP } ; DIG 4 } ;
                                            CAR ;
                                            CDR ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            PAIR ;
                                            SENDER ;
                                            { DIP 4 { DUP } ; DIG 5 } ;
                                            CAR ;
                                            PAIR ;
                                            PAIR ;
                                            { DIP 5 { DUP } ; DIG 6 } ;
                                            CAR ;
                                            CDR ;
                                            SWAP ;
                                            EXEC ;
                                            DROP ;
                                            SWAP ;
                                            CAR ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 4 ;
                                            PAIR ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            { DIP 3 { DUP } ; DIG 4 } ;
                                            CAR ;
                                            PAIR ;
                                            PAIR ;
                                            { DIP 7 { DUP } ; DIG 8 } ;
                                            SWAP ;
                                            EXEC ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 4 ;
                                            PAIR ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            CAR ;
                                            PAIR ;
                                            PAIR ;
                                            { DIP 7 { DUP } ; DIG 8 } ;
                                            SWAP ;
                                            EXEC }
                                          { DROP 2 ; { DIP 7 { DUP } ; DIG 8 } ; FAILWITH } } ;
                                SWAP ;
                                DROP } ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         CDR ;
                         SWAP ;
                         CAR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     PAIR ;
                     SENDER ;
                     SWAP ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     ITER { SWAP ;
                            PAIR ;
                            DUP ;
                            CDR ;
                            { DIP 2 { DUP } ; DIG 3 } ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            IF_LEFT {} {} ;
                            CAR ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                            SWAP ;
                            CAR ;
                            SWAP ;
                            IF_LEFT
                              { SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 2 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                DUG 4 ;
                                GET 3 ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                UPDATE }
                              { DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                GET 3 ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                NONE unit ;
                                SWAP ;
                                UPDATE } } ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } ;
                 { { DUP ; CAR ; DIP { CDR } } } ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 DIG 2 ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR } }
           { DIG 5 ;
             DROP ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             DIG 3 ;
             SWAP ;
             EXEC ;
             DROP ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CDR ;
             SWAP ;
             IF_LEFT
               { DIG 4 ;
                 DROP ;
                 IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     ITER { DUP ;
                            DUG 2 ;
                            GET 4 ;
                            PAIR ;
                            SWAP ;
                            DUP ;
                            GET 3 ;
                            SWAP ;
                            CAR ;
                            PAIR ;
                            PAIR ;
                            { DIP 4 { DUP } ; DIG 5 } ;
                            SWAP ;
                            EXEC } ;
                     DIG 4 ;
                     DROP ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 4 ;
                     DIG 2 ;
                     ITER { SWAP ;
                            DUP ;
                            DUG 2 ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 3 ;
                            GET ;
                            IF_NONE
                              { DROP 2 ; { DIP 4 { DUP } ; DIG 5 } ; FAILWITH }
                              { SWAP ;
                                DUP ;
                                DUG 2 ;
                                GET 4 ;
                                SWAP ;
                                SUB ;
                                ISNAT ;
                                IF_NONE { { DIP 5 { DUP } ; DIG 6 } ; FAILWITH } {} ;
                                DIG 2 ;
                                SWAP ;
                                SOME ;
                                DIG 2 ;
                                GET 3 ;
                                UPDATE } } ;
                     DIG 4 ;
                     DROP ;
                     DIG 4 ;
                     DROP ;
                     DIG 2 ;
                     DUP ;
                     CDR ;
                     SWAP ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     PAIR ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR }
                   { DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DUP ;
                     CAR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE
                       { DIG 2 ;
                         DUP ;
                         GET 4 ;
                         SWAP ;
                         DUP ;
                         DUG 4 ;
                         GET 3 ;
                         DIG 3 ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         GET 4 ;
                         PUSH (option nat) (Some 0) ;
                         DIG 3 ;
                         UPDATE ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR }
                       { PUSH string "FA2_DUP_TOKEN_ID" ; FAILWITH } ;
                     NIL operation ;
                     PAIR } }
               { DIG 3 ;
                 DROP ;
                 DIG 4 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 ITER { DUP ;
                        DUG 2 ;
                        GET 4 ;
                        PAIR ;
                        SWAP ;
                        DUP ;
                        GET 3 ;
                        SWAP ;
                        CAR ;
                        PAIR ;
                        PAIR ;
                        { DIP 4 { DUP } ; DIG 5 } ;
                        SWAP ;
                        EXEC } ;
                 DIG 4 ;
                 DROP ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 GET 4 ;
                 DIG 2 ;
                 ITER { SWAP ;
                        DUP ;
                        DUG 2 ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        GET 3 ;
                        GET ;
                        IF_NONE
                          { DROP 2 ; { DIP 3 { DUP } ; DIG 4 } ; FAILWITH }
                          { SWAP ;
                            DUP ;
                            DUG 2 ;
                            GET 4 ;
                            ADD ;
                            DIG 2 ;
                            SWAP ;
                            SOME ;
                            DIG 2 ;
                            GET 3 ;
                            UPDATE } } ;
                 DIG 4 ;
                 DROP ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 PAIR ;
                 PAIR ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 PAIR ;
                 SWAP ;
                 CAR ;
                 PAIR ;
                 NIL operation ;
                 PAIR } ;
             { { DUP ; CAR ; DIP { CDR } } } ;
             { DIP 2 { DUP } ; DIG 3 } ;
             CDR ;
             DIG 2 ;
             DIG 3 ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             SWAP ;
             PAIR } } }
