{ parameter
    (or (or (pair %execute_offer
               (address %token_contract)
               (pair (nat %token_id) (address %seller)))
            (pair %place_offer
               (address %token_contract)
               (pair (nat %token_id) (pair (mutez %price) (option %expiration timestamp)))))
        (or (pair %remove_offer (address %token_contract) (nat %token_id))
            (pair %update_config
               (bool %pause)
               (pair (nat %fee_ratio)
                     (pair (mutez %min_price) (pair (address %proceed_recipient) (address %owner))))))) ;
  storage
    (pair (big_map %offers
             (pair (pair address address) nat)
             (pair (mutez %price) (option %expiration timestamp)))
          (pair %config
             (bool %pause)
             (pair (nat %fee_ratio)
                   (pair (mutez %min_price) (pair (address %proceed_recipient) (address %owner)))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CAR ;
                 IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                 DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 DUP 3 ;
                 GET 4 ;
                 PAIR ;
                 PAIR ;
                 DUP 3 ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH string "OFFER_NOT_FOUND" ; FAILWITH } {} ;
                 DUP ;
                 CDR ;
                 IF_NONE
                   { PUSH unit Unit }
                   { NOW ;
                     SWAP ;
                     COMPARE ;
                     LE ;
                     IF { PUSH string "OFFER_EXPIRED" ; FAILWITH } { PUSH unit Unit } } ;
                 DROP ;
                 DUP ;
                 CAR ;
                 AMOUNT ;
                 COMPARE ;
                 GT ;
                 IF { PUSH string "AMOUNT_TOO_HIGH" ; FAILWITH }
                    { DUP ;
                      CAR ;
                      AMOUNT ;
                      COMPARE ;
                      LT ;
                      IF { PUSH string "AMOUNT_TOO_LOW" ; FAILWITH } {} } ;
                 PUSH nat 1000000 ;
                 DUP 5 ;
                 CDR ;
                 GET 3 ;
                 DUP 3 ;
                 CAR ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP ;
                 DIG 2 ;
                 CAR ;
                 SUB ;
                 DUP 4 ;
                 GET 4 ;
                 CONTRACT unit ;
                 IF_NONE
                   { DROP ; PUSH string "INVALID_SELLER" ; FAILWITH }
                   { SWAP ; PUSH unit Unit ; TRANSFER_TOKENS } ;
                 DUP 5 ;
                 CDR ;
                 GET 7 ;
                 CONTRACT unit ;
                 IF_NONE
                   { SWAP ; DROP ; PUSH string "INVALID_PROCEED_RECIPIENT" ; FAILWITH }
                   { DIG 2 ; PUSH unit Unit ; TRANSFER_TOKENS } ;
                 DUP 4 ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE
                   { DIG 3 ; DROP ; PUSH string "INVALID_TOKEN_CONTRACT" ; FAILWITH }
                   { NIL (pair address (list (pair address (pair nat nat)))) ;
                     DUP 6 ;
                     GET 4 ;
                     NIL (pair address (pair nat nat)) ;
                     SENDER ;
                     DIG 8 ;
                     GET 3 ;
                     PUSH nat 1 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     CONS ;
                     SWAP ;
                     PAIR ;
                     CONS ;
                     SWAP ;
                     PUSH mutez 0 ;
                     DIG 2 ;
                     TRANSFER_TOKENS } ;
                 DIG 4 ;
                 DUP ;
                 CAR ;
                 DIG 5 ;
                 NONE (pair mutez (option timestamp)) ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 1 ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 DIG 2 ;
                 CONS ;
                 DIG 2 ;
                 CONS ;
                 PAIR }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CAR ;
                 IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 GT ;
                 IF { PUSH string "AMOUNT_NOT_ZERO" ; FAILWITH } {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 GET 5 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 COMPARE ;
                 LT ;
                 IF { PUSH string "PRICE_TOO_LOW" ; FAILWITH } {} ;
                 DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SENDER ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 6 ;
                 IF_NONE
                   { PUSH unit Unit }
                   { NOW ;
                     SWAP ;
                     COMPARE ;
                     LE ;
                     IF { PUSH string "EXPIRATION_PAST" ; FAILWITH } { PUSH unit Unit } } ;
                 DROP ;
                 DIG 2 ;
                 DUP ;
                 CAR ;
                 DUP 4 ;
                 GET 5 ;
                 DIG 4 ;
                 GET 6 ;
                 SWAP ;
                 PAIR ;
                 SOME ;
                 DIG 3 ;
                 UPDATE ;
                 UPDATE 1 ;
                 NIL operation ;
                 PAIR } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 CAR ;
                 IF { PUSH string "CONTRACT_PAUSED" ; FAILWITH } {} ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 GT ;
                 IF { PUSH string "AMOUNT_NOT_ZERO" ; FAILWITH } {} ;
                 UNPAIR ;
                 SENDER ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH string "OFFER_NOT_FOUND" ; FAILWITH } { DROP } ;
                 SWAP ;
                 DUP ;
                 CAR ;
                 DIG 2 ;
                 NONE (pair mutez (option timestamp)) ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 1 ;
                 NIL operation ;
                 PAIR }
               { PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 GT ;
                 IF { PUSH string "AMOUNT_NOT_ZERO" ; FAILWITH } {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 GET 8 ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "NOT_AUTHORIZED" ; FAILWITH } {} ;
                 UPDATE 2 ;
                 NIL operation ;
                 PAIR } } } }
