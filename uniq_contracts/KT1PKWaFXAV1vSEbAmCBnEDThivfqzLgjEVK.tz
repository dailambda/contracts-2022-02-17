{ storage
    (pair (pair (pair %config (address %admin) (bool %paused))
                (pair (pair %metadata
                         (pair (list %authors string) (pair (string %description) (string %homepage)))
                         (pair (pair (list %interfaces string) (string %name))
                               (pair (string %twitter) (string %version))))
                      (pair %permissions
                         (pair (bool %can_admin_edit_other_authors) (bool %can_author_remove))
                         (pair (bool %can_author_update) (bool %only_admin_can_be_author)))))
          (pair (big_map %posts nat (pair (nat %id) (pair (address %author) (string %value))))
                (pair (nat %posts_lastId) (string %posts_payload_type)))) ;
  parameter
    (or (or (string %add_post) (bool %pause))
        (or (nat %remove_post)
            (or (address %set_admin) (pair %update_post (nat %postId) (string %value))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 8 ;
                 IF { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CAR ;
                      CAR ;
                      CDR ;
                      IF { PUSH string "TZBLG1_PAUSED" ; FAILWITH } {} }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CAR ;
                      CAR ;
                      CDR ;
                      IF { SWAP ; DUP ; DUG 2 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ }
                         { PUSH bool True } ;
                      IF {} { PUSH string "TZBLG1_PAUSED" ; FAILWITH } } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 8 ;
                 IF { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CAR ;
                      CAR ;
                      CAR ;
                      SENDER ;
                      COMPARE ;
                      EQ ;
                      IF {} { PUSH string "TZBLG1_NOT_ADMIN" ; FAILWITH } }
                    {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 5 ;
                 DUP 3 ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF { PUSH string "TZBLG1_POST_ALREADY_EXISTS" ; FAILWITH } {} ;
                 DIG 2 ;
                 DUP ;
                 GET 3 ;
                 DIG 3 ;
                 SENDER ;
                 DUP 5 ;
                 PAIR 3 ;
                 SOME ;
                 DUP 4 ;
                 UPDATE ;
                 UPDATE 3 ;
                 PUSH nat 1 ;
                 DIG 2 ;
                 ADD ;
                 UPDATE 5 }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "TZBLG1_NOT_ADMIN" ; FAILWITH } ;
                 SWAP ;
                 UNPAIR ;
                 UNPAIR ;
                 DIG 3 ;
                 UPDATE 2 ;
                 PAIR ;
                 PAIR } }
           { IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 8 ;
                 IF { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CAR ;
                      CAR ;
                      CDR ;
                      IF { PUSH string "TZBLG1_PAUSED" ; FAILWITH } {} }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      CAR ;
                      CAR ;
                      CDR ;
                      IF { SWAP ; DUP ; DUG 2 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ }
                         { PUSH bool True } ;
                      IF {} { PUSH string "TZBLG1_PAUSED" ; FAILWITH } } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 5 ;
                 CDR ;
                 IF {} { PUSH string "TZBLG1_REMOVE_NOT_POSSIBLE" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF {} { PUSH string "TZBLG1_POST_NOT_FOUND" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 GET 5 ;
                 CAR ;
                 IF { SWAP ;
                      DUP ;
                      DUG 2 ;
                      GET 3 ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      GET ;
                      IF_NONE { PUSH int 243 ; FAILWITH } {} ;
                      GET 3 ;
                      SENDER ;
                      COMPARE ;
                      EQ ;
                      IF { PUSH bool True }
                         { SWAP ; DUP ; DUG 2 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                      IF {} { PUSH string "TZBLG1_NOT_AUTHOR" ; FAILWITH } }
                    { SWAP ;
                      DUP ;
                      DUG 2 ;
                      GET 3 ;
                      SWAP ;
                      DUP ;
                      DUG 2 ;
                      GET ;
                      IF_NONE { PUSH int 234 ; FAILWITH } {} ;
                      GET 3 ;
                      SENDER ;
                      COMPARE ;
                      EQ ;
                      IF {} { PUSH string "TZBLG1_NOT_AUTHOR" ; FAILWITH } } ;
                 SWAP ;
                 DUP ;
                 GET 3 ;
                 NONE (pair nat (pair address string)) ;
                 DIG 3 ;
                 UPDATE ;
                 UPDATE 3 }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "TZBLG1_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     DIG 3 ;
                     UPDATE 1 ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 8 ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          CAR ;
                          CDR ;
                          IF { PUSH string "TZBLG1_PAUSED" ; FAILWITH } {} }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          CAR ;
                          CDR ;
                          IF { SWAP ; DUP ; DUG 2 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ }
                             { PUSH bool True } ;
                          IF {} { PUSH string "TZBLG1_PAUSED" ; FAILWITH } } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 7 ;
                     IF {} { PUSH string "TZBLG1_UPDATE_NOT_POSSIBLE" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     DUP 3 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     MEM ;
                     IF {} { PUSH string "TZBLG1_POST_NOT_FOUND" ; FAILWITH } ;
                     DUP 3 ;
                     CAR ;
                     GET 5 ;
                     CAR ;
                     IF { DUP 3 ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 243 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          COMPARE ;
                          EQ ;
                          IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                          IF {} { PUSH string "TZBLG1_NOT_AUTHOR" ; FAILWITH } }
                        { DUP 3 ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 234 ; FAILWITH } {} ;
                          GET 3 ;
                          SENDER ;
                          COMPARE ;
                          EQ ;
                          IF {} { PUSH string "TZBLG1_NOT_AUTHOR" ; FAILWITH } } ;
                     SWAP ;
                     CDR ;
                     DUP 3 ;
                     DUP ;
                     GET 3 ;
                     DIG 2 ;
                     DIG 4 ;
                     GET 3 ;
                     DUP 5 ;
                     GET ;
                     IF_NONE { PUSH int 322 ; FAILWITH } {} ;
                     GET 3 ;
                     DUP 5 ;
                     PAIR 3 ;
                     SOME ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 3 } } } ;
         NIL operation ;
         PAIR } }
