{ parameter
    (or (list %transfer
           (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
        (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback (list (pair (address %owner) (nat %token_id) (nat %balance)))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                       (pair %remove_operator (address %owner) (address %operator) (nat %token_id))))
                (contract %total_supplies (map nat nat))))) ;
  storage
    (pair (big_map %ledger
             (address :owner)
             (map (nat :token_id) (pair (nat %balance) (set %operators address))))
          (map %total_supplies (nat :token_id) (nat :total_supply))
          (map nat (pair nat (set address)))
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))) ;
  code { UNPAIR ;
         IF_LEFT
           { DIP { DUP ; CAR @% } ;
             ITER { UNPAIR @% @% ;
                    SWAP ;
                    ITER { DIP { DUP 2 ;
                                 DUP 2 ;
                                 GET ;
                                 IF_NONE { DUP 3 ; GET 5 } {} ;
                                 RENAME @from_balances } ;
                           UNPAIR @% @% ;
                           SWAP ;
                           UNPAIR @to_id @to_amount ;
                           DUP ;
                           DUG 3 ;
                           DIP 3
                               { DUP 3 ;
                                 SENDER ;
                                 { COMPARE ;
                                   EQ ;
                                   IF { DROP }
                                      { DIP { DUP } ;
                                        GET ;
                                        { IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} } ;
                                        CDR ;
                                        SENDER ;
                                        MEM ;
                                        IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } } } ;
                           DIG 3 ;
                           DUP ;
                           DIP { DUP 2 ;
                                 GET ;
                                 { IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} } ;
                                 UNPAIR @from_balance ;
                                 DUP 4 ;
                                 SWAP ;
                                 SUB ;
                                 ISNAT ;
                                 { IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} } ;
                                 PAIR ;
                                 SOME } ;
                           SWAP ;
                           DUP 3 ;
                           UPDATE ;
                           DUG 3 ;
                           DIP 3 { SOME ; SWAP ; DUP ; DIP { UPDATE } } ;
                           DUP 5 ;
                           DUP 4 ;
                           GET ;
                           IF_NONE { DUP 6 ; GET 5 } {} ;
                           DUP ;
                           DIP { DUP 2 ;
                                 GET ;
                                 { IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} } ;
                                 SWAP ;
                                 DIP { UNPAIR ; SWAP ; DIP { ADD } ; SWAP ; PAIR ; SOME } } ;
                           DUG 2 ;
                           UPDATE ;
                           SOME ;
                           SWAP ;
                           DIP 2 { SWAP } ;
                           UPDATE ;
                           SWAP } ;
                    DROP } ;
             SWAP ;
             { DUP ; CDR @%% ; DIP { CAR ; { DROP } } ; SWAP ; PAIR % %@ } ;
             NIL operation }
           { IF_LEFT
               { UNPAIR @% @% ;
                 DIP { NIL (pair address nat nat) ; DUP 3 ; CAR @% } ;
                 ITER { DUP ;
                        UNPAIR @% @request_id ;
                        DUP 4 ;
                        SWAP ;
                        GET ;
                        IF_NONE
                          { DROP ; PUSH nat 0 }
                          { SWAP ;
                            GET ;
                            { IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } { CAR } } } ;
                        SWAP ;
                        UNPAIR @% @% ;
                        PAIR 3 ;
                        SWAP ;
                        DIP { CONS } } ;
                 DROP ;
                 PUSH mutez 0 ;
                 SWAP ;
                 TRANSFER_TOKENS ;
                 NIL operation ;
                 SWAP ;
                 CONS }
               { IF_LEFT
                   { ITER { SWAP ;
                            DUP ;
                            GET 5 ;
                            DUG 2 ;
                            DUP ;
                            DIP { CAR ;
                                  DIP { IF_LEFT { PUSH bool True } { PUSH bool False } } ;
                                  DUP ;
                                  DIP { DIP { SWAP ;
                                              UNPAIR @% ;
                                              DUP ;
                                              SENDER ;
                                              { COMPARE ; EQ ; IF {} { PUSH string "not owner" ; FAILWITH } } } ;
                                        SWAP ;
                                        DUP ;
                                        DIP { GET ;
                                              IF_NONE { DIG 2 } { DIP 3 { DROP } } ;
                                              DUP ;
                                              DIP { SWAP ;
                                                    UNPAIR @% @% ;
                                                    DUP 2 ;
                                                    DIP 2
                                                        { GET ;
                                                          IF_NONE { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} ;
                                                          UNPAIR @balance @operators } ;
                                                    DIG 2 ;
                                                    DIP { DIP { DIP { SWAP } ; UPDATE } } ;
                                                    SWAP ;
                                                    DIP { PAIR ; SOME } ;
                                                    SWAP } ;
                                              SWAP ;
                                              DIG 2 ;
                                              UPDATE ;
                                              SOME } ;
                                        SWAP } ;
                                  SWAP ;
                                  DIG 2 ;
                                  UPDATE } ;
                            { DUP ; CDR @%% ; DIP { CAR ; { DROP } } ; SWAP ; PAIR % %@ } } ;
                     NIL operation }
                   { DUP 2 ;
                     GET 3 ;
                     PUSH mutez 0 ;
                     SWAP ;
                     TRANSFER_TOKENS ;
                     NIL operation ;
                     SWAP ;
                     CONS } } } ;
         PAIR } }
