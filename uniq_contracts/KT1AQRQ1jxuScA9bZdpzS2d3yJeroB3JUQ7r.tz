{ parameter
    (or (or (or (list %addToWhitelist address) (unit %default))
            (or (unit %getRandomStartIndex) (or (nat %mint) (mutez %payRecipients))))
        (or (or (list %removeFromWhitelist address)
                (or (pair %reveal (nat %end) (pair (string %ipfs_folder) (nat %start)))
                    (pair %send (mutez %amount) (address %receiver))))
            (or (nat %setRandomStartIndex) (or (address %setTokenAdmin) (bool %togglePause))))) ;
  storage
    (pair (pair (pair (pair (address %admin) (mutez %cost))
                      (pair (nat %maxPerWallet) (big_map %metadata string bytes)))
                (pair (pair (nat %minted) (big_map %minters address nat))
                      (pair (bool %paused) (bytes %placeholderIpfs))))
          (pair (pair (pair (string %provenanceHash) (nat %randomStartIndex))
                      (pair (address %randomizerAddress) (map %recipients address nat)))
                (pair (pair (timestamp %startTime) (address %tokenAddress))
                      (pair (nat %totalSupply)
                            (pair (set %whitelist address) (timestamp %whitelistStartTime)))))) ;
  code { LAMBDA
           (pair unit
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             CDR ;
             DUP ;
             CAR ;
             CAR ;
             CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF {} { PUSH string "Only admin can call this entrypoint" ; FAILWITH } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair nat
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CDR ;
             DIG 2 ;
             CAR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             CAR ;
             CDR ;
             MUL ;
             AMOUNT ;
             COMPARE ;
             GE ;
             IF {} { PUSH string "Amount too low" ; FAILWITH } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair nat
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CDR ;
             DUP ;
             CAR ;
             CAR ;
             GET 3 ;
             DIG 3 ;
             CAR ;
             COMPARE ;
             LE ;
             IF {} { PUSH string "Supply exhausted for wallet" ; FAILWITH } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair unit
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             CDR ;
             DUP ;
             CAR ;
             GET 5 ;
             PUSH bool True ;
             COMPARE ;
             NEQ ;
             IF {} { PUSH string "Minter halted" ; FAILWITH } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair unit
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             CDR ;
             DUP ;
             GET 9 ;
             SENDER ;
             MEM ;
             IF { DUP ;
                  GET 10 ;
                  NOW ;
                  COMPARE ;
                  GT ;
                  IF {} { PUSH string "Minter not started" ; FAILWITH } }
                { DUP ;
                  GET 5 ;
                  CAR ;
                  NOW ;
                  COMPARE ;
                  GT ;
                  IF {} { PUSH string "Minter not started" ; FAILWITH } } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         LAMBDA
           (pair nat
                 (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                             (pair (pair nat (big_map address nat)) (pair bool bytes)))
                       (pair (pair (pair string nat) (pair address (map address nat)))
                             (pair (pair timestamp address) (pair nat (pair (set address) timestamp))))))
           (pair (list operation)
                 (pair unit
                       (pair (pair (pair (pair address mutez) (pair nat (big_map string bytes)))
                                   (pair (pair nat (big_map address nat)) (pair bool bytes)))
                             (pair (pair (pair string nat) (pair address (map address nat)))
                                   (pair (pair timestamp address) (pair nat (pair (set address) timestamp)))))))
           { NIL operation ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CDR ;
             DUP ;
             GET 7 ;
             DIG 3 ;
             CAR ;
             DUP 3 ;
             CAR ;
             GET 3 ;
             CAR ;
             ADD ;
             COMPARE ;
             LE ;
             IF {} { PUSH string "Supply exhausted" ; FAILWITH } ;
             UNIT ;
             DIG 2 ;
             PAIR 3 } ;
         SWAP ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP 8 ;
                     DIG 2 ;
                     UNIT ;
                     PAIR ;
                     EXEC ;
                     DUP ;
                     GET 4 ;
                     DUG 2 ;
                     DUP ;
                     CAR ;
                     DUP 3 ;
                     ITER { DIG 4 ; DUP ; GET 9 ; PUSH bool True ; DIG 3 ; UPDATE ; UPDATE 9 ; DUG 3 } ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP }
                   { DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     NIL operation } }
               { IF_LEFT
                   { DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     UNIT ;
                     PAIR ;
                     EXEC ;
                     DUP ;
                     GET 4 ;
                     SWAP ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 3 ;
                     CONTRACT %randomBetween (pair nat (pair nat address)) ;
                     IF_NONE { PUSH int 117 ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     SELF %setRandomStartIndex ;
                     ADDRESS ;
                     DUP 5 ;
                     GET 7 ;
                     PUSH nat 1 ;
                     PAIR 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { DUP 5 ;
                         DIG 2 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 2 ;
                         DUP ;
                         CAR ;
                         DUP 6 ;
                         DIG 4 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 4 ;
                         DUP ;
                         CAR ;
                         NIL operation ;
                         SWAP ;
                         ITER { CONS } ;
                         ITER { DIG 2 ; SWAP ; CONS ; SWAP } ;
                         DUP 6 ;
                         DIG 5 ;
                         DUP 6 ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 5 ;
                         DUP ;
                         CAR ;
                         NIL operation ;
                         SWAP ;
                         ITER { CONS } ;
                         ITER { DIG 3 ; SWAP ; CONS ; DUG 2 } ;
                         DUP 11 ;
                         DIG 6 ;
                         DUP 7 ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 6 ;
                         DUP ;
                         CAR ;
                         NIL operation ;
                         SWAP ;
                         ITER { CONS } ;
                         ITER { DIG 4 ; SWAP ; CONS ; DUG 3 } ;
                         PUSH bool False ;
                         DUP 8 ;
                         CAR ;
                         GET 3 ;
                         CDR ;
                         SENDER ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF { DIG 6 ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              UNPAIR ;
                              UNPAIR ;
                              SWAP ;
                              PUSH (option nat) (Some 0) ;
                              SENDER ;
                              UPDATE ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              SWAP ;
                              PAIR ;
                              PAIR ;
                              DUG 6 }
                            {} ;
                         DUP 11 ;
                         DUP 8 ;
                         DUP 8 ;
                         DIG 9 ;
                         CAR ;
                         GET 3 ;
                         CDR ;
                         SENDER ;
                         GET ;
                         IF_NONE { PUSH int 131 ; FAILWITH } {} ;
                         ADD ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 7 ;
                         DUP ;
                         CAR ;
                         NIL operation ;
                         SWAP ;
                         ITER { CONS } ;
                         ITER { DIG 5 ; SWAP ; CONS ; DUG 4 } ;
                         DUP 8 ;
                         CAR ;
                         GET 3 ;
                         CAR ;
                         DIG 8 ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         DUP 13 ;
                         ADD ;
                         SWAP ;
                         DUP ;
                         SENDER ;
                         DUP ;
                         DUG 2 ;
                         GET ;
                         IF_NONE { PUSH int 134 ; FAILWITH } {} ;
                         DUP 15 ;
                         ADD ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         DUG 8 ;
                         NIL nat ;
                         DUP 10 ;
                         GET 3 ;
                         CAR ;
                         CDR ;
                         DUP 3 ;
                         ADD ;
                         DUP 11 ;
                         GET 3 ;
                         CAR ;
                         CDR ;
                         DUP 4 ;
                         ADD ;
                         DUP 11 ;
                         DUP 13 ;
                         GET 3 ;
                         CAR ;
                         CDR ;
                         DUP 6 ;
                         ADD ;
                         ADD ;
                         DUP ;
                         DUP 3 ;
                         COMPARE ;
                         LT ;
                         LOOP { DIG 2 ;
                                DROP ;
                                DUP 12 ;
                                GET 7 ;
                                DUP 3 ;
                                COMPARE ;
                                GT ;
                                IF { SWAP ; DUP ; DUG 2 ; DUP 13 ; GET 7 ; SUB ; ABS ; DUG 2 }
                                   { SWAP ; DUP ; DIG 2 } ;
                                DIG 3 ;
                                DUP 4 ;
                                CONS ;
                                DUG 3 ;
                                PUSH nat 1 ;
                                DIG 2 ;
                                ADD ;
                                SWAP ;
                                DUP ;
                                DUP 3 ;
                                COMPARE ;
                                LT } ;
                         DROP 3 ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         DIG 3 ;
                         DROP ;
                         SWAP ;
                         DUP 3 ;
                         GET 5 ;
                         CDR ;
                         CONTRACT %mint_range (pair (list nat) (pair address (pair nat (map string bytes)))) ;
                         IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         EMPTY_MAP string bytes ;
                         DUP 6 ;
                         CAR ;
                         GET 6 ;
                         SOME ;
                         PUSH string "" ;
                         UPDATE ;
                         PUSH nat 1 ;
                         SENDER ;
                         PAIR 3 ;
                         DIG 4 ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { PUSH bool False ;
                         DUP 3 ;
                         GET 3 ;
                         GET 4 ;
                         SENDER ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "Only recipients can call this entrypoint" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         GET 4 ;
                         ITER { DUP ;
                                DUG 2 ;
                                CAR ;
                                CONTRACT unit ;
                                IF_NONE { PUSH int 192 ; FAILWITH } {} ;
                                PUSH nat 100 ;
                                DUP 5 ;
                                DIG 4 ;
                                CDR ;
                                MUL ;
                                EDIV ;
                                IF_NONE { PUSH int 192 ; FAILWITH } {} ;
                                CAR ;
                                UNIT ;
                                TRANSFER_TOKENS ;
                                CONS } ;
                         SWAP ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP } } } }
           { IF_LEFT
               { IF_LEFT
                   { DUP 8 ;
                     DIG 2 ;
                     UNIT ;
                     PAIR ;
                     EXEC ;
                     DUP ;
                     GET 4 ;
                     DUG 2 ;
                     DUP ;
                     CAR ;
                     DUP 3 ;
                     ITER { DIG 4 ;
                            DUP ;
                            GET 9 ;
                            PUSH bool False ;
                            DIG 3 ;
                            UPDATE ;
                            UPDATE 9 ;
                            DUG 3 } ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DUG 2 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 2 ;
                         CAR ;
                         DUP 3 ;
                         GET 5 ;
                         CDR ;
                         CONTRACT %reveal_range (pair nat (pair nat string)) ;
                         IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         DUP ;
                         GET 3 ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         SWAP ;
                         GET 4 ;
                         PAIR 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DUG 2 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 2 ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 86 ; FAILWITH } {} ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { PUSH string "Only Randomizer can call this entrypoint" ; FAILWITH } {} ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     CAR ;
                     DIG 4 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DUG 2 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 2 ;
                         CAR ;
                         DUP 3 ;
                         GET 5 ;
                         CDR ;
                         CONTRACT %set_administrator address ;
                         IF_NONE { PUSH int 91 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DUG 2 ;
                         UNIT ;
                         PAIR ;
                         EXEC ;
                         DUP ;
                         GET 4 ;
                         DUG 2 ;
                         CAR ;
                         DIG 2 ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 5 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         SWAP } } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
