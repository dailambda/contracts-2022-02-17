{ storage
    (pair (big_map %metadata string bytes)
          (big_map %offers
             (pair (address %offer_origin) (pair (address %offer_fa2) (nat %offer_fa2_id)))
             (set address))) ;
  parameter
    (or (or (pair %accept
               (address %offer_fa2)
               (pair (nat %offer_fa2_id) (address %offer_origin)))
            (pair %add_offer
               (address %offer_fa2)
               (pair (nat %offer_fa2_id) (set %recipients address))))
        (or (pair %cancel_offer (address %offer_fa2) (nat %offer_fa2_id))
            (pair %decline
               (address %offer_fa2)
               (pair (nat %offer_fa2_id) (address %offer_origin))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 3 ;
                 GET 4 ;
                 PAIR 3 ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF {} { PUSH string "OFFER_DOES_NOT_EXIST" ; FAILWITH } ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH int 119 ; FAILWITH } {} ;
                 SENDER ;
                 MEM ;
                 IF {} { PUSH string "NOT_OFFERED" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE { PUSH string "INTERFACE_MISMATCH" ; FAILWITH } {} ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 NIL (pair address (pair nat nat)) ;
                 PUSH nat 1 ;
                 DIG 5 ;
                 GET 3 ;
                 SENDER ;
                 PAIR 3 ;
                 CONS ;
                 SELF_ADDRESS ;
                 PAIR ;
                 CONS ;
                 NIL operation ;
                 DIG 2 ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 DUP ;
                 DIG 4 ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH int 138 ; FAILWITH } {} ;
                 PUSH bool False ;
                 SENDER ;
                 UPDATE ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 2 ;
                 SWAP }
               { DUP ;
                 GET 4 ;
                 SIZE ;
                 PUSH nat 0 ;
                 COMPARE ;
                 LT ;
                 IF {} { PUSH string "OFFER_AMOUNT_TOO_SMALL" ; FAILWITH } ;
                 DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SENDER ;
                 PAIR 3 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE { PUSH string "INTERFACE_MISMATCH" ; FAILWITH } {} ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 NIL (pair address (pair nat nat)) ;
                 DUP 5 ;
                 GET 4 ;
                 SIZE ;
                 DUP 6 ;
                 GET 3 ;
                 SELF_ADDRESS ;
                 PAIR 3 ;
                 CONS ;
                 SENDER ;
                 PAIR ;
                 CONS ;
                 NIL operation ;
                 DUP 3 ;
                 PUSH mutez 0 ;
                 DUP 4 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DUP 6 ;
                 CDR ;
                 DUP 5 ;
                 MEM ;
                 IF { DUP 5 ;
                      GET 4 ;
                      ITER { DIG 6 ;
                             DUP ;
                             CDR ;
                             DUP ;
                             DUP 8 ;
                             DUP ;
                             DUG 2 ;
                             GET ;
                             IF_NONE { PUSH int 73 ; FAILWITH } {} ;
                             PUSH bool True ;
                             DIG 5 ;
                             UPDATE ;
                             SOME ;
                             SWAP ;
                             UPDATE ;
                             UPDATE 2 ;
                             DUG 5 } ;
                      SWAP ;
                      DROP ;
                      SWAP ;
                      DROP ;
                      SWAP ;
                      DROP ;
                      SWAP ;
                      DROP }
                    { SWAP ;
                      DROP ;
                      SWAP ;
                      DROP ;
                      DIG 3 ;
                      DUP ;
                      CDR ;
                      DIG 4 ;
                      GET 4 ;
                      SOME ;
                      DIG 4 ;
                      UPDATE ;
                      UPDATE 2 ;
                      SWAP } } }
           { IF_LEFT
               { DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 SENDER ;
                 PAIR 3 ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF {} { PUSH string "OFFER_DOES_NOT_EXIST" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE { PUSH string "INTERFACE_MISMATCH" ; FAILWITH } {} ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 NIL (pair address (pair nat nat)) ;
                 DUP 6 ;
                 CDR ;
                 DUP 5 ;
                 GET ;
                 IF_NONE { PUSH int 98 ; FAILWITH } {} ;
                 SIZE ;
                 DIG 5 ;
                 CDR ;
                 SENDER ;
                 PAIR 3 ;
                 CONS ;
                 SELF_ADDRESS ;
                 PAIR ;
                 CONS ;
                 NIL operation ;
                 DIG 2 ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 PUSH (option (set address)) (Some {}) ;
                 DIG 4 ;
                 UPDATE ;
                 UPDATE 2 ;
                 SWAP }
               { DUP ;
                 GET 3 ;
                 SWAP ;
                 DUP ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 3 ;
                 GET 4 ;
                 PAIR 3 ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 MEM ;
                 IF {} { PUSH string "OFFER_DOES_NOT_EXIST" ; FAILWITH } ;
                 DUP 3 ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH int 150 ; FAILWITH } {} ;
                 SENDER ;
                 MEM ;
                 IF {} { PUSH string "NOT_OFFERED" ; FAILWITH } ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CONTRACT %transfer
                   (list (pair (address %from_)
                               (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                 IF_NONE { PUSH string "INTERFACE_MISMATCH" ; FAILWITH } {} ;
                 NIL (pair address (list (pair address (pair nat nat)))) ;
                 NIL (pair address (pair nat nat)) ;
                 PUSH nat 1 ;
                 DIG 5 ;
                 DUP ;
                 GET 3 ;
                 SWAP ;
                 GET 4 ;
                 PAIR 3 ;
                 CONS ;
                 SELF_ADDRESS ;
                 PAIR ;
                 CONS ;
                 NIL operation ;
                 DIG 2 ;
                 PUSH mutez 0 ;
                 DIG 3 ;
                 TRANSFER_TOKENS ;
                 CONS ;
                 DIG 2 ;
                 DUP ;
                 CDR ;
                 DUP ;
                 DIG 4 ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE { PUSH int 169 ; FAILWITH } {} ;
                 PUSH bool False ;
                 SENDER ;
                 UPDATE ;
                 SOME ;
                 SWAP ;
                 UPDATE ;
                 UPDATE 2 ;
                 SWAP } } ;
         PAIR } }
