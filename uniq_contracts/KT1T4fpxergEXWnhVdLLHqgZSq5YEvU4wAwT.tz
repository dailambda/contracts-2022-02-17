{ storage
    (pair (pair (pair (address %administrator) (pair (nat %all_tokens) (bytes %base_uri)))
                (pair (big_map %hashes nat bytes) (pair (big_map %ledger nat address) (bool %locked))))
          (pair (pair (nat %max_editions)
                      (pair (big_map %metadata string bytes)
                            (big_map %operators
                               (pair (address %owner) (pair (address %operator) (nat %token_id)))
                               unit)))
                (pair (bool %paused) (pair (mutez %price) (string %script))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (unit %lock))
            (or (int %mint)
                (or (pair %mutez_transfer (mutez %amount) (address %destination))
                    (address %set_administrator))))
        (or (or (bytes %set_base_uri)
                (or (pair %set_mint_parameters (mutez %price) (nat %max_editions)) (bool %set_pause)))
            (or (string %set_script)
                (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) unit)
                            (or int (or (pair mutez address) address)))
                        (or (or bytes (or (pair mutez nat) bool))
                            (or string
                                (or (list (pair address (list (pair address (pair nat nat)))))
                                    (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair (pair (pair address (pair nat bytes))
                                (pair (big_map nat bytes) (pair (big_map nat address) bool)))
                          (pair (pair nat (pair (big_map string bytes) (big_map (pair address (pair address nat)) unit)))
                                (pair bool (pair mutez string))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           CAR ;
                           GET 3 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP ;
                           CAR ;
                           DUP 4 ;
                           CAR ;
                           GET 5 ;
                           DUP 3 ;
                           CDR ;
                           GET ;
                           IF_NONE { PUSH int 345 ; FAILWITH } {} ;
                           COMPARE ;
                           EQ ;
                           IF { PUSH nat 1 ; SWAP ; PAIR } { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { DROP ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     PUSH bool True ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     NIL operation } }
               { IF_LEFT
                   { DUP ;
                     PUSH int 0 ;
                     COMPARE ;
                     LT ;
                     IF {} { PUSH string "FA2_BAD_QUANTITY" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ISNAT ;
                     IF_NONE { PUSH string "FA2_BAD_QUANTITY" ; FAILWITH } {} ;
                     DUP 3 ;
                     GET 7 ;
                     MUL ;
                     AMOUNT ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_BAD_VALUE" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     ISNAT ;
                     IF_NONE { PUSH string "FA2_BAD_QUANTITY" ; FAILWITH } {} ;
                     DUP 4 ;
                     CAR ;
                     CAR ;
                     GET 3 ;
                     ADD ;
                     COMPARE ;
                     LE ;
                     IF {} { PUSH string "FA2_MAX_EDITIONS_REACHED" ; FAILWITH } ;
                     DUP ;
                     DUP ;
                     PUSH int 0 ;
                     COMPARE ;
                     LT ;
                     LOOP { DUP 3 ;
                            CAR ;
                            CAR ;
                            GET 3 ;
                            DUP 4 ;
                            GET 3 ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            COMPARE ;
                            LT ;
                            IF {} { PUSH string "FA2_MAX_EDITIONS_REACHED" ; FAILWITH } ;
                            DIG 3 ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            SWAP ;
                            UNPAIR ;
                            SENDER ;
                            SOME ;
                            DUP 7 ;
                            UPDATE ;
                            PAIR ;
                            SWAP ;
                            DUP 5 ;
                            SENDER ;
                            NOW ;
                            PAIR 3 ;
                            PACK ;
                            KECCAK ;
                            SOME ;
                            DUP 6 ;
                            UPDATE ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            DUG 3 ;
                            DUP ;
                            DUP 5 ;
                            CAR ;
                            CAR ;
                            GET 3 ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                            DIG 3 ;
                            UNPAIR ;
                            UNPAIR ;
                            UNPAIR ;
                            SWAP ;
                            CDR ;
                            PUSH nat 1 ;
                            DIG 5 ;
                            ADD ;
                            PAIR ;
                            SWAP ;
                            PAIR ;
                            PAIR ;
                            PAIR ;
                            DUG 2 ;
                            PUSH int 1 ;
                            SWAP ;
                            SUB ;
                            DUP ;
                            PUSH int 0 ;
                            COMPARE ;
                            LT } ;
                     DROP 2 ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH int 260 ; FAILWITH } ;
                         DUP ;
                         CAR ;
                         BALANCE ;
                         SWAP ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH int 263 ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         CONTRACT unit ;
                         IF_NONE { PUSH int 264 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         NIL operation } } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 6 ;
                     IF { PUSH string "FA2_LOCKED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     DIG 4 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         PUSH nat 1 ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         GET 3 ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH string "FA2_SALE_STARTED" ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         DUP 3 ;
                         CAR ;
                         CAR ;
                         GET 3 ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH string "FA2_BAD_QUANTITY" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         CDR ;
                         DUP 4 ;
                         CDR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         UPDATE 7 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 5 } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 6 ;
                     IF { PUSH string "FA2_LOCKED" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 8 }
                   { IF_LEFT
                       { DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { SENDER ;
                                       DUP 3 ;
                                       CAR ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 3 ;
                                            GET 4 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP 4 ;
                                       CAR ;
                                       GET 3 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 1 ;
                                       SWAP ;
                                       COMPARE ;
                                       LE ;
                                       IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 1 ;
                                       COMPARE ;
                                       EQ ;
                                       IF { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            CAR ;
                                            DUP 5 ;
                                            CAR ;
                                            GET 5 ;
                                            DUP 3 ;
                                            GET 3 ;
                                            GET ;
                                            IF_NONE { PUSH int 333 ; FAILWITH } {} ;
                                            COMPARE ;
                                            EQ ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DIG 3 ;
                                            UNPAIR ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            SWAP ;
                                            UNPAIR ;
                                            DUP 6 ;
                                            CAR ;
                                            SOME ;
                                            DIG 6 ;
                                            GET 3 ;
                                            UPDATE ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            SWAP ;
                                            PAIR ;
                                            PAIR ;
                                            DUG 2 }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    SWAP ;
                                    NONE unit ;
                                    DIG 6 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
