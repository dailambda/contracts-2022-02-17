{ parameter
    (or (or (or (or (pair %assert_merits (pair (address %address) (list %merits nat)) (nat %threshold))
                    (pair %balance_of
                       (list %requests (pair (address %owner) (nat %token_id)))
                       (contract %callback
                          (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))))))
                (or (unit %confirm_admin) (nat %discard)))
            (or (or (list %give_awards (pair address nat))
                    (pair %mint
                       (pair (address %address) (nat %amount))
                       (pair (map %metadata string bytes) (nat %token_id))))
                (or (bool %pause) (address %set_admin))))
        (or (or (or (address %set_frontend) (pair %set_metadata (string %k) (bytes %v)))
                (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))
            (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes))))) ;
  storage
    (pair (pair (pair (pair (address %admin) (address %frontend))
                      (pair (big_map %issuers (pair address nat) nat)
                            (big_map %ledger (pair address nat) nat)))
                (pair (pair (big_map %metadata string bytes) (big_map %operators (pair address nat) unit))
                      (pair (bool %paused) (option %pending_admin address))))
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))) ;
  code { NIL operation ;
         SWAP ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DUP ;
                         CDR ;
                         PUSH nat 0 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         ITER { SWAP ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                DIG 2 ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                CAR ;
                                CAR ;
                                DIG 2 ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CDR ;
                                DUG 2 ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH nat 0 } {} ;
                                ADD } ;
                         DIG 2 ;
                         DROP ;
                         COMPARE ;
                         LT ;
                         IF { PUSH int 0 ; FAILWITH } {} ;
                         SWAP ;
                         PAIR }
                       { DIG 2 ;
                         DROP ;
                         DUP ;
                         CAR ;
                         MAP { DUP ;
                               { DIP 3 { DUP } ; DIG 4 } ;
                               { DIP 2 { DUP } ; DIG 3 } ;
                               CDR ;
                               DIG 3 ;
                               CAR ;
                               DIG 2 ;
                               CAR ;
                               CAR ;
                               CDR ;
                               CDR ;
                               DUG 2 ;
                               PAIR ;
                               GET ;
                               IF_NONE { PUSH nat 0 } {} ;
                               SWAP ;
                               PAIR } ;
                         DIG 2 ;
                         NIL operation ;
                         DIG 3 ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 4 ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         PAIR } }
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         IF_NONE { PUSH string "NO_PENDING_ADMIN" ; FAILWITH } {} ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { DUP ;
                              CDR ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              CAR ;
                              CDR ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              CAR ;
                              CAR ;
                              CDR ;
                              DIG 3 ;
                              CAR ;
                              CAR ;
                              CAR ;
                              CDR ;
                              SENDER ;
                              PAIR ;
                              PAIR ;
                              PAIR ;
                              PAIR ;
                              DUP ;
                              CDR ;
                              NONE address ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              CAR ;
                              CDR ;
                              CDR ;
                              CAR ;
                              PAIR ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              CAR ;
                              CDR ;
                              CAR ;
                              PAIR ;
                              DIG 2 ;
                              CAR ;
                              CAR ;
                              PAIR ;
                              PAIR }
                            { PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         PAIR }
                       { PUSH bool True ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         DIG 3 ;
                         SENDER ;
                         PAIR ;
                         NONE nat ;
                         SWAP ;
                         UPDATE ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { PUSH bool True ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         ITER { SWAP ;
                                DUP ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CAR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CDR ;
                                SENDER ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} ;
                                PUSH nat 0 ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                { { DUP ; CAR ; DIP { CDR } } } ;
                                DIG 2 ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CDR ;
                                DUG 2 ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH nat 0 } {} ;
                                COMPARE ;
                                NEQ ;
                                IF { PUSH string "RECEIVER BALANCE IS NOT 0" ; FAILWITH } {} ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CDR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                CDR ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CDR ;
                                PUSH nat 1 ;
                                { DIP 6 { DUP } ; DIG 7 } ;
                                SWAP ;
                                SOME ;
                                SWAP ;
                                UPDATE ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CAR ;
                                PAIR ;
                                DIG 4 ;
                                CAR ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                PAIR ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                CDR ;
                                CDR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                CDR ;
                                CAR ;
                                CDR ;
                                UNIT ;
                                { DIP 6 { DUP } ; DIG 7 } ;
                                SWAP ;
                                SOME ;
                                SWAP ;
                                UPDATE ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                CAR ;
                                CDR ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                CDR ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CDR ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                CAR ;
                                CAR ;
                                CDR ;
                                CAR ;
                                PUSH nat 1 ;
                                DIG 6 ;
                                SUB ;
                                ABS ;
                                DIG 6 ;
                                CDR ;
                                SENDER ;
                                PAIR ;
                                SWAP ;
                                SOME ;
                                SWAP ;
                                UPDATE ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                CAR ;
                                CAR ;
                                PAIR ;
                                PAIR ;
                                PAIR } ;
                         SWAP ;
                         PAIR }
                       { PUSH bool True ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "failed assertion" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CDR ;
                         MEM ;
                         IF { PUSH string "TOKEN_ID_EXISTS" ; FAILWITH } {} ;
                         PUSH nat 0 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "ZERO_AMOUNT" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         CAR ;
                         SWAP ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         CDR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         DIG 2 ;
                         CAR ;
                         PAIR ;
                         DUP ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CAR ;
                         { DIP 5 { DUP } ; DIG 6 } ;
                         CAR ;
                         CDR ;
                         { DIP 6 { DUP } ; DIG 7 } ;
                         CDR ;
                         CDR ;
                         DIG 7 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "NOT_ADMIN" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "NOT_ADMIN" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         SOME ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } } } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "NOT_ADMIN" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         DIG 3 ;
                         DIG 4 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "NOT_ADMIN" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         CDR ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         CAR ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         CDR ;
                         DIG 5 ;
                         CAR ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR } }
                   { IF_LEFT
                       { PUSH bool True ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                         ITER { DUP ;
                                DUG 2 ;
                                CAR ;
                                SENDER ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                COMPARE ;
                                NEQ ;
                                IF { PUSH string "FA2_NOT_OWNER" ; FAILWITH } {} ;
                                SWAP ;
                                DIG 2 ;
                                CDR ;
                                ITER { SWAP ;
                                       DUP ;
                                       CAR ;
                                       CDR ;
                                       CAR ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       GET 3 ;
                                       { DIP 4 { DUP } ; DIG 5 } ;
                                       PAIR ;
                                       MEM ;
                                       NOT ;
                                       IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} ;
                                       PUSH nat 1 ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       GET 4 ;
                                       COMPARE ;
                                       NEQ ;
                                       IF { PUSH string "AMOUNT MUST BE 1" ; FAILWITH } {} ;
                                       PUSH nat 1 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       GET 3 ;
                                       { DIP 5 { DUP } ; DIG 6 } ;
                                       DIG 2 ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CDR ;
                                       DUG 2 ;
                                       PAIR ;
                                       GET ;
                                       IF_NONE { PUSH nat 0 } {} ;
                                       COMPARE ;
                                       LT ;
                                       IF { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                                       PUSH nat 0 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       GET 3 ;
                                       { DIP 4 { DUP } ; DIG 5 } ;
                                       CAR ;
                                       DIG 2 ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CDR ;
                                       DUG 2 ;
                                       PAIR ;
                                       GET ;
                                       IF_NONE { PUSH nat 0 } {} ;
                                       COMPARE ;
                                       GT ;
                                       IF { PUSH string "RECEIVER BALANCE IS NOT 0" ; FAILWITH } {} ;
                                       DUP ;
                                       CDR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CAR ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CDR ;
                                       { DIP 4 { DUP } ; DIG 5 } ;
                                       GET 3 ;
                                       { DIP 6 { DUP } ; DIG 7 } ;
                                       PAIR ;
                                       NONE nat ;
                                       SWAP ;
                                       UPDATE ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CAR ;
                                       PAIR ;
                                       DIG 3 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       PAIR ;
                                       DUP ;
                                       CDR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CAR ;
                                       CDR ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       CDR ;
                                       CAR ;
                                       CDR ;
                                       { DIP 4 { DUP } ; DIG 5 } ;
                                       GET 3 ;
                                       { DIP 6 { DUP } ; DIG 7 } ;
                                       PAIR ;
                                       NONE unit ;
                                       SWAP ;
                                       UPDATE ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       CDR ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       DIG 2 ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       DUP ;
                                       CDR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CAR ;
                                       CDR ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CDR ;
                                       PUSH nat 1 ;
                                       { DIP 5 { DUP } ; DIG 6 } ;
                                       GET 3 ;
                                       DIG 6 ;
                                       CAR ;
                                       PAIR ;
                                       SWAP ;
                                       SOME ;
                                       SWAP ;
                                       UPDATE ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       CAR ;
                                       CAR ;
                                       CDR ;
                                       CAR ;
                                       PAIR ;
                                       DIG 3 ;
                                       CAR ;
                                       CAR ;
                                       CAR ;
                                       PAIR ;
                                       PAIR ;
                                       PAIR } ;
                                SWAP ;
                                DROP } ;
                         SWAP ;
                         PAIR }
                       { DROP 3 ; PUSH string "FA2_TX_DENIED" ; FAILWITH } } }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CDR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF {} { PUSH string "failed assertion" ; FAILWITH } ;
                 ITER { SWAP ;
                        DUP ;
                        CDR ;
                        { DIP 2 { DUP } ; DIG 3 } ;
                        CAR ;
                        GET ;
                        IF_NONE { PUSH string "TOKEN_NOT_FOUND" ; FAILWITH } {} ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        CDR ;
                        SWAP ;
                        { DIP 3 { DUP } ; DIG 4 } ;
                        CDR ;
                        UPDATE 2 ;
                        DIG 3 ;
                        CAR ;
                        SWAP ;
                        SOME ;
                        SWAP ;
                        UPDATE ;
                        SWAP ;
                        CAR ;
                        PAIR } ;
                 SWAP ;
                 PAIR } } } }
