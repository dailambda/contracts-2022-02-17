{ storage
    (pair (pair (pair (set %all_tokens nat) (big_map %ledger (pair address nat) nat))
                (pair (address %manager) (big_map %metadata string bytes)))
          (pair (pair (big_map %minters address unit)
                      (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit))
                (pair (bool %paused)
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (big_map %token_metadata_updater address unit))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (pair %mint
                   (pair (address %address) (nat %amount))
                   (pair (nat %token_id) (map %token_info string bytes))))
            (or (address %set_manager)
                (or (pair %set_metadata (string %k) (bytes %v)) (bool %set_pause))))
        (or (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (list %update_minters (or (address %add_minter) (address %remove_minter))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (or (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes)))
                    (list %update_token_metadata_updater
                       (or (address %add_address) (address %remove_address))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (pair (pair address nat) (pair nat (map string bytes))))
                            (or address (or (pair string bytes) bool)))
                        (or (or (list (pair address (list (pair address (pair nat nat))))) (list (or address address)))
                            (or (list (or (pair address (pair address nat)) (pair address (pair address nat))))
                                (or (list (pair nat (map string bytes))) (list (or address address))))))
                    (pair (pair (pair (set nat) (big_map (pair address nat) nat))
                                (pair address (big_map string bytes)))
                          (pair (pair (big_map address unit) (big_map (pair address (pair address nat)) unit))
                                (pair bool (pair (big_map nat (pair nat (map string bytes))) (big_map address unit)))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           CAR ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                CAR ;
                                CAR ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 427 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     SENDER ;
                     MEM ;
                     IF {} { PUSH string "FA2_NOT_minter" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     CDR ;
                     PUSH nat 1 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NFT-asset: amount <> 1" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     MEM ;
                     IF { PUSH string "NFT-asset: cannot mint twice same token" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     MEM ;
                     IF { SWAP ;
                          UNPAIR ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          DUP ;
                          DIG 5 ;
                          DUP ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 7 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 559 ; FAILWITH } {} ;
                          DUP 7 ;
                          CAR ;
                          CDR ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          SWAP }
                        { SWAP ;
                          UNPAIR ;
                          UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          DUP 5 ;
                          CAR ;
                          CDR ;
                          SOME ;
                          DIG 5 ;
                          DUP ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 7 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          SWAP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     MEM ;
                     IF { DROP }
                        { SWAP ;
                          UNPAIR ;
                          UNPAIR ;
                          UNPAIR ;
                          PUSH bool True ;
                          DUP 6 ;
                          GET 3 ;
                          UPDATE ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          DUP ;
                          GET 7 ;
                          DIG 2 ;
                          DUP ;
                          GET 4 ;
                          SWAP ;
                          DUP ;
                          DUG 4 ;
                          GET 3 ;
                          PAIR ;
                          SOME ;
                          DIG 3 ;
                          GET 3 ;
                          UPDATE ;
                          UPDATE 7 } ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_MANAGER" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF { PUSH bool True }
                            { SWAP ; DUP ; DUG 2 ; CAR ; GET 3 ; SENDER ; COMPARE ; EQ } ;
                         IF {} { PUSH string "FA2_NOT_minter_OR_MANAGER" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 5 ;
                         CDR ;
                         SOME ;
                         DIG 5 ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF { PUSH bool True }
                            { SWAP ; DUP ; DUG 2 ; CAR ; GET 3 ; SENDER ; COMPARE ; EQ } ;
                         IF {} { PUSH string "FA2_NOT_minter_OR_MANAGER" ; FAILWITH } ;
                         UPDATE 5 } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   GET 3 ;
                                   CAR ;
                                   SENDER ;
                                   MEM ;
                                   IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP 4 ;
                                   GET 7 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        DUP 5 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 405 ; FAILWITH } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        DUP ;
                                        DUP 6 ;
                                        GET 3 ;
                                        DUP 8 ;
                                        CAR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 409 ; FAILWITH } { DROP } ;
                                        DUP 6 ;
                                        GET 4 ;
                                        DIG 9 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        DUP 8 ;
                                        GET 3 ;
                                        DUP 10 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 409 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 409 ; FAILWITH } {} ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        CAR ;
                                        CAR ;
                                        CDR ;
                                        SWAP ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 3 ;
                                        CAR ;
                                        PAIR ;
                                        MEM ;
                                        IF { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             DUP ;
                                             DIG 5 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             DUP ;
                                             DUG 7 ;
                                             CAR ;
                                             PAIR ;
                                             DUP ;
                                             DUG 2 ;
                                             GET ;
                                             IF_NONE { PUSH int 412 ; FAILWITH } {} ;
                                             DIG 6 ;
                                             GET 4 ;
                                             ADD ;
                                             SOME ;
                                             SWAP ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 }
                                           { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             DUP 5 ;
                                             GET 4 ;
                                             SOME ;
                                             DIG 5 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             CAR ;
                                             PAIR ;
                                             UPDATE ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 } }
                                      { DROP } } ;
                            DROP } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_MANAGER" ; FAILWITH } ;
                     DUP ;
                     ITER { IF_LEFT
                              { DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                NONE unit ;
                                DIG 5 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; GET 3 ; CAR ; SENDER ; MEM } ;
                                IF {} { PUSH string "FA2_NOT_minter_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; GET 3 ; CAR ; SENDER ; MEM } ;
                                IF {} { PUSH string "FA2_NOT_minter_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                SWAP ;
                                NONE unit ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 8 ;
                         SENDER ;
                         MEM ;
                         IF {} { PUSH int 597 ; FAILWITH } ;
                         DUP ;
                         ITER { DIG 2 ;
                                DUP ;
                                GET 7 ;
                                DIG 2 ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 4 ;
                                CAR ;
                                PAIR ;
                                SOME ;
                                DIG 3 ;
                                CAR ;
                                UPDATE ;
                                UPDATE 7 ;
                                SWAP } ;
                         DROP }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         GET 3 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH int 607 ; FAILWITH } ;
                         DUP ;
                         ITER { IF_LEFT
                                  { DIG 2 ;
                                    DUP ;
                                    GET 8 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    UPDATE ;
                                    UPDATE 8 ;
                                    SWAP }
                                  { DIG 2 ; DUP ; GET 8 ; NONE unit ; DIG 3 ; UPDATE ; UPDATE 8 ; SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
