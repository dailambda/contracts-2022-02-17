{ parameter
    (or (or (or (address %setAdmin) (unit %acceptAdminCandidate))
            (or (pair %grantRole (string %irole) (list %iaddrs address))
                (pair %revokeRole (string %irole) (list %iaddrs address))))
        (or (or (bytes %setMetadataUri) (address %setArchetype))
            (or (or (address %setStablecoin) (bool %setFlatFees))
                (or (bool %setSupportFees)
                    (pair %mintWithPayment
                       (nat %archetypeId)
                       (pair (nat %serialNumber)
                             (pair (address %vault) (pair (nat %amount) (pair (key %pk) (signature %sig)))))))))) ;
  storage
    (pair (address %admin)
          (pair (address %archetype)
                (pair (address %stablecoin)
                      (pair (option %adminCandidate address)
                            (pair (map %role string (set address))
                                  (pair (bool %flat_fees) (pair (bool %support_fees) (big_map %metadata string bytes)))))))) ;
  code { LAMBDA
           (pair (map string (set address)) (pair string address))
           bool
           { UNPAIR 3 ;
             PUSH unit Unit ;
             DUP 2 ;
             DUP 4 ;
             GET ;
             IF_NONE
               { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
               {} ;
             DUP 5 ;
             MEM ;
             SWAP ;
             DROP ;
             DUG 3 ;
             DROP 3 } ;
         LAMBDA
           (pair (map string (set address))
                 (pair string (lambda (pair (map string (set address)) (pair string address)) bool)))
           bool
           { UNPAIR 3 ;
             PUSH unit Unit ;
             DUP 4 ;
             SENDER ;
             PUSH string "minter" ;
             PAIR ;
             DUP 4 ;
             PAIR ;
             EXEC ;
             IF { PUSH bool True ; SWAP ; DROP }
                { PUSH string "Must be a minter" ; FAILWITH } ;
             DUG 3 ;
             DROP 3 } ;
         NIL operation ;
         DIG 3 ;
         UNPAIR ;
         DIP { UNPAIR 8 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     SOME ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR }
                   { DROP ;
                     DUP 4 ;
                     IF_NONE
                       { PUSH string "adminCandidate is none" ; FAILWITH }
                       { DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "adminCandidate is not caller" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         NONE address ;
                         DIP { DIG 4 ; DROP } ;
                         DUG 4 ;
                         DROP } ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { UNPAIR ;
                     SWAP ;
                     DUP 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP 8 ;
                            DUP 9 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE
                              { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                              {} ;
                            PUSH bool True ;
                            DUP 4 ;
                            UPDATE ;
                            SOME ;
                            DUP 5 ;
                            UPDATE ;
                            DIP { DIG 7 ; DROP } ;
                            DUG 7 ;
                            DROP } ;
                     DROP 2 ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     DUP 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP 8 ;
                            DUP 9 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE
                              { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                              {} ;
                            PUSH bool False ;
                            DUP 4 ;
                            UPDATE ;
                            SOME ;
                            DUP 5 ;
                            UPDATE ;
                            DIP { DIG 7 ; DROP } ;
                            DUG 7 ;
                            DROP } ;
                     DROP 2 ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DIG 8 ;
                     DUP 2 ;
                     SOME ;
                     PUSH string "" ;
                     UPDATE ;
                     DUG 8 ;
                     DROP ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR }
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     DIP { DIG 2 ; DROP } ;
                     DUG 2 ;
                     DROP ;
                     PAIR 8 ;
                     DIG 1 ;
                     PAIR } }
               { IF_LEFT
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 3 ; DROP } ;
                         DUG 3 ;
                         DROP ;
                         PAIR 8 ;
                         DIG 1 ;
                         PAIR }
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 6 ; DROP } ;
                         DUG 6 ;
                         DROP ;
                         PAIR 8 ;
                         DIG 1 ;
                         PAIR } }
                   { IF_LEFT
                       { DUP 2 ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 7 ; DROP } ;
                         DUG 7 ;
                         DROP ;
                         PAIR 8 ;
                         DIG 1 ;
                         PAIR }
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 16 ;
                         DUP 18 ;
                         PUSH string "minter" ;
                         PAIR ;
                         DUP 13 ;
                         PAIR ;
                         EXEC ;
                         NOT ;
                         IF { PUSH string "r0" ; PUSH string "InvalidCondition" ; PAIR ; FAILWITH } {} ;
                         DUP 15 ;
                         DUP 10 ;
                         CONTRACT %transfer_gasless
                           (pair bool (pair bool (list (pair key (pair (list (pair address (pair nat nat))) signature))))) ;
                         IF_NONE
                           { PUSH string "transfer_gasless" ;
                             PUSH string "EntryNotFound" ;
                             PAIR ;
                             FAILWITH }
                           {} ;
                         PUSH mutez 0 ;
                         NIL (pair key (pair (list (pair address (pair nat nat))) signature)) ;
                         DUP 5 ;
                         NIL (pair address (pair nat nat)) ;
                         DUP 9 ;
                         PUSH nat 0 ;
                         PAIR ;
                         DUP 11 ;
                         PAIR ;
                         CONS ;
                         PAIR ;
                         DUP 7 ;
                         PAIR ;
                         CONS ;
                         DUP 17 ;
                         PAIR ;
                         DUP 16 ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         DIP { DIG 14 ; DROP } ;
                         DUG 14 ;
                         DUP 15 ;
                         DUP 9 ;
                         CONTRACT %mint (pair address (pair nat nat)) ;
                         IF_NONE
                           { PUSH string "mint" ; PUSH string "EntryNotFound" ; PAIR ; FAILWITH }
                           {} ;
                         PUSH mutez 0 ;
                         DUP 8 ;
                         DUP 10 ;
                         PAIR ;
                         DUP 6 ;
                         HASH_KEY ;
                         IMPLICIT_ACCOUNT ;
                         ADDRESS ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS ;
                         DIP { DIG 14 ; DROP } ;
                         DUG 14 ;
                         DROP 6 ;
                         PAIR 8 ;
                         DIG 1 ;
                         PAIR } } } } ;
         DIP { DROP 2 } } }
