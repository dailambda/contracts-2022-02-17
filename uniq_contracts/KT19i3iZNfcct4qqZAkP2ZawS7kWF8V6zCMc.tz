{ storage
    (pair (pair (pair (int %background) (map %canvas (pair int int) int))
                (pair (int %colors) (map %contributions address int)))
          (pair (pair (mutez %cost) (address %owner)) (pair (bool %paused) (int %size)))) ;
  parameter
    (or (or (or (unit %pauseContract) (int %setColors))
            (or (mutez %setCost) (pair %setPixel (int %color) (pair (int %xpos) (int %ypos)))))
        (or (or (int %setSize) (address %transferOwnership))
            (or (unit %unpauseContract) (mutez %withdraw)))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     DUP ;
                     PUSH int 0 ;
                     COMPARE ;
                     LT ;
                     IF {} { PUSH string "Colours must be greater than 0" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     IF { PUSH string "Pixel Place is currently paused." ; FAILWITH } {} ;
                     DUP ;
                     GET 3 ;
                     PUSH int 0 ;
                     SWAP ;
                     COMPARE ;
                     GE ;
                     IF { PUSH int 1 ;
                          DUP 3 ;
                          GET 6 ;
                          SUB ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 3 ;
                          COMPARE ;
                          LE }
                        { PUSH bool False } ;
                     IF {} { PUSH string "X position out of range" ; FAILWITH } ;
                     DUP ;
                     GET 4 ;
                     PUSH int 0 ;
                     SWAP ;
                     COMPARE ;
                     GE ;
                     IF { PUSH int 1 ;
                          DUP 3 ;
                          GET 6 ;
                          SUB ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 4 ;
                          COMPARE ;
                          LE }
                        { PUSH bool False } ;
                     IF {} { PUSH string "Y position out of range" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     PUSH int 0 ;
                     SWAP ;
                     COMPARE ;
                     GE ;
                     IF { PUSH int 1 ;
                          DUP 3 ;
                          CAR ;
                          GET 3 ;
                          SUB ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          COMPARE ;
                          LE }
                        { PUSH bool False } ;
                     IF {} { PUSH string "Color out of range" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CAR ;
                     AMOUNT ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Invalid amount of XTZ" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     DUP 5 ;
                     CAR ;
                     SOME ;
                     DIG 5 ;
                     DUP ;
                     GET 4 ;
                     SWAP ;
                     GET 3 ;
                     PAIR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     CAR ;
                     GET 4 ;
                     SENDER ;
                     MEM ;
                     IF { UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          DUP ;
                          SENDER ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 32 ; FAILWITH } {} ;
                          PUSH int 1 ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR }
                        { UNPAIR ;
                          UNPAIR ;
                          SWAP ;
                          UNPAIR ;
                          SWAP ;
                          PUSH (option int) (Some 1) ;
                          SENDER ;
                          UPDATE ;
                          SWAP ;
                          PAIR ;
                          SWAP ;
                          PAIR ;
                          PAIR } } } ;
             NIL operation }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     DUP ;
                     PUSH int 0 ;
                     COMPARE ;
                     LT ;
                     IF {} { PUSH string "Size must be greater than 0" ; FAILWITH } ;
                     UPDATE 6 }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } ;
                 NIL operation }
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     NIL operation }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {}
                        { PUSH string "You are not the owner of this Smart Contract" ; FAILWITH } ;
                     NIL operation ;
                     SENDER ;
                     CONTRACT unit ;
                     IF_NONE { PUSH int 40 ; FAILWITH } {} ;
                     DIG 2 ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS } } } ;
         PAIR } }
