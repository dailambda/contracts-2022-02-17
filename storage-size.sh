#!/bin/bash

set -e

for fn in storage/*
do
    adrs=`echo $fn | sed -e 's|storage/||'`
    (cd ../scaml; eval `opam env`; dune exec tools/storage/size2.exe ../new-contracts/contracts/$adrs.tz ../new-contracts/storage/$adrs)
done

