#!/bin/sh

set -e

for adrs in `cat $1`
do
    if [ ! -f "storage/$adrs" ]; then
	echo $adrs
	echo tezos-client get contract storage for $adrs
	if TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=y tezos-client get contract storage for $adrs > storage/$adrs.tmp; then
	    mv storage/$adrs.tmp storage/$adrs
	else
	    sleep 10
	fi
    fi
done

