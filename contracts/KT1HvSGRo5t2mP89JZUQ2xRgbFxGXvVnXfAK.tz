{ parameter
    (or (or (or (bytes %setMetadataUri) (pair %grantRole (string %irole) (list %iaddrs address)))
            (or (pair %revokeRole (string %irole) (list %iaddrs address))
                (pair %setMintingLimit (nat %id) (nat %lim))))
        (pair %validateMint (nat %id) (nat %serialNr))) ;
  storage
    (pair (address %admin)
          (pair (map %role string (set address))
                (pair (big_map %mintingLimits nat nat) (big_map %metadata string bytes)))) ;
  code { LAMBDA
           (pair (map string (set address)) (pair string address))
           bool
           { UNPAIR 3 ;
             PUSH unit Unit ;
             DUP 2 ;
             DUP 4 ;
             GET ;
             IF_NONE
               { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
               {} ;
             DUP 5 ;
             MEM ;
             SWAP ;
             DROP ;
             DUG 3 ;
             DROP 3 } ;
         LAMBDA
           (pair (map string (set address))
                 (pair string
                       (pair address (lambda (pair (map string (set address)) (pair string address)) bool))))
           bool
           { UNPAIR 4 ;
             PUSH unit Unit ;
             DUP 5 ;
             DUP 5 ;
             PUSH string "minter" ;
             PAIR ;
             DUP 4 ;
             PAIR ;
             EXEC ;
             SWAP ;
             DROP ;
             DUG 4 ;
             DROP 4 } ;
         LAMBDA
           (pair (map string (set address))
                 (pair string
                       (pair (lambda
                                (pair (map string (set address))
                                      (pair string
                                            (pair address (lambda (pair (map string (set address)) (pair string address)) bool))))
                                bool)
                             (lambda (pair (map string (set address)) (pair string address)) bool))))
           bool
           { UNPAIR 4 ;
             PUSH unit Unit ;
             DUP 4 ;
             DUP 6 ;
             SENDER ;
             PAIR ;
             PUSH string "minter" ;
             PAIR ;
             DUP 4 ;
             PAIR ;
             EXEC ;
             IF { PUSH bool True ; SWAP ; DROP }
                { PUSH string "Must be a minter" ; FAILWITH } ;
             DUG 4 ;
             DROP 4 } ;
         DIG 3 ;
         UNPAIR ;
         DIP { UNPAIR 4 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DIG 4 ;
                     DUP 2 ;
                     SOME ;
                     PUSH string "" ;
                     UPDATE ;
                     DUG 4 ;
                     DROP ;
                     PAIR 4 ;
                     NIL operation ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     DUP 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP 5 ;
                            DUP 6 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE
                              { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                              {} ;
                            PUSH bool True ;
                            DUP 4 ;
                            UPDATE ;
                            SOME ;
                            DUP 5 ;
                            UPDATE ;
                            DIP { DIG 4 ; DROP } ;
                            DUG 4 ;
                            DROP } ;
                     DROP 2 ;
                     PAIR 4 ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { UNPAIR ;
                     SWAP ;
                     DUP 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP 5 ;
                            DUP 6 ;
                            DUP 5 ;
                            GET ;
                            IF_NONE
                              { PUSH string "role" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
                              {} ;
                            PUSH bool False ;
                            DUP 4 ;
                            UPDATE ;
                            SOME ;
                            DUP 5 ;
                            UPDATE ;
                            DIP { DIG 4 ; DROP } ;
                            DUG 4 ;
                            DROP } ;
                     DROP 2 ;
                     PAIR 4 ;
                     NIL operation ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     DUP 7 ;
                     DUP 10 ;
                     DUP 10 ;
                     PAIR ;
                     PUSH string "minter" ;
                     PAIR ;
                     DUP 6 ;
                     PAIR ;
                     EXEC ;
                     NOT ;
                     IF { PUSH string "r0" ; PUSH string "InvalidCondition" ; PAIR ; FAILWITH } {} ;
                     DUP 5 ;
                     DUP 3 ;
                     MEM ;
                     IF { PUSH string "MintingValidator: minting limit already set" ; FAILWITH }
                        {} ;
                     DUP 5 ;
                     DUP 3 ;
                     MEM ;
                     IF { PUSH string "mintingLimits" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                        { DUP 5 ; DUP 2 ; SOME ; DUP 4 ; UPDATE ; DIP { DIG 4 ; DROP } ; DUG 4 } ;
                     DROP 2 ;
                     PAIR 4 ;
                     NIL operation ;
                     PAIR } } }
           { UNPAIR ;
             SWAP ;
             DUP 5 ;
             DUP 3 ;
             MEM ;
             NOT ;
             IF { PUSH string "MintingValidator: serial number out of bounds" ; FAILWITH }
                {} ;
             DUP 5 ;
             DUP 3 ;
             GET ;
             IF_NONE
               { PUSH string "mintingLimits" ; PUSH string "AssetNotFound" ; PAIR ; FAILWITH }
               {} ;
             DUP 2 ;
             COMPARE ;
             LE ;
             PUSH nat 0 ;
             DUP 3 ;
             COMPARE ;
             GT ;
             AND ;
             NOT ;
             IF { PUSH string "MintingValidator: serial number out of bounds" ; FAILWITH }
                {} ;
             DROP 2 ;
             PAIR 4 ;
             NIL operation ;
             PAIR } ;
         DIP { DROP 3 } } }
