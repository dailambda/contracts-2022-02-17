{ parameter
    (or (or (or (address %addMinter) (pair %approve (address %spender) (nat %value)))
            (or (pair %burn (address %spender) (nat %value))
                (pair %getAllowance (pair (address %owner) (address %spender)) (contract nat))))
        (or (or (pair %getBalance (address %owner) (contract nat))
                (pair %getTotalSupply unit (contract nat)))
            (or (pair %mint (address %spender) (nat %value))
                (pair %transfer (address %from) (pair (address %to) (nat %value)))))) ;
  storage
    (pair (pair (pair (big_map %ledger address (pair (map %allowances address nat) (nat %balance)))
                      (big_map %metadata string bytes))
                (pair (address %minter) (address %owner)))
          (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                (nat %totalSupply))) ;
  code { NIL operation ;
         LAMBDA
           (pair address
                 (pair (pair (pair (big_map address (pair (map address nat) nat)) (big_map string bytes))
                             (pair address address))
                       (pair (big_map nat (pair nat (map string bytes))) nat)))
           (pair (map address nat) nat)
           { { { DUP ; CAR ; DIP { CDR } } } ;
             SWAP ;
             CAR ;
             CAR ;
             CAR ;
             SWAP ;
             GET ;
             IF_NONE { PUSH nat 0 ; EMPTY_MAP address nat ; PAIR } {} } ;
         LAMBDA
           (pair (pair (pair (map address nat) nat) address)
                 (pair (pair (pair (big_map address (pair (map address nat) nat)) (big_map string bytes))
                             (pair address address))
                       (pair (big_map nat (pair nat (map string bytes))) nat)))
           nat
           { CAR ;
             { { DUP ; CAR ; DIP { CDR } } } ;
             CAR ;
             SWAP ;
             GET ;
             IF_NONE { PUSH nat 0 } {} } ;
         DIG 3 ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP ;
                          PUSH string "You must be the owner of the contract to add minter" ;
                          FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          CAR ;
                          CDR ;
                          CDR ;
                          DIG 2 ;
                          PAIR ;
                          DIG 2 ;
                          CAR ;
                          CAR ;
                          PAIR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR }
                   { { { DUP ; CAR ; DIP { CDR } } } ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     SENDER ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     PAIR ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     PUSH nat 0 ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     COMPARE ;
                     GT ;
                     PUSH nat 0 ;
                     DIG 2 ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "UnsafeAllowanceChange" ; FAILWITH } {} ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CDR ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CAR ;
                     CDR ;
                     { DIP 5 { DUP } ; DIG 6 } ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 6 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CDR ;
                     DIG 5 ;
                     CAR ;
                     DIG 7 ;
                     DIG 7 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     PAIR ;
                     SOME ;
                     SENDER ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } }
               { DIG 4 ;
                 DROP ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DROP 2 ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SENDER ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CAR ;
                     CDR ;
                     CAR ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ;
                          PUSH string "You must be the owner of the contract to burn tokens" ;
                          FAILWITH }
                        { { DIP 2 { DUP } ; DIG 3 } ;
                          CAR ;
                          CAR ;
                          CAR ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH nat 0 ; EMPTY_MAP address nat ; PAIR } {} ;
                          DUP ;
                          CDR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          COMPARE ;
                          GT ;
                          IF { PUSH string "Owner balance is too low" ; FAILWITH } {} ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          SUB ;
                          ABS ;
                          SWAP ;
                          CAR ;
                          PAIR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          CDR ;
                          { DIP 4 { DUP } ; DIG 5 } ;
                          CAR ;
                          CDR ;
                          { DIP 5 { DUP } ; DIG 6 } ;
                          CAR ;
                          CAR ;
                          CDR ;
                          DIG 6 ;
                          CAR ;
                          CAR ;
                          CAR ;
                          DIG 4 ;
                          DIG 5 ;
                          SWAP ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CDR ;
                          SUB ;
                          ABS ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CAR ;
                          PAIR ;
                          SWAP ;
                          CAR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR }
                   { DUP ;
                     DUG 2 ;
                     CDR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     DIG 2 ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     DIG 3 ;
                     PAIR ;
                     DIG 5 ;
                     SWAP ;
                     EXEC ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     DIG 4 ;
                     DIG 2 ;
                     PAIR ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DIG 2 ;
                     NIL operation ;
                     DIG 3 ;
                     PUSH mutez 0 ;
                     DIG 4 ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } } }
           { IF_LEFT
               { DIG 2 ;
                 DIG 4 ;
                 DROP 2 ;
                 IF_LEFT
                   { { { DUP ; CAR ; DIP { CDR } } } ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     SWAP ;
                     PAIR ;
                     DIG 3 ;
                     SWAP ;
                     EXEC ;
                     DIG 2 ;
                     NIL operation ;
                     DIG 3 ;
                     PUSH mutez 0 ;
                     DIG 4 ;
                     CDR ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR }
                   { DIG 2 ;
                     DROP ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     DIG 2 ;
                     PUSH mutez 0 ;
                     DIG 4 ;
                     CDR ;
                     CDR ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } }
               { IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DIG 4 ;
                     DROP 3 ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SENDER ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CAR ;
                     CDR ;
                     CAR ;
                     COMPARE ;
                     NEQ ;
                     IF { DROP 2 ;
                          PUSH string "You must be the minter of the contract to mint tokens" ;
                          FAILWITH }
                        { { DIP 2 { DUP } ; DIG 3 } ;
                          CAR ;
                          CAR ;
                          CAR ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH nat 0 ; EMPTY_MAP address nat ; PAIR } {} ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          ADD ;
                          SWAP ;
                          CAR ;
                          PAIR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          CDR ;
                          { DIP 4 { DUP } ; DIG 5 } ;
                          CAR ;
                          CDR ;
                          { DIP 5 { DUP } ; DIG 6 } ;
                          CAR ;
                          CAR ;
                          CDR ;
                          DIG 6 ;
                          CAR ;
                          CAR ;
                          CAR ;
                          DIG 4 ;
                          DIG 5 ;
                          SWAP ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          PAIR ;
                          PAIR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CDR ;
                          ADD ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          CAR ;
                          PAIR ;
                          SWAP ;
                          CAR ;
                          PAIR } ;
                     NIL operation ;
                     PAIR }
                   { DUP ;
                     DUG 2 ;
                     CDR ;
                     CDR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     DIG 2 ;
                     CAR ;
                     DIG 2 ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     PAIR ;
                     { DIP 6 { DUP } ; DIG 7 } ;
                     SWAP ;
                     EXEC ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     COMPARE ;
                     LT ;
                     IF { PUSH string "NotEnoughBalance" ; FAILWITH } {} ;
                     SENDER ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     COMPARE ;
                     NEQ ;
                     IF { { DIP 2 { DUP } ; DIG 3 } ;
                          SENDER ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          PAIR ;
                          PAIR ;
                          DIG 6 ;
                          SWAP ;
                          EXEC ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          COMPARE ;
                          LT ;
                          IF { PUSH string "NotEnoughAllowance" ; FAILWITH } {} ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          DIG 2 ;
                          CAR ;
                          { DIP 3 { DUP } ; DIG 4 } ;
                          DIG 3 ;
                          SUB ;
                          ABS ;
                          SOME ;
                          SENDER ;
                          UPDATE ;
                          PAIR }
                        { DIG 5 ; DROP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SUB ;
                     ABS ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CAR ;
                     CDR ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     DIG 4 ;
                     DIG 6 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     PAIR ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DIG 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     ADD ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     CDR ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 4 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     DIG 4 ;
                     DIG 5 ;
                     SWAP ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } } } } }
