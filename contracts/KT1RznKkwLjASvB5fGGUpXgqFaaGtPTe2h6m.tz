{ parameter
    (or (or (or (or (or (address %add_admin) (address %blacklist))
                    (or (pair %buy_tickets address (pair nat string))
                        (pair %buy_tickets_with_kolibri address (pair nat string))))
                (or (or (pair %create_ticket address string)
                        (pair %kolibri_ticket_price_oracle string (pair timestamp nat)))
                    (or (pair %redeem_tickets address (pair string nat)) (address %remove_pool))))
            (or (or (or (unit %reset_kolibri_tx_status) (address %update_admin))
                    (or (address %update_kolibri_address) (address %update_kolibri_recipient)))
                (or (or (nat %update_kolibri_ticket_price) (address %update_manager))
                    (or (list %update_pool_addresses (pair address string))
                        (address %update_storage_contract)))))
        (or (or (mutez %update_ticket_price) (string %update_ticket_types))
            (unit %withdraw_balance))) ;
  storage
    (pair (address %manager)
          (pair (set %admins address)
                (pair (big_map %pool_addresses address string)
                      (pair (big_map %blacklist address timestamp)
                            (pair (set %ticket_types string)
                                  (pair (mutez %ticket_price)
                                        (pair (int %ticket_validity)
                                              (pair (address %oracle)
                                                    (pair (address %kolibri)
                                                          (pair (nat %kolibri_ticket_price)
                                                                (pair (pair %tx_to_kolibri bool (option (pair address (pair address (pair nat string)))))
                                                                      (pair (address %kolibri_recipient) (address %storage_contract))))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ; DUP ; GET 3 ; DIG 2 ; PUSH bool True ; SWAP ; UPDATE ; UPDATE 3 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET 7 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET ;
                                  IF_NONE
                                    { SWAP ;
                                      DUP ;
                                      GET 7 ;
                                      NOW ;
                                      DIG 3 ;
                                      SWAP ;
                                      SOME ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 7 }
                                    { DROP ;
                                      SWAP ;
                                      DUP ;
                                      GET 7 ;
                                      DIG 2 ;
                                      NONE timestamp ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 7 } } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { DUP ;
                             CDR ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 7 ;
                             SENDER ;
                             MEM ;
                             IF { DROP 4 ; PUSH string "BLACKLISTED_ADDRESS" ; FAILWITH }
                                { DUP 4 ;
                                  GET 11 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  MUL ;
                                  AMOUNT ;
                                  COMPARE ;
                                  NEQ ;
                                  PUSH mutez 0 ;
                                  AMOUNT ;
                                  COMPARE ;
                                  NEQ ;
                                  AND ;
                                  IF { DROP 4 ; PUSH string "INCORRECT_AMOUNT" ; FAILWITH }
                                     { SELF_ADDRESS ;
                                       SENDER ;
                                       COMPARE ;
                                       NEQ ;
                                       PUSH mutez 0 ;
                                       AMOUNT ;
                                       COMPARE ;
                                       EQ ;
                                       AND ;
                                       IF { DROP 4 ; PUSH string "INVALID_SENDER" ; FAILWITH }
                                          { DUP 4 ;
                                            GET 21 ;
                                            CAR ;
                                            NOT ;
                                            SELF_ADDRESS ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            PUSH mutez 0 ;
                                            AMOUNT ;
                                            COMPARE ;
                                            EQ ;
                                            AND ;
                                            AND ;
                                            IF { DROP 4 ; PUSH string "INVALID_KOLIBRI_BUY" ; FAILWITH }
                                               { PUSH nat 0 ;
                                                 SWAP ;
                                                 COMPARE ;
                                                 EQ ;
                                                 IF { DROP 3 ; PUSH string "ZERO_TICKET_AMOUNT" ; FAILWITH }
                                                    { DUP 3 ;
                                                      GET 9 ;
                                                      SWAP ;
                                                      MEM ;
                                                      NOT ;
                                                      IF { DROP 2 ; PUSH string "INVALID_TICKET_TYPE" ; FAILWITH }
                                                         { UNPAIR ;
                                                           SWAP ;
                                                           UNPAIR ;
                                                           DUP 4 ;
                                                           GET 24 ;
                                                           CONTRACT %add_tickets
                                                             (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                                           IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                                           PUSH mutez 0 ;
                                                           DIG 4 ;
                                                           DIG 4 ;
                                                           DIG 4 ;
                                                           SWAP ;
                                                           PAIR ;
                                                           SWAP ;
                                                           PAIR ;
                                                           TRANSFER_TOKENS ;
                                                           SWAP ;
                                                           DUP ;
                                                           DUG 2 ;
                                                           GET 21 ;
                                                           CAR ;
                                                           SELF_ADDRESS ;
                                                           SENDER ;
                                                           COMPARE ;
                                                           EQ ;
                                                           PUSH mutez 0 ;
                                                           AMOUNT ;
                                                           COMPARE ;
                                                           EQ ;
                                                           AND ;
                                                           AND ;
                                                           IF { SWAP ;
                                                                NONE (pair address (pair address (pair nat string))) ;
                                                                PUSH bool False ;
                                                                PAIR ;
                                                                UPDATE 21 ;
                                                                NIL operation ;
                                                                DIG 2 ;
                                                                CONS ;
                                                                PAIR }
                                                              { SWAP ; NIL operation ; DIG 2 ; CONS ; PAIR } } } } } } } }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 21 ;
                             UNPAIR ;
                             NOT ;
                             IF { DROP ;
                                  DUP ;
                                  CAR ;
                                  SENDER ;
                                  SWAP ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 2 ; PUSH string "PLAYER_IS_NOT_SENDER" ; FAILWITH }
                                     { SELF_ADDRESS ;
                                       CONTRACT %kolibri_ticket_price_oracle (pair string (pair timestamp nat)) ;
                                       IF_NONE { PUSH string "NO_CONTRACT" ; FAILWITH } {} ;
                                       DUP 3 ;
                                       GET 15 ;
                                       CONTRACT %get (pair string (contract (pair string (pair timestamp nat)))) ;
                                       IF_NONE { PUSH string "NO_ORACLE" ; FAILWITH } {} ;
                                       PUSH mutez 0 ;
                                       DIG 2 ;
                                       PUSH string "XTZ-USD" ;
                                       PAIR ;
                                       TRANSFER_TOKENS ;
                                       DUG 2 ;
                                       SENDER ;
                                       PAIR ;
                                       SOME ;
                                       PUSH bool True ;
                                       PAIR ;
                                       UPDATE 21 ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } }
                                { SWAP ;
                                  DROP ;
                                  PUSH nat 0 ;
                                  DUP 3 ;
                                  GET 19 ;
                                  COMPARE ;
                                  EQ ;
                                  IF { DROP 2 ; PUSH string "UNKNOWN_KUSD_PRICE" ; FAILWITH }
                                     { IF_NONE { PUSH string "NO_TRANSFER_DETAILS" ; FAILWITH } {} ;
                                       UNPAIR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       CAR ;
                                       DUP 4 ;
                                       GET 17 ;
                                       CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                                       IF_NONE { PUSH string "UNKNOWN_KOLIBRI_TRANSFER" ; FAILWITH } {} ;
                                       DIG 2 ;
                                       DUP 5 ;
                                       GET 23 ;
                                       DUP 6 ;
                                       GET 19 ;
                                       DIG 4 ;
                                       MUL ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PUSH mutez 0 ;
                                       DIG 2 ;
                                       TRANSFER_TOKENS ;
                                       SELF %buy_tickets ;
                                       PUSH mutez 0 ;
                                       DIG 3 ;
                                       TRANSFER_TOKENS ;
                                       DIG 2 ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } } } } }
                   { IF_LEFT
                       { IF_LEFT
                           { UNPAIR ;
                             DUP 3 ;
                             GET 5 ;
                             SENDER ;
                             MEM ;
                             DUP 4 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             OR ;
                             IF { DUP 3 ;
                                  GET 24 ;
                                  CONTRACT %add_tickets
                                    (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                  IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                  PUSH mutez 0 ;
                                  DIG 2 ;
                                  DIG 3 ;
                                  PUSH nat 1 ;
                                  SWAP ;
                                  PAIR ;
                                  SWAP ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  SWAP ;
                                  NIL operation ;
                                  DIG 2 ;
                                  CONS ;
                                  PAIR }
                                { DROP 3 ; PUSH string "FORBIDDEN_OP" ; FAILWITH } }
                           { UNPAIR ;
                             SWAP ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 15 ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 4 ; PUSH string "NOT_FROM_ORACLE" ; FAILWITH }
                                { PUSH string "XTZ-USD" ;
                                  DIG 3 ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 3 ; PUSH string "WRONG_CURRENCY_PAIR" ; FAILWITH }
                                     { PUSH int 900 ;
                                       NOW ;
                                       SUB ;
                                       SWAP ;
                                       COMPARE ;
                                       LT ;
                                       IF { DROP 2 ; PUSH string "OLD_EXCHANGE_RATE" ; FAILWITH }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 21 ;
                                            CAR ;
                                            NOT ;
                                            IF { DROP 2 ; PUSH string "NO_ONGOING_TX" ; FAILWITH }
                                               { SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET 21 ;
                                                 CDR ;
                                                 IF_NONE
                                                   { PUSH string "UNKNOWN_BUY_PARAMS" ; FAILWITH }
                                                   { SELF %buy_tickets_with_kolibri ;
                                                     NIL operation ;
                                                     SWAP ;
                                                     PUSH mutez 0 ;
                                                     DIG 3 ;
                                                     CDR ;
                                                     TRANSFER_TOKENS ;
                                                     CONS } ;
                                                 DUP 3 ;
                                                 PUSH nat 1000000 ;
                                                 PUSH mutez 1 ;
                                                 DIG 5 ;
                                                 GET 11 ;
                                                 EDIV ;
                                                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                                 CAR ;
                                                 DIG 4 ;
                                                 MUL ;
                                                 MUL ;
                                                 UPDATE 19 ;
                                                 SWAP ;
                                                 PAIR } } } } } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 5 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "UNKNOWN_POOL" ; FAILWITH }
                                { UNPAIR ;
                                  SWAP ;
                                  UNPAIR ;
                                  DUP 4 ;
                                  GET 7 ;
                                  DUP 4 ;
                                  MEM ;
                                  IF { DROP 4 ; PUSH string "BLACKLISTED_PLAYER" ; FAILWITH }
                                     { DUP 4 ;
                                       GET 24 ;
                                       CONTRACT %sub_tickets
                                         (pair (address %user)
                                               (pair (string %ticket_type) (pair (nat %amount_to_sub) (int %ticket_expiry)))) ;
                                       IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                       PUSH mutez 0 ;
                                       DIG 4 ;
                                       DIG 3 ;
                                       DIG 4 ;
                                       DUP 6 ;
                                       GET 13 ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       TRANSFER_TOKENS ;
                                       SWAP ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } } }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ; DUP ; GET 5 ; DIG 2 ; NONE string ; SWAP ; UPDATE ; UPDATE 5 } ;
                             NIL operation ;
                             PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { DROP ;
                             DUP ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { NONE (pair address (pair address (pair nat string))) ;
                                  PUSH bool False ;
                                  PAIR ;
                                  UPDATE 21 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  GET 3 ;
                                  PUSH bool False ;
                                  SENDER ;
                                  UPDATE ;
                                  DIG 2 ;
                                  PUSH bool True ;
                                  SWAP ;
                                  UPDATE ;
                                  UPDATE 3 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 17 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 23 } ;
                             NIL operation ;
                             PAIR } } }
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 19 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UPDATE 1 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET 5 ;
                                  SWAP ;
                                  ITER { UNPAIR ;
                                         DUP 3 ;
                                         SWAP ;
                                         DUP ;
                                         DUG 2 ;
                                         GET ;
                                         IF_NONE
                                           { SWAP ; SOME ; SWAP ; UPDATE }
                                           { DROP ; DUG 2 ; SOME ; DIG 2 ; UPDATE } } ;
                                  UPDATE 5 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UPDATE 24 } ;
                             NIL operation ;
                             PAIR } } } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SENDER ;
                     MEM ;
                     NOT ;
                     IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UPDATE 11 } ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SENDER ;
                     MEM ;
                     NOT ;
                     IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                        { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 9 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          MEM ;
                          IF { SWAP ; DUP ; GET 9 ; DIG 2 ; PUSH bool False ; SWAP ; UPDATE ; UPDATE 9 }
                             { SWAP ; DUP ; GET 9 ; DIG 2 ; PUSH bool True ; SWAP ; UPDATE ; UPDATE 9 } } ;
                     NIL operation ;
                     PAIR } }
               { DROP ;
                 DUP ;
                 GET 3 ;
                 SENDER ;
                 MEM ;
                 NOT ;
                 IF { DROP ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                    { DUP ;
                      CAR ;
                      CONTRACT unit ;
                      IF_NONE
                        { PUSH string "UNKNOWN_MANAGER" ; FAILWITH }
                        { BALANCE ; UNIT ; TRANSFER_TOKENS } ;
                      SWAP ;
                      NIL operation ;
                      DIG 2 ;
                      CONS ;
                      PAIR } } } } }
