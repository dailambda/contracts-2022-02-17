{ parameter
    (or (list %transfer
           (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))
        (or (pair %balance_of
               (list %requests (pair (address %owner) (nat %token_id)))
               (contract %callback (list (pair (address %owner) (nat %token_id) (nat %balance)))))
            (list %update_operators
               (or (pair %add_operator (address %owner) (address %operator) (nat %token_id))
                   (pair %remove_operator (address %owner) (address %operator) (nat %token_id)))))) ;
  storage
    (pair (big_map %ledger (address :owner) (nat :balance))
          (big_map %operators_ledger (address :owner) (set :operators address))
          (nat :total_supply)
          (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))) ;
  code { UNPAIR ;
         IF_LEFT
           { DIP { DUP ; CAR @% } ;
             ITER { UNPAIR @% @% ;
                    SWAP ;
                    ITER { DIP { DUP 2 ;
                                 DUP 2 ;
                                 GET ;
                                 IF_NONE { PUSH nat 0 } {} ;
                                 RENAME @from_balance } ;
                           UNPAIR @% @% ;
                           SWAP ;
                           UNPAIR @to_id @to_amount ;
                           PUSH nat 0 ;
                           { COMPARE ; EQ ; IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } } ;
                           DUP 4 ;
                           SENDER ;
                           { COMPARE ;
                             NEQ ;
                             IF { DUP 6 ;
                                  GET 3 ;
                                  DUP 5 ;
                                  GET ;
                                  IF_NONE { EMPTY_SET address } {} ;
                                  SENDER ;
                                  MEM ;
                                  NOT ;
                                  IF { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } {} }
                                {} } ;
                           DUP ;
                           DIG 3 ;
                           SUB @new_from_balance ;
                           ISNAT ;
                           IF_NONE { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } {} ;
                           SOME ;
                           DUG 3 ;
                           DIP 2 { DUP ; DIP { UPDATE } } ;
                           DUP 4 ;
                           DUP 3 ;
                           GET ;
                           IF_NONE { PUSH nat 0 } {} ;
                           ADD @new_to_balance ;
                           SOME ;
                           SWAP ;
                           DIG 2 ;
                           DIP { UPDATE } } ;
                    DROP } ;
             SWAP ;
             { DUP ; CDR @%% ; DIP { CAR ; { DROP } } ; SWAP ; PAIR % %@ } ;
             NIL operation }
           { IF_LEFT
               { UNPAIR @% @% ;
                 DIP { NIL (pair address nat nat) ; DUP 3 ; CAR @% } ;
                 ITER { DUP ;
                        UNPAIR @% @% ;
                        SWAP ;
                        PUSH nat 0 ;
                        { COMPARE ;
                          NEQ ;
                          IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} } ;
                        DUP 3 ;
                        SWAP ;
                        GET ;
                        IF_NONE { PUSH nat 0 } {} ;
                        SWAP ;
                        UNPAIR @% @% ;
                        PAIR 3 ;
                        SWAP ;
                        DIP { CONS } } ;
                 DROP ;
                 PUSH mutez 0 ;
                 SWAP ;
                 TRANSFER_TOKENS ;
                 NIL operation ;
                 SWAP ;
                 CONS }
               { ITER { IF_LEFT { PUSH bool True } { PUSH bool False } ;
                        SWAP ;
                        UNPAIR 3 ;
                        DUP ;
                        SENDER ;
                        { COMPARE ; NEQ ; IF { PUSH string "FA2_NOT_OWNER" ; FAILWITH } {} } ;
                        DUP 5 ;
                        GET 3 ;
                        DUP 2 ;
                        GET ;
                        IF_NONE { EMPTY_SET address } {} ;
                        DIG 4 ;
                        DIG 3 ;
                        UPDATE ;
                        SOME ;
                        SWAP ;
                        DIP 2
                            { PUSH nat 0 ;
                              { COMPARE ;
                                NEQ ;
                                IF { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } {} } ;
                              DUP ;
                              GET 3 } ;
                        UPDATE ;
                        UPDATE 3 } ;
                 NIL operation } } ;
         PAIR } }
