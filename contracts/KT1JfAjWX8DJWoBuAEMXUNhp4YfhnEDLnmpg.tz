{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (or %assets
               (or (pair %balance_of
                      (list %requests (pair (address %owner) (nat %token_id)))
                      (contract %callback
                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                   (list %transfer
                      (pair (address %from_)
                            (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
               (list %update_operators
                  (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                      (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))
        (or (list %mint
               (pair (address %owner)
                     (list %tokens (pair (nat %token_id) (map %token_info string bytes)))))
            (unit %mint_freeze))) ;
  storage
    (pair (pair (pair (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))
                      (pair %assets
                         (pair (big_map %ledger nat address)
                               (big_map %operators (pair address (pair address nat)) unit))
                         (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))
                (pair (big_map %metadata string bytes) (bool %mint_freeze)))
          (nat %next_token_id)) ;
  code { PUSH string "FA2_TOKEN_UNDEFINED" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         DIG 3 ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { DIG 3 ;
                 DIG 4 ;
                 DROP 2 ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR }
                                { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SOME ;
                     SWAP ;
                     CAR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } ;
                 { { DUP ; CAR ; DIP { CDR } } } ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 DIG 4 ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR }
               { DIG 2 ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CAR ;
                 CDR ;
                 IF { PUSH string "PAUSED" ; FAILWITH } {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 SWAP ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         MAP { { DIP 2 { DUP } ; DIG 3 } ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               GET ;
                               IF_NONE
                                 { DROP ; { DIP 4 { DUP } ; DIG 5 } ; FAILWITH }
                                 { SWAP ;
                                   DUP ;
                                   CAR ;
                                   DIG 2 ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH nat 1 } { PUSH nat 0 } ;
                                   SWAP ;
                                   PAIR } } ;
                         DIG 2 ;
                         DIG 5 ;
                         DROP 2 ;
                         SWAP ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         PAIR ;
                         LAMBDA
                           (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                           unit
                           { { { DUP ; CAR ; DIP { CDR } } } ;
                             { { DUP ; CAR ; DIP { CDR } } } ;
                             DIG 2 ;
                             { { DUP ; CAR ; DIP { CDR } } } ;
                             { DIP 3 { DUP } ; DIG 4 } ;
                             { DIP 3 { DUP } ; DIG 4 } ;
                             COMPARE ;
                             EQ ;
                             IF { DROP 4 ; UNIT }
                                { DIG 3 ;
                                  PAIR ;
                                  DIG 2 ;
                                  PAIR ;
                                  MEM ;
                                  IF { UNIT } { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } } } ;
                         DUG 2 ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         SWAP ;
                         DIG 2 ;
                         ITER { DUP ;
                                DUG 2 ;
                                CDR ;
                                ITER { SWAP ;
                                       PUSH nat 0 ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       GET 4 ;
                                       COMPARE ;
                                       EQ ;
                                       IF { SWAP ; DROP }
                                          { PUSH nat 1 ;
                                            { DIP 2 { DUP } ; DIG 3 } ;
                                            GET 4 ;
                                            COMPARE ;
                                            NEQ ;
                                            IF { DROP 2 ; { DIP 5 { DUP } ; DIG 6 } ; FAILWITH }
                                               { DUP ;
                                                 { DIP 2 { DUP } ; DIG 3 } ;
                                                 GET 3 ;
                                                 GET ;
                                                 IF_NONE
                                                   { DROP 2 ; { DIP 6 { DUP } ; DIG 7 } ; FAILWITH }
                                                   { { DIP 3 { DUP } ; DIG 4 } ;
                                                     CAR ;
                                                     SWAP ;
                                                     DUP ;
                                                     DUG 2 ;
                                                     COMPARE ;
                                                     NEQ ;
                                                     IF { DROP 3 ; { DIP 5 { DUP } ; DIG 6 } ; FAILWITH }
                                                        { { DIP 4 { DUP } ; DIG 5 } ;
                                                          { DIP 3 { DUP } ; DIG 4 } ;
                                                          GET 3 ;
                                                          PAIR ;
                                                          SENDER ;
                                                          DIG 2 ;
                                                          PAIR ;
                                                          PAIR ;
                                                          { DIP 5 { DUP } ; DIG 6 } ;
                                                          SWAP ;
                                                          EXEC ;
                                                          DROP ;
                                                          SWAP ;
                                                          DUP ;
                                                          DUG 2 ;
                                                          CAR ;
                                                          SOME ;
                                                          DIG 2 ;
                                                          GET 3 ;
                                                          UPDATE } } } } } ;
                                SWAP ;
                                DROP } ;
                         SWAP ;
                         DIG 2 ;
                         DIG 5 ;
                         DIG 6 ;
                         DROP 4 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         DIG 2 ;
                         CAR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { DIG 3 ;
                     DIG 4 ;
                     DROP 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     SENDER ;
                     DUG 2 ;
                     ITER { SWAP ;
                            { DIP 2 { DUP } ; DIG 3 } ;
                            { DIP 2 { DUP } ; DIG 3 } ;
                            IF_LEFT {} {} ;
                            CAR ;
                            COMPARE ;
                            EQ ;
                            IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                            SWAP ;
                            IF_LEFT
                              { SWAP ;
                                UNIT ;
                                SOME ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                GET 4 ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                GET 3 ;
                                PAIR ;
                                DIG 3 ;
                                CAR ;
                                PAIR ;
                                UPDATE }
                              { DUP ;
                                DUG 2 ;
                                GET 4 ;
                                { DIP 2 { DUP } ; DIG 3 } ;
                                GET 3 ;
                                PAIR ;
                                DIG 2 ;
                                CAR ;
                                PAIR ;
                                NONE unit ;
                                SWAP ;
                                UPDATE } } ;
                     SWAP ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     NIL operation ;
                     PAIR } ;
                 { { DUP ; CAR ; DIP { CDR } } } ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 CAR ;
                 CDR ;
                 DIG 3 ;
                 DIG 4 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 SWAP ;
                 PAIR } }
           { DIG 3 ;
             DIG 4 ;
             DROP 2 ;
             IF_LEFT
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 DIG 3 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 CDR ;
                 IF { PUSH string "FROZEN" ; FAILWITH } {} ;
                 SWAP ;
                 DUP ;
                 CAR ;
                 CAR ;
                 CDR ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CDR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 CAR ;
                 CDR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 DIG 2 ;
                 ITER { DUP ;
                        DUG 2 ;
                        CDR ;
                        ITER { DUP ;
                               DUG 2 ;
                               CAR ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CAR ;
                               CDR ;
                               COMPARE ;
                               GT ;
                               IF { DROP 2 ; PUSH string "USED_TOKEN_ID" ; FAILWITH }
                                  { DUP ;
                                    CDR ;
                                    DIG 2 ;
                                    DUP ;
                                    DUP ;
                                    DUG 4 ;
                                    CAR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PUSH nat 1 ;
                                    { DIP 3 { DUP } ; DIG 4 } ;
                                    CAR ;
                                    ADD ;
                                    DIG 2 ;
                                    CAR ;
                                    CAR ;
                                    { DIP 4 { DUP } ; DIG 5 } ;
                                    CAR ;
                                    DIG 4 ;
                                    CAR ;
                                    SWAP ;
                                    SOME ;
                                    SWAP ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR } } ;
                        SWAP ;
                        DROP } ;
                 DUP ;
                 CAR ;
                 CDR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 PAIR ;
                 DUP ;
                 CDR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 CDR ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 CDR ;
                 DIG 5 ;
                 CAR ;
                 CAR ;
                 CDR ;
                 { { DUP ; CAR ; DIP { CDR } } } ;
                 CDR ;
                 DIG 6 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 CAR ;
                 PAIR ;
                 DIG 3 ;
                 CAR ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 PAIR ;
                 NIL operation ;
                 PAIR }
               { DROP ;
                 DUP ;
                 CAR ;
                 CAR ;
                 CAR ;
                 DIG 2 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 DUP ;
                 CDR ;
                 PUSH bool True ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 CDR ;
                 CAR ;
                 PAIR ;
                 DIG 2 ;
                 CAR ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 NIL operation ;
                 PAIR } } } }
