{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (unit %confirm_admin))
            (or (pair %mint (address %address) (pair (nat %amount) (nat %token_id)))
                (or (pair %mint_new
                       (address %address)
                       (pair (nat %amount)
                             (pair (nat %max_supply) (pair (map %metadata string bytes) (nat %token_id)))))
                    (address %set_administrator))))
        (or (or (pair %set_max_supply (nat %max_supply) (nat %token_id))
                (or (pair %set_metadata (string %k) (bytes %v)) (address %set_minter)))
            (or (bool %set_pause)
                (or (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  storage
    (pair (address %administrator)
          (pair (nat %all_tokens)
                (pair (big_map %ledger (pair address nat) nat)
                      (pair (big_map %max_supply nat nat)
                            (pair (big_map %metadata string bytes)
                                  (pair (address %minter)
                                        (pair (big_map %operators
                                                 (pair (address %owner) (pair (address %operator) (nat %token_id)))
                                                 unit)
                                              (pair (bool %paused)
                                                    (pair (option %pending_admin address)
                                                          (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                                                                (big_map %total_supply nat nat))))))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) unit)
                            (or (pair address (pair nat nat))
                                (or (pair address (pair nat (pair nat (pair (map string bytes) nat)))) address)))
                        (or (or (pair nat nat) (or (pair string bytes) address))
                            (or bool
                                (or (list (pair address (list (pair address (pair nat nat)))))
                                    (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair address
                          (pair nat
                                (pair (big_map (pair address nat) nat)
                                      (pair (big_map nat nat)
                                            (pair (big_map string bytes)
                                                  (pair address
                                                        (pair (big_map (pair address (pair address nat)) unit)
                                                              (pair bool
                                                                    (pair (option address) (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat)))))))))))) ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 15 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { { DIP 2 { DUP } ; DIG 3 } ;
                           GET 19 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           { DIP 2 { DUP } ; DIG 3 } ;
                           GET 5 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { { DIP 2 { DUP } ; DIG 3 } ;
                                GET 5 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 415 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { DROP ;
                     DUP ;
                     GET 17 ;
                     IF_NONE { PUSH string "NO_PENDING_ADMIN" ; FAILWITH } {} ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_PENGING_ADMIN" ; FAILWITH } ;
                     DUP ;
                     GET 17 ;
                     IF_NONE { PUSH string "NO_PENDING_ADMIN" ; FAILWITH } {} ;
                     UPDATE 1 ;
                     NONE address ;
                     UPDATE 17 ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 11 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF { PUSH bool True }
                        { SWAP ; DUP ; DUG 2 ; CAR ; SENDER ; COMPARE ; EQ } ;
                     IF {} { PUSH string "FA2_UNAUTHORIZED" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     COMPARE ;
                     LT ;
                     IF {} { PUSH string "INVALID_TOKEN_ID" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     GET 4 ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     CAR ;
                     PAIR ;
                     MEM ;
                     IF { SWAP ;
                          DUP ;
                          GET 5 ;
                          DUP ;
                          DIG 3 ;
                          DUP ;
                          GET 4 ;
                          SWAP ;
                          DUP ;
                          DUG 5 ;
                          CAR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 576 ; FAILWITH } {} ;
                          { DIP 4 { DUP } ; DIG 5 } ;
                          GET 3 ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          UPDATE 5 ;
                          SWAP }
                        { SWAP ;
                          DUP ;
                          GET 5 ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          GET 3 ;
                          SOME ;
                          DIG 3 ;
                          DUP ;
                          GET 4 ;
                          SWAP ;
                          DUP ;
                          DUG 5 ;
                          CAR ;
                          PAIR ;
                          UPDATE ;
                          UPDATE 5 ;
                          SWAP } ;
                     SWAP ;
                     DUP ;
                     GET 20 ;
                     DUP ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     GET 4 ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH int 580 ; FAILWITH } {} ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     GET 3 ;
                     ADD ;
                     SOME ;
                     SWAP ;
                     UPDATE ;
                     UPDATE 20 ;
                     SWAP ;
                     PUSH nat 0 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 7 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 4 ;
                     GET ;
                     IF_NONE { PUSH int 531 ; FAILWITH } {} ;
                     COMPARE ;
                     GT ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 7 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 4 ;
                          GET ;
                          IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          GET 20 ;
                          DIG 2 ;
                          GET 4 ;
                          GET ;
                          IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                          COMPARE ;
                          LE ;
                          IF {} { PUSH int 532 ; FAILWITH } }
                        { DROP } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         GET 8 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 3 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 3 ;
                         DUP ;
                         PUSH nat 1 ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         GET 8 ;
                         ADD ;
                         DUP ;
                         DUG 2 ;
                         COMPARE ;
                         LE ;
                         IF { DROP } { SWAP ; DROP } ;
                         UPDATE 3 ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         GET 8 ;
                         SWAP ;
                         DUP ;
                         DUG 3 ;
                         CAR ;
                         PAIR ;
                         MEM ;
                         IF { SWAP ;
                              DUP ;
                              GET 5 ;
                              DUP ;
                              DIG 3 ;
                              DUP ;
                              GET 8 ;
                              SWAP ;
                              DUP ;
                              DUG 5 ;
                              CAR ;
                              PAIR ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 550 ; FAILWITH } {} ;
                              { DIP 4 { DUP } ; DIG 5 } ;
                              GET 3 ;
                              ADD ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              UPDATE 5 ;
                              SWAP }
                            { SWAP ;
                              DUP ;
                              GET 5 ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              GET 3 ;
                              SOME ;
                              DIG 3 ;
                              DUP ;
                              GET 8 ;
                              SWAP ;
                              DUP ;
                              DUG 5 ;
                              CAR ;
                              PAIR ;
                              UPDATE ;
                              UPDATE 5 ;
                              SWAP } ;
                         SWAP ;
                         DUP ;
                         GET 19 ;
                         DIG 2 ;
                         DUP ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         DUG 4 ;
                         GET 8 ;
                         PAIR ;
                         SOME ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         GET 8 ;
                         UPDATE ;
                         UPDATE 19 ;
                         DUP ;
                         GET 7 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 5 ;
                         SOME ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         GET 8 ;
                         UPDATE ;
                         UPDATE 7 ;
                         DUP ;
                         GET 20 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 3 ;
                         SOME ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         GET 8 ;
                         UPDATE ;
                         UPDATE 20 ;
                         SWAP ;
                         PUSH nat 0 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 7 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 8 ;
                         GET ;
                         IF_NONE { PUSH int 531 ; FAILWITH } {} ;
                         COMPARE ;
                         GT ;
                         IF { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 7 ;
                              SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 8 ;
                              GET ;
                              IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              GET 20 ;
                              DIG 2 ;
                              GET 8 ;
                              GET ;
                              IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                              COMPARE ;
                              LE ;
                              IF {} { PUSH int 532 ; FAILWITH } }
                            { DROP } }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SOME ;
                         UPDATE 17 } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH int 589 ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 7 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     SOME ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CDR ;
                     UPDATE ;
                     UPDATE 7 ;
                     SWAP ;
                     PUSH nat 0 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 7 ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     GET ;
                     IF_NONE { PUSH int 531 ; FAILWITH } {} ;
                     COMPARE ;
                     GT ;
                     IF { SWAP ;
                          DUP ;
                          DUG 2 ;
                          GET 7 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CDR ;
                          GET ;
                          IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                          { DIP 2 { DUP } ; DIG 3 } ;
                          GET 20 ;
                          DIG 2 ;
                          CDR ;
                          GET ;
                          IF_NONE { PUSH int 532 ; FAILWITH } {} ;
                          COMPARE ;
                          LE ;
                          IF {} { PUSH int 532 ; FAILWITH } }
                        { DROP } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 9 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         SOME ;
                         DIG 3 ;
                         CAR ;
                         UPDATE ;
                         UPDATE 9 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 11 } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 15 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 15 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { SENDER ;
                                       { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True }
                                          { { DIP 3 { DUP } ; DIG 4 } ;
                                            GET 13 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            { DIP 4 { DUP } ; DIG 5 } ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       { DIP 3 { DUP } ; DIG 4 } ;
                                       GET 19 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            { DIP 4 { DUP } ; DIG 5 } ;
                                            GET 5 ;
                                            { DIP 2 { DUP } ; DIG 3 } ;
                                            GET 3 ;
                                            { DIP 4 { DUP } ; DIG 5 } ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 394 ; FAILWITH } {} ;
                                            COMPARE ;
                                            GE ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            { DIP 3 { DUP } ; DIG 4 } ;
                                            DUP ;
                                            GET 5 ;
                                            DUP ;
                                            { DIP 3 { DUP } ; DIG 4 } ;
                                            GET 3 ;
                                            { DIP 5 { DUP } ; DIG 6 } ;
                                            CAR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 2 ;
                                            GET ;
                                            IF_NONE { PUSH int 397 ; FAILWITH } { DROP } ;
                                            { DIP 3 { DUP } ; DIG 4 } ;
                                            GET 4 ;
                                            DIG 7 ;
                                            GET 5 ;
                                            { DIP 5 { DUP } ; DIG 6 } ;
                                            GET 3 ;
                                            { DIP 7 { DUP } ; DIG 8 } ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 398 ; FAILWITH } {} ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE { PUSH int 397 ; FAILWITH } {} ;
                                            SOME ;
                                            SWAP ;
                                            UPDATE ;
                                            UPDATE 5 ;
                                            DUP ;
                                            DUG 4 ;
                                            GET 5 ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 3 ;
                                            CAR ;
                                            PAIR ;
                                            MEM ;
                                            IF { DIG 3 ;
                                                 DUP ;
                                                 GET 5 ;
                                                 DUP ;
                                                 DIG 3 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 5 ;
                                                 CAR ;
                                                 PAIR ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 400 ; FAILWITH } {} ;
                                                 DIG 4 ;
                                                 GET 4 ;
                                                 ADD ;
                                                 SOME ;
                                                 SWAP ;
                                                 UPDATE ;
                                                 UPDATE 5 ;
                                                 DUG 2 }
                                               { DIG 3 ;
                                                 DUP ;
                                                 GET 5 ;
                                                 { DIP 2 { DUP } ; DIG 3 } ;
                                                 GET 4 ;
                                                 SOME ;
                                                 DIG 3 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 CAR ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 UPDATE 5 ;
                                                 DUG 2 } }
                                          { DROP } } ;
                                DROP } ;
                         DROP }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 13 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 13 ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 13 ;
                                    NONE unit ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 13 ;
                                    SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
