{ parameter
    (or (or (or (or %admin (unit %confirm_admin) (address %set_admin))
                (or %assets
                   (pair %balance_of
                      (list %requests (pair (address %owner) (nat %token_id)))
                      (contract %callback
                         (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                   (list %update_operators
                      (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                          (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))))
            (or (list %mint
                   (pair (pair %token_metadata (nat %token_id) (map %token_info string bytes))
                         (address %owner)))
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))))
        (or %update_admin_operator
           (nat %revoke_admin_operation)
           (nat %reinstate_admin_operation))) ;
  storage
    (pair (pair (pair %admin (address %admin) (option %pending_admin address))
                (pair %assets
                   (pair (pair (big_map %admin_operators nat unit) (big_map %ledger nat address))
                         (pair (nat %next_token_id) (big_map %operators (pair address (pair address nat)) unit)))
                   (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))
          (big_map %metadata string bytes)) ;
  code { LAMBDA
           (pair address (option address))
           unit
           { CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         PUSH string "FA2_TOKEN_UNDEFINED" ;
         PUSH string "FA2_INSUFFICIENT_BALANCE" ;
         PUSH string "FA2_NOT_OWNER" ;
         PUSH string "FA2_NOT_OPERATOR" ;
         DUP 4 ;
         DIG 3 ;
         PAIR ;
         LAMBDA
           (pair (pair string string)
                 (pair (pair (list (pair (option address) (list (pair (option address) (pair nat nat)))))
                             (lambda
                                (pair (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                                      (pair address (big_map nat unit)))
                                unit))
                       (pair (pair (pair address (option address))
                                   (pair (pair (pair (big_map nat unit) (big_map nat address))
                                               (pair nat (big_map (pair address (pair address nat)) unit)))
                                         (big_map nat (pair nat (map string bytes)))))
                             (big_map string bytes))))
           (pair (list operation)
                 (pair (pair (pair address (option address))
                             (pair (pair (pair (big_map nat unit) (big_map nat address))
                                         (pair nat (big_map (pair address (pair address nat)) unit)))
                                   (big_map nat (pair nat (map string bytes)))))
                       (big_map string bytes)))
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             UNPAIR ;
             DUP 3 ;
             CAR ;
             CDR ;
             DUP ;
             CAR ;
             CAR ;
             CAR ;
             DUP 5 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             CDR ;
             DIG 2 ;
             CAR ;
             CDR ;
             CDR ;
             PAIR ;
             DIG 3 ;
             DUP 4 ;
             DIG 2 ;
             UNPAIR ;
             DIG 4 ;
             UNPAIR ;
             DIG 3 ;
             DIG 4 ;
             ITER { DUP ;
                    DUG 2 ;
                    CDR ;
                    ITER { SWAP ;
                           DUP 3 ;
                           CAR ;
                           IF_NONE
                             { UNIT }
                             { DUP 6 ;
                               DUP 6 ;
                               PAIR ;
                               DUP 8 ;
                               DUP 5 ;
                               GET 3 ;
                               PAIR ;
                               SENDER ;
                               DIG 3 ;
                               PAIR ;
                               PAIR ;
                               PAIR ;
                               DUP 8 ;
                               SWAP ;
                               EXEC } ;
                           DROP ;
                           PUSH nat 1 ;
                           DUP 3 ;
                           GET 4 ;
                           COMPARE ;
                           GT ;
                           IF { DROP 2 ; DUP 8 ; FAILWITH }
                              { PUSH nat 0 ;
                                DUP 3 ;
                                GET 4 ;
                                COMPARE ;
                                EQ ;
                                IF { DUP ;
                                     DIG 2 ;
                                     GET 3 ;
                                     GET ;
                                     IF_NONE { DROP ; DUP 9 ; FAILWITH } { DROP } }
                                   { SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     GET 3 ;
                                     DUP 4 ;
                                     CAR ;
                                     IF_NONE
                                       { DROP }
                                       { DUP 3 ;
                                         DUP 3 ;
                                         GET ;
                                         IF_NONE
                                           { DROP 3 ; DUP 10 ; FAILWITH }
                                           { COMPARE ;
                                             EQ ;
                                             IF { NONE address ; SWAP ; UPDATE } { DROP 2 ; DUP 9 ; FAILWITH } } } ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     GET 3 ;
                                     DIG 2 ;
                                     CAR ;
                                     IF_NONE { DROP } { DIG 2 ; SWAP ; DIG 2 ; SWAP ; SOME ; SWAP ; UPDATE } } } } ;
                    SWAP ;
                    DROP } ;
             SWAP ;
             DIG 2 ;
             DIG 3 ;
             DIG 4 ;
             DIG 7 ;
             DIG 8 ;
             DROP 6 ;
             DUP 3 ;
             CDR ;
             DUP 4 ;
             CAR ;
             CDR ;
             DUP ;
             CDR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CDR ;
             DIG 4 ;
             DIG 3 ;
             CAR ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             PAIR ;
             DUP 4 ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             DUG 2 ;
             DROP 2 ;
             NIL operation ;
             PAIR } ;
         SWAP ;
         APPLY ;
         DIG 5 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { DIG 2 ;
                 DIG 3 ;
                 DROP 2 ;
                 IF_LEFT
                   { DIG 2 ;
                     DIG 3 ;
                     DROP 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SWAP ;
                     IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         CDR ;
                         IF_NONE
                           { PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; SENDER ; PAIR }
                                { PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SOME ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } ;
                     UNPAIR ;
                     DUP 3 ;
                     CDR ;
                     DIG 3 ;
                     CAR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { DIG 4 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     IF_LEFT
                       { DIG 3 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         MAP { DUP 3 ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CDR ;
                               GET ;
                               IF_NONE
                                 { DROP ; DUP 5 ; FAILWITH }
                                 { SWAP ;
                                   DUP ;
                                   CAR ;
                                   DIG 2 ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH nat 1 } { PUSH nat 0 } ;
                                   SWAP ;
                                   PAIR } } ;
                         DIG 2 ;
                         DIG 5 ;
                         DROP 2 ;
                         SWAP ;
                         CDR ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR }
                       { DIG 4 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CDR ;
                         SWAP ;
                         SENDER ;
                         DUG 2 ;
                         ITER { SWAP ;
                                DUP 3 ;
                                DUP 3 ;
                                IF_LEFT {} {} ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                IF {} { DUP 6 ; FAILWITH } ;
                                SWAP ;
                                IF_LEFT
                                  { SWAP ;
                                    UNIT ;
                                    SOME ;
                                    DUP 3 ;
                                    GET 4 ;
                                    DUP 4 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 3 ;
                                    CAR ;
                                    PAIR ;
                                    UPDATE }
                                  { DUP ;
                                    DUG 2 ;
                                    GET 4 ;
                                    DUP 3 ;
                                    GET 3 ;
                                    PAIR ;
                                    DIG 2 ;
                                    CAR ;
                                    PAIR ;
                                    NONE unit ;
                                    SWAP ;
                                    UPDATE } } ;
                         SWAP ;
                         DIG 4 ;
                         DROP 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         SWAP ;
                         DUP 3 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } ;
                     UNPAIR ;
                     DUP 3 ;
                     CDR ;
                     DIG 2 ;
                     DIG 3 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR } }
               { DIG 4 ;
                 DIG 5 ;
                 DROP 2 ;
                 IF_LEFT
                   { DIG 3 ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     DIG 4 ;
                     SWAP ;
                     EXEC ;
                     DROP ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     NIL (pair (option address) (pair nat nat)) ;
                     PAIR ;
                     SWAP ;
                     ITER { DUP ;
                            DUG 2 ;
                            CAR ;
                            CAR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            CDR ;
                            CAR ;
                            CAR ;
                            CDR ;
                            SWAP ;
                            DUP ;
                            DUG 2 ;
                            MEM ;
                            IF { DROP 3 ; PUSH string "FA2_INVALID_TOKEN_ID" ; FAILWITH }
                               { PUSH nat 1 ;
                                 SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 ADD ;
                                 DUP 3 ;
                                 CDR ;
                                 CDR ;
                                 DUP 5 ;
                                 CAR ;
                                 DUP 4 ;
                                 SWAP ;
                                 SOME ;
                                 SWAP ;
                                 UPDATE ;
                                 DUP 4 ;
                                 CDR ;
                                 CAR ;
                                 PAIR ;
                                 DUP ;
                                 CDR ;
                                 SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 CAR ;
                                 CDR ;
                                 CDR ;
                                 DIG 3 ;
                                 PAIR ;
                                 DIG 2 ;
                                 CAR ;
                                 CAR ;
                                 PAIR ;
                                 PAIR ;
                                 DIG 2 ;
                                 CAR ;
                                 DIG 3 ;
                                 CDR ;
                                 SOME ;
                                 DIG 3 ;
                                 PUSH nat 1 ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR ;
                                 CONS ;
                                 PAIR } } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     DIG 3 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     PAIR ;
                     LAMBDA
                       (pair (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                             (pair address (big_map nat unit)))
                       unit
                       { DROP ; UNIT } ;
                     NIL (pair (option address) (list (pair (option address) (pair nat nat)))) ;
                     NONE address ;
                     DIG 4 ;
                     CAR ;
                     SWAP ;
                     PAIR ;
                     CONS ;
                     PAIR ;
                     PAIR ;
                     EXEC }
                   { DIG 4 ;
                     DROP ;
                     MAP { DUP ;
                           CDR ;
                           MAP { DUP ;
                                 CAR ;
                                 SOME ;
                                 SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 GET 3 ;
                                 DIG 2 ;
                                 GET 4 ;
                                 SWAP ;
                                 PAIR ;
                                 SWAP ;
                                 PAIR } ;
                           SWAP ;
                           CAR ;
                           SOME ;
                           PAIR } ;
                     SWAP ;
                     DIG 3 ;
                     LAMBDA
                       (pair string
                             (pair (pair (pair address address) (pair nat (big_map (pair address (pair address nat)) unit)))
                                   (pair address (big_map nat unit))))
                       unit
                       { UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         UNPAIR ;
                         DIG 2 ;
                         UNPAIR ;
                         DIG 4 ;
                         UNPAIR ;
                         DUP 6 ;
                         DUP 6 ;
                         COMPARE ;
                         EQ ;
                         IF { DROP 7 ; UNIT }
                            { DIG 3 ;
                              DUP 4 ;
                              DUP 7 ;
                              PAIR ;
                              DIG 5 ;
                              PAIR ;
                              MEM ;
                              IF { DROP 5 ; UNIT }
                                 { SWAP ;
                                   DIG 2 ;
                                   MEM ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   DUP 4 ;
                                   COMPARE ;
                                   EQ ;
                                   AND ;
                                   IF { DROP 2 ; FAILWITH }
                                      { SWAP ; COMPARE ; EQ ; IF { DROP ; UNIT } { FAILWITH } } } } } ;
                     SWAP ;
                     APPLY ;
                     DIG 2 ;
                     PAIR ;
                     PAIR ;
                     EXEC } } }
           { DIG 2 ;
             DIG 3 ;
             DIG 5 ;
             DIG 6 ;
             DROP 4 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CDR ;
             DUP 3 ;
             CAR ;
             CAR ;
             CAR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             CDR ;
             PAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CAR ;
             CAR ;
             DIG 3 ;
             DIG 2 ;
             UNPAIR ;
             DIG 2 ;
             IF_LEFT
               { DUG 2 ;
                 DUP 3 ;
                 GET ;
                 IF_NONE
                   { DROP 3 ; DIG 2 ; FAILWITH }
                   { SENDER ;
                     DIG 2 ;
                     COMPARE ;
                     EQ ;
                     SENDER ;
                     DIG 2 ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     IF { DIG 4 ; DROP ; SWAP ; UNIT ; SOME ; DIG 2 ; UPDATE }
                        { DROP 2 ; DIG 2 ; FAILWITH } } }
               { DUG 2 ;
                 PAIR ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET ;
                 IF_NONE
                   { DROP 2 ; DIG 2 ; FAILWITH }
                   { SENDER ;
                     SWAP ;
                     COMPARE ;
                     EQ ;
                     IF { DIG 4 ; DROP ; NONE unit ; SWAP ; UPDATE }
                        { DROP 2 ; DIG 2 ; FAILWITH } } } ;
             DUP 3 ;
             CDR ;
             DUP 3 ;
             CDR ;
             DUP 4 ;
             CAR ;
             CDR ;
             DIG 4 ;
             CAR ;
             CAR ;
             CDR ;
             DIG 4 ;
             PAIR ;
             PAIR ;
             PAIR ;
             DIG 2 ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             NIL operation ;
             PAIR } } }
