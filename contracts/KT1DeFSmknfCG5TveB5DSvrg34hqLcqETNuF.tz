{ storage
    (pair (address %administrator)
          (pair (set %all_tokens nat)
                (pair (bool %enable_burn)
                      (pair (big_map %ledger (pair address nat) nat)
                            (pair (address %locker)
                                  (pair (big_map %metadata string bytes)
                                        (pair (big_map %operators
                                                 (pair (address %owner) (pair (address %operator) (nat %token_id)))
                                                 unit)
                                              (pair (bool %paused)
                                                    (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes))))))))))) ;
  parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (or (nat %burn) (unit %default)))
            (or (pair %mint
                   (address %address)
                   (pair (nat %amount) (pair (map %metadata string bytes) (nat %token_id))))
                (or (pair %mint_lock
                       (address %address)
                       (pair (nat %amount) (pair (map %metadata string bytes) (nat %token_id))))
                    (address %set_administrator))))
        (or (or (pair %set_metadata (string %k) (bytes %v))
                (or (bool %set_pause)
                    (list %transfer
                       (pair (address %from_)
                             (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))))
            (or (bool %update_enable_burn)
                (or (address %update_locker)
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (or nat unit))
                            (or (pair address (pair nat (pair (map string bytes) nat)))
                                (or (pair address (pair nat (pair (map string bytes) nat))) address)))
                        (or (or (pair string bytes) (or bool (list (pair address (list (pair address (pair nat nat)))))))
                            (or bool
                                (or address (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair address
                          (pair (set nat)
                                (pair bool
                                      (pair (big_map (pair address nat) nat)
                                            (pair address
                                                  (pair (big_map string bytes)
                                                        (pair (big_map (pair address (pair address nat)) unit)
                                                              (pair bool (big_map nat (pair nat (map string bytes)))))))))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 15 ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 16 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                GET 7 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 426 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         IF {} { PUSH string "BURN_DISABLED" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 16 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                         PUSH nat 1 ;
                         DUP 3 ;
                         GET 7 ;
                         DUP 3 ;
                         SENDER ;
                         PAIR ;
                         GET ;
                         IF_NONE { PUSH int 569 ; FAILWITH } {} ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         GET 7 ;
                         NONE nat ;
                         DUP 4 ;
                         SENDER ;
                         PAIR ;
                         UPDATE ;
                         UPDATE 7 ;
                         DUP ;
                         GET 16 ;
                         NONE (pair nat (map string bytes)) ;
                         DIG 3 ;
                         UPDATE ;
                         UPDATE 16 }
                       { PUSH string "CANNOT_TRANSFER_TEZOS" ; FAILWITH } ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     GET 3 ;
                     PUSH nat 1 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NFT-asset: amount <> 1" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 6 ;
                     MEM ;
                     IF { PUSH string "NFT-asset: cannot mint twice same token" ; FAILWITH } {} ;
                     SWAP ;
                     DUP ;
                     GET 3 ;
                     PUSH bool True ;
                     DUP 4 ;
                     GET 6 ;
                     UPDATE ;
                     UPDATE 3 ;
                     DUP ;
                     DUG 2 ;
                     GET 7 ;
                     SWAP ;
                     DUP ;
                     GET 6 ;
                     SWAP ;
                     DUP ;
                     DUG 3 ;
                     CAR ;
                     PAIR ;
                     MEM ;
                     IF { SWAP ;
                          DUP ;
                          GET 7 ;
                          DUP ;
                          DIG 3 ;
                          DUP ;
                          GET 6 ;
                          SWAP ;
                          DUP ;
                          DUG 5 ;
                          CAR ;
                          PAIR ;
                          DUP ;
                          DUG 2 ;
                          GET ;
                          IF_NONE { PUSH int 537 ; FAILWITH } {} ;
                          DUP 5 ;
                          GET 3 ;
                          ADD ;
                          SOME ;
                          SWAP ;
                          UPDATE ;
                          UPDATE 7 ;
                          SWAP }
                        { SWAP ;
                          DUP ;
                          GET 7 ;
                          DUP 3 ;
                          GET 3 ;
                          SOME ;
                          DIG 3 ;
                          DUP ;
                          GET 6 ;
                          SWAP ;
                          DUP ;
                          DUG 5 ;
                          CAR ;
                          PAIR ;
                          UPDATE ;
                          UPDATE 7 ;
                          SWAP } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 16 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 6 ;
                     MEM ;
                     IF { DROP }
                        { SWAP ;
                          DUP ;
                          GET 16 ;
                          DIG 2 ;
                          DUP ;
                          GET 5 ;
                          SWAP ;
                          DUP ;
                          DUG 4 ;
                          GET 6 ;
                          PAIR ;
                          SOME ;
                          DIG 3 ;
                          GET 6 ;
                          UPDATE ;
                          UPDATE 16 } ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         GET 3 ;
                         PUSH nat 1 ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NFT-asset: amount <> 1" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         MEM ;
                         IF { PUSH string "NFT-asset: cannot mint twice same token" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         GET 3 ;
                         PUSH bool True ;
                         DUP 4 ;
                         GET 6 ;
                         UPDATE ;
                         UPDATE 3 ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         DUP 4 ;
                         GET 9 ;
                         PAIR ;
                         MEM ;
                         IF { SWAP ;
                              DUP ;
                              DUG 2 ;
                              DUP ;
                              GET 7 ;
                              DUP ;
                              DUP 4 ;
                              GET 6 ;
                              DIG 5 ;
                              GET 9 ;
                              PAIR ;
                              DUP ;
                              DUG 2 ;
                              GET ;
                              IF_NONE { PUSH int 537 ; FAILWITH } {} ;
                              DUP 5 ;
                              GET 3 ;
                              ADD ;
                              SOME ;
                              SWAP ;
                              UPDATE ;
                              UPDATE 7 ;
                              SWAP }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              DUP ;
                              GET 7 ;
                              DUP 3 ;
                              GET 3 ;
                              SOME ;
                              DUP 4 ;
                              GET 6 ;
                              DIG 5 ;
                              GET 9 ;
                              PAIR ;
                              UPDATE ;
                              UPDATE 7 ;
                              SWAP } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 16 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         MEM ;
                         IF {}
                            { SWAP ;
                              DUP ;
                              GET 16 ;
                              DIG 2 ;
                              DUP ;
                              GET 5 ;
                              SWAP ;
                              DUP ;
                              DUG 4 ;
                              GET 6 ;
                              PAIR ;
                              SOME ;
                              DUP 4 ;
                              GET 6 ;
                              UPDATE ;
                              UPDATE 16 ;
                              SWAP } ;
                         NIL operation ;
                         DUP 3 ;
                         GET 9 ;
                         CONTRACT %lock_twitz (pair nat address) ;
                         IF_NONE { PUSH int 561 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         DUP ;
                         CAR ;
                         SWAP ;
                         GET 6 ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 1 ;
                         NIL operation } } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     GET 11 ;
                     DUP 3 ;
                     CDR ;
                     SOME ;
                     DIG 3 ;
                     CAR ;
                     UPDATE ;
                     UPDATE 11 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 15 }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 15 ;
                         IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                         DUP ;
                         ITER { DUP ;
                                CDR ;
                                ITER { SENDER ;
                                       DUP 3 ;
                                       CAR ;
                                       COMPARE ;
                                       EQ ;
                                       IF { PUSH bool True }
                                          { DUP 4 ;
                                            GET 13 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 3 ;
                                            SENDER ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR 3 ;
                                            MEM } ;
                                       IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                       DUP 4 ;
                                       GET 16 ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       GET 3 ;
                                       MEM ;
                                       IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                       DUP ;
                                       GET 4 ;
                                       PUSH nat 0 ;
                                       COMPARE ;
                                       LT ;
                                       IF { DUP ;
                                            GET 4 ;
                                            DUP 5 ;
                                            GET 7 ;
                                            DUP 3 ;
                                            GET 3 ;
                                            DUP 5 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 404 ; FAILWITH } {} ;
                                            COMPARE ;
                                            GE ;
                                            IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                            DUP 4 ;
                                            DUP ;
                                            GET 7 ;
                                            DUP ;
                                            DUP 4 ;
                                            GET 3 ;
                                            DUP 6 ;
                                            CAR ;
                                            PAIR ;
                                            DUP ;
                                            DUG 2 ;
                                            GET ;
                                            IF_NONE { PUSH int 408 ; FAILWITH } { DROP } ;
                                            DUP 4 ;
                                            GET 4 ;
                                            DIG 7 ;
                                            GET 7 ;
                                            DUP 6 ;
                                            GET 3 ;
                                            DUP 8 ;
                                            CAR ;
                                            PAIR ;
                                            GET ;
                                            IF_NONE { PUSH int 408 ; FAILWITH } {} ;
                                            SUB ;
                                            ISNAT ;
                                            IF_NONE { PUSH int 408 ; FAILWITH } {} ;
                                            SOME ;
                                            SWAP ;
                                            UPDATE ;
                                            UPDATE 7 ;
                                            DUP ;
                                            DUG 4 ;
                                            GET 7 ;
                                            SWAP ;
                                            DUP ;
                                            GET 3 ;
                                            SWAP ;
                                            DUP ;
                                            DUG 3 ;
                                            CAR ;
                                            PAIR ;
                                            MEM ;
                                            IF { DIG 3 ;
                                                 DUP ;
                                                 GET 7 ;
                                                 DUP ;
                                                 DIG 3 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 DUP ;
                                                 DUG 5 ;
                                                 CAR ;
                                                 PAIR ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET ;
                                                 IF_NONE { PUSH int 411 ; FAILWITH } {} ;
                                                 DIG 4 ;
                                                 GET 4 ;
                                                 ADD ;
                                                 SOME ;
                                                 SWAP ;
                                                 UPDATE ;
                                                 UPDATE 7 ;
                                                 DUG 2 }
                                               { DIG 3 ;
                                                 DUP ;
                                                 GET 7 ;
                                                 DUP 3 ;
                                                 GET 4 ;
                                                 SOME ;
                                                 DIG 3 ;
                                                 DUP ;
                                                 GET 3 ;
                                                 SWAP ;
                                                 CAR ;
                                                 PAIR ;
                                                 UPDATE ;
                                                 UPDATE 7 ;
                                                 DUG 2 } }
                                          { DROP } } ;
                                DROP } ;
                         DROP } } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 5 }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         UPDATE 9 }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 13 ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 13 ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF {} { PUSH string "FA2_NOT_OWNER" ; FAILWITH } ;
                                    DIG 2 ;
                                    DUP ;
                                    GET 13 ;
                                    NONE unit ;
                                    DIG 3 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    UPDATE 13 ;
                                    SWAP } } ;
                         DROP } } } ;
             NIL operation } ;
         PAIR } }
