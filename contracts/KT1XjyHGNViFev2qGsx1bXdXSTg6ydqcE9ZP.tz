{ parameter
    (or (or %gameplay
           (list %create_games
              (pair (nat %game_id) (pair (mutez %entry_fee) (mutez %game_authority_fee))))
           (or (nat %join_game)
               (or (list %submit_winners (pair (nat %game_id) (nat %game_result)))
                   (list %cancel_games nat))))
        (or (address %set_game_authority)
            (or (unit %confirm_game_authority) (or (address %set_fee_collector) (unit %lock))))) ;
  storage
    (pair (big_map %metadata string bytes)
          (pair (big_map %games
                   nat
                   (pair (mutez %entry_fee)
                         (pair (mutez %game_authority_fee)
                               (pair (option %player1 address) (pair (option %player2 address) (nat %progress))))))
                (pair (address %fee_collector)
                      (pair (address %game_authority)
                            (pair (address %game_authority_candidate) (bool %locked)))))) ;
  code { { { DUP ; CAR ; DIP { CDR } } } ;
         PUSH string "LOCKED" ;
         { DIP 2 { DUP } ; DIG 3 } ;
         GET 10 ;
         IF { FAILWITH } { DROP } ;
         IF_LEFT
           { SWAP ;
             DUP ;
             DUG 2 ;
             SWAP ;
             IF_LEFT
               { PUSH string "NOT_GA" ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 GET 7 ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { FAILWITH } { DROP } ;
                 PUSH string "XTZ_RECEIVED" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 NEQ ;
                 IF { FAILWITH } { DROP } ;
                 SWAP ;
                 GET 3 ;
                 SWAP ;
                 ITER { DUP ;
                        DUG 2 ;
                        GET 4 ;
                        { DIP 2 { DUP } ; DIG 3 } ;
                        GET 3 ;
                        SWAP ;
                        DUP ;
                        DUG 2 ;
                        COMPARE ;
                        GT ;
                        IF { PUSH string "LOW_ENTRY_FEE" ; FAILWITH }
                           { PUSH mutez 0 ;
                             PUSH mutez 2 ;
                             { DIP 2 { DUP } ; DIG 3 } ;
                             EDIV ;
                             IF_NONE { PUSH string "MOD by 0" ; FAILWITH } {} ;
                             CDR ;
                             COMPARE ;
                             EQ ;
                             NOT ;
                             IF { PUSH string "ODD_GA_FEE" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  { DIP 3 { DUP } ; DIG 4 } ;
                                  CAR ;
                                  MEM ;
                                  IF { { DIP 2 { DUP } ; DIG 3 } ;
                                       CAR ;
                                       PUSH string "INVALID_GAME_ID" ;
                                       PAIR ;
                                       FAILWITH }
                                     {} } } ;
                        SWAP ;
                        { DIP 2 { DUP } ; DIG 3 } ;
                        GET 3 ;
                        DIG 2 ;
                        NONE address ;
                        NONE address ;
                        PUSH nat 0 ;
                        SWAP ;
                        PAIR ;
                        SWAP ;
                        PAIR ;
                        SWAP ;
                        PAIR ;
                        SWAP ;
                        PAIR ;
                        DIG 2 ;
                        CAR ;
                        SWAP ;
                        SOME ;
                        SWAP ;
                        UPDATE } ;
                 NIL operation ;
                 PAIR }
               { IF_LEFT
                   { SWAP ;
                     GET 3 ;
                     DUP ;
                     DUG 2 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET ;
                     IF_NONE { PUSH string "GAME_NOT_FOUND" ; FAILWITH } {} ;
                     DUP ;
                     GET 8 ;
                     PUSH nat 0 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     EQ ;
                     IF { DROP ;
                          PUSH string "WRONG_ENTRY_FEE" ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          CAR ;
                          AMOUNT ;
                          COMPARE ;
                          NEQ ;
                          IF { FAILWITH } { DROP } ;
                          SENDER ;
                          SOME ;
                          UPDATE 5 ;
                          PUSH nat 1 ;
                          UPDATE 8 }
                        { PUSH nat 1 ;
                          SWAP ;
                          DUP ;
                          DUG 2 ;
                          COMPARE ;
                          EQ ;
                          IF { DROP ;
                               PUSH string "WRONG_ENTRY_FEE" ;
                               SWAP ;
                               DUP ;
                               DUG 2 ;
                               CAR ;
                               AMOUNT ;
                               COMPARE ;
                               NEQ ;
                               IF { FAILWITH } { DROP } ;
                               SENDER ;
                               SOME ;
                               UPDATE 7 ;
                               PUSH nat 2 ;
                               UPDATE 8 }
                             { SWAP ; DROP ; PUSH string "GAME_PROGRESS" ; PAIR ; FAILWITH } } ;
                     DIG 2 ;
                     SWAP ;
                     SOME ;
                     DIG 2 ;
                     UPDATE ;
                     NIL operation ;
                     PAIR }
                   { IF_LEFT
                       { PUSH string "NOT_GA" ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 7 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         PUSH string "XTZ_RECEIVED" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 5 ;
                         DIG 2 ;
                         GET 3 ;
                         NIL operation ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         ITER { SWAP ;
                                DUP ;
                                CAR ;
                                CAR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                CDR ;
                                DIG 2 ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                CAR ;
                                GET ;
                                IF_NONE { PUSH string "GAME_NOT_FOUND" ; FAILWITH } {} ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                CAR ;
                                PUSH nat 2 ;
                                MUL ;
                                SUB ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                GET 3 ;
                                PUSH nat 2 ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                GET 8 ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH mutez 0 ;
                                     { DIP 3 { DUP } ; DIG 4 } ;
                                     CAR ;
                                     COMPARE ;
                                     EQ ;
                                     IF { DROP 2 ; DIG 3 }
                                        { PUSH mutez 0 ;
                                          SWAP ;
                                          DUP ;
                                          DUG 2 ;
                                          COMPARE ;
                                          EQ ;
                                          IF { DIG 5 }
                                             { DIG 5 ;
                                               SWAP ;
                                               DUP ;
                                               DUG 2 ;
                                               { DIP 5 { DUP } ; DIG 6 } ;
                                               DUP ;
                                               CONTRACT unit ;
                                               IF_NONE
                                                 { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                 { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                               CONS } ;
                                          { DIP 3 { DUP } ; DIG 4 } ;
                                          GET 5 ;
                                          IF_NONE { PUSH string "MISS_PLAYER" ; FAILWITH } {} ;
                                          { DIP 4 { DUP } ; DIG 5 } ;
                                          GET 7 ;
                                          IF_NONE { PUSH string "MISS_PLAYER" ; FAILWITH } {} ;
                                          PUSH nat 1 ;
                                          { DIP 9 { DUP } ; DIG 10 } ;
                                          CDR ;
                                          COMPARE ;
                                          EQ ;
                                          IF { DIG 3 ;
                                               DROP 2 ;
                                               SWAP ;
                                               DUG 2 ;
                                               DUP ;
                                               CONTRACT unit ;
                                               IF_NONE
                                                 { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                 { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                               CONS }
                                             { PUSH nat 2 ;
                                               { DIP 9 { DUP } ; DIG 10 } ;
                                               CDR ;
                                               COMPARE ;
                                               EQ ;
                                               IF { SWAP ;
                                                    DIG 3 ;
                                                    DROP 2 ;
                                                    SWAP ;
                                                    DUG 2 ;
                                                    DUP ;
                                                    CONTRACT unit ;
                                                    IF_NONE
                                                      { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                      { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                                    CONS }
                                                  { PUSH nat 0 ;
                                                    { DIP 9 { DUP } ; DIG 10 } ;
                                                    CDR ;
                                                    COMPARE ;
                                                    EQ ;
                                                    IF { DIG 3 ;
                                                         DROP ;
                                                         DIG 2 ;
                                                         PUSH nat 2 ;
                                                         { DIP 4 { DUP } ; DIG 5 } ;
                                                         EDIV ;
                                                         IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                                         CAR ;
                                                         DIG 2 ;
                                                         DUP ;
                                                         CONTRACT unit ;
                                                         IF_NONE
                                                           { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                           { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                                         CONS ;
                                                         PUSH nat 2 ;
                                                         DIG 3 ;
                                                         EDIV ;
                                                         IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                                         CAR ;
                                                         DIG 2 ;
                                                         DUP ;
                                                         CONTRACT unit ;
                                                         IF_NONE
                                                           { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                           { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                                         CONS }
                                                       { DROP 2 ;
                                                         PUSH nat 3 ;
                                                         { DIP 7 { DUP } ; DIG 8 } ;
                                                         CDR ;
                                                         COMPARE ;
                                                         EQ ;
                                                         IF { DROP ;
                                                              NIL operation ;
                                                              DUG 2 ;
                                                              ADD ;
                                                              { DIP 3 { DUP } ; DIG 4 } ;
                                                              DUP ;
                                                              CONTRACT unit ;
                                                              IF_NONE
                                                                { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                                { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                                              CONS }
                                                            { SWAP ; DIG 2 ; DROP 2 ; PUSH string "NOT_RESULT" ; FAILWITH } } } } } }
                                   { SWAP ;
                                     DIG 5 ;
                                     DROP 3 ;
                                     DUP ;
                                     GET 8 ;
                                     PUSH string "GAME_PROGRESS" ;
                                     PAIR ;
                                     FAILWITH } ;
                                SWAP ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                CDR ;
                                PUSH nat 3 ;
                                ADD ;
                                UPDATE 8 ;
                                DIG 2 ;
                                DIG 3 ;
                                DIG 2 ;
                                SOME ;
                                DIG 4 ;
                                CAR ;
                                UPDATE ;
                                DIG 2 ;
                                PAIR ;
                                PAIR } ;
                         CAR }
                       { PUSH string "NOT_GA" ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 7 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         PUSH string "XTZ_RECEIVED" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         SWAP ;
                         GET 3 ;
                         NIL operation ;
                         PAIR ;
                         SWAP ;
                         ITER { SWAP ;
                                DUP ;
                                CAR ;
                                SWAP ;
                                CDR ;
                                DUP ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                GET ;
                                IF_NONE { PUSH string "GAME_NOT_FOUND" ; FAILWITH } {} ;
                                DUP ;
                                GET 8 ;
                                PUSH nat 0 ;
                                SWAP ;
                                DUP ;
                                DUG 2 ;
                                COMPARE ;
                                EQ ;
                                PUSH mutez 0 ;
                                { DIP 3 { DUP } ; DIG 4 } ;
                                CAR ;
                                COMPARE ;
                                EQ ;
                                OR ;
                                IF { DROP ; DIG 2 }
                                   { PUSH nat 1 ;
                                     SWAP ;
                                     DUP ;
                                     DUG 2 ;
                                     COMPARE ;
                                     EQ ;
                                     IF { DROP ;
                                          DIG 2 ;
                                          SWAP ;
                                          DUP ;
                                          DUG 2 ;
                                          CAR ;
                                          { DIP 2 { DUP } ; DIG 3 } ;
                                          GET 5 ;
                                          IF_NONE { PUSH string "MISS_PLAYER" ; FAILWITH } {} ;
                                          DUP ;
                                          CONTRACT unit ;
                                          IF_NONE
                                            { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                            { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                          CONS }
                                        { PUSH nat 2 ;
                                          SWAP ;
                                          DUP ;
                                          DUG 2 ;
                                          COMPARE ;
                                          EQ ;
                                          IF { DROP ;
                                               DIG 2 ;
                                               SWAP ;
                                               DUP ;
                                               DUG 2 ;
                                               CAR ;
                                               { DIP 2 { DUP } ; DIG 3 } ;
                                               GET 7 ;
                                               IF_NONE { PUSH string "MISS_PLAYER" ; FAILWITH } {} ;
                                               DUP ;
                                               CONTRACT unit ;
                                               IF_NONE
                                                 { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                 { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                               CONS ;
                                               SWAP ;
                                               DUP ;
                                               DUG 2 ;
                                               CAR ;
                                               { DIP 2 { DUP } ; DIG 3 } ;
                                               GET 5 ;
                                               IF_NONE { PUSH string "MISS_PLAYER" ; FAILWITH } {} ;
                                               DUP ;
                                               CONTRACT unit ;
                                               IF_NONE
                                                 { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                                 { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                                               CONS }
                                             { DIG 3 ; DROP ; PUSH string "GAME_PROGRESS" ; PAIR ; FAILWITH } } } ;
                                DUG 2 ;
                                PUSH nat 7 ;
                                UPDATE 8 ;
                                SOME ;
                                DIG 3 ;
                                UPDATE ;
                                SWAP ;
                                PAIR } } } } ;
             { { DUP ; CAR ; DIP { CDR } } } ;
             DUG 2 ;
             UPDATE 3 ;
             SWAP ;
             PAIR }
           { IF_LEFT
               { PUSH string "NOT_GA" ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 GET 7 ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { FAILWITH } { DROP } ;
                 PUSH string "XTZ_RECEIVED" ;
                 PUSH mutez 0 ;
                 AMOUNT ;
                 COMPARE ;
                 NEQ ;
                 IF { FAILWITH } { DROP } ;
                 UPDATE 9 ;
                 NIL operation ;
                 PAIR }
               { IF_LEFT
                   { DROP ;
                     PUSH string "NOT_GA_CANDIDATE" ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 9 ;
                     SENDER ;
                     COMPARE ;
                     NEQ ;
                     IF { FAILWITH } { DROP } ;
                     PUSH string "XTZ_RECEIVED" ;
                     PUSH mutez 0 ;
                     AMOUNT ;
                     COMPARE ;
                     NEQ ;
                     IF { FAILWITH } { DROP } ;
                     DUP ;
                     GET 9 ;
                     UPDATE 7 ;
                     NIL operation ;
                     PAIR }
                   { IF_LEFT
                       { PUSH string "NOT_GA" ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 7 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         PUSH string "XTZ_RECEIVED" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         UPDATE 5 ;
                         NIL operation ;
                         PAIR }
                       { DROP ;
                         PUSH string "NOT_GA" ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 7 ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         PUSH string "XTZ_RECEIVED" ;
                         PUSH mutez 0 ;
                         AMOUNT ;
                         COMPARE ;
                         NEQ ;
                         IF { FAILWITH } { DROP } ;
                         BALANCE ;
                         PUSH mutez 0 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         COMPARE ;
                         EQ ;
                         IF { DROP ; NIL operation }
                            { NIL operation ;
                              SWAP ;
                              { DIP 2 { DUP } ; DIG 3 } ;
                              GET 5 ;
                              DUP ;
                              CONTRACT unit ;
                              IF_NONE
                                { SWAP ; DROP ; PUSH string "CANT_TRANSFER" ; PAIR ; FAILWITH }
                                { SWAP ; DROP ; SWAP ; UNIT ; TRANSFER_TOKENS } ;
                              CONS } ;
                         SWAP ;
                         PUSH bool True ;
                         UPDATE 10 ;
                         SWAP ;
                         PAIR } } } } } }
