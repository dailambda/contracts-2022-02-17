{ parameter
    (or (or (or (or (nat %acceptInvitation) (unit %addAddressToWhitelist))
                (or (pair %addIpfsLink nat bytes) (unit %mint)))
            (or (or (unit %ownerWithdraw) (bool %setIsMintAllowed))
                (or (mutez %setMintCost) (address %setOwnerAddress))))
        (bool %setWhiteListMintAvailable)) ;
  storage
    (pair (pair (pair (big_map %ipfsStorage nat bytes)
                      (pair %mintProperties
                         (pair (nat %collection_id) (nat %editions))
                         (pair (bool %isMintAllowed) (mutez %mintCost))))
                (pair (address %ownerAddress)
                      (pair %statisticProperties (nat %totalNftMinted) (nat %whitelistCounter))))
          (pair %whiteListProperties
             (pair (set %whiteList address) (bool %whiteListMintAvailable))
             (nat %whiteListSize))) ;
  code { { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         PUSH address "KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM" ;
                         CONTRACT %accept_invitation nat ;
                         IF_NONE
                           { PUSH string "not a correct contract for mint a NFT" ; FAILWITH }
                           {} ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR }
                       { DROP ;
                         DUP ;
                         CDR ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CAR ;
                         SIZE ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH string "Whitelist is full." ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         MEM ;
                         IF { PUSH string "Your address is already added to the whitelist." ; FAILWITH }
                            {} ;
                         DUP ;
                         CDR ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         CAR ;
                         CAR ;
                         PUSH bool True ;
                         SENDER ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         DUP ;
                         CDR ;
                         PUSH nat 1 ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         ADD ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { IF_LEFT
                       { { { DUP ; CAR ; DIP { CDR } } } ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         MEM ;
                         IF { PUSH string "Key already exists." ; FAILWITH } {} ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CDR ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         DIG 5 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         DIG 5 ;
                         DIG 5 ;
                         SWAP ;
                         SOME ;
                         SWAP ;
                         UPDATE ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR }
                       { DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         IF {} { PUSH string "Mint is allowed yet." ; FAILWITH } ;
                         DUP ;
                         CDR ;
                         CAR ;
                         CDR ;
                         IF { DUP ;
                              CDR ;
                              CAR ;
                              CAR ;
                              SENDER ;
                              MEM ;
                              IF { PUSH unit Unit }
                                 { PUSH string "Only whitelist user are able to mint" ; FAILWITH } }
                            { PUSH unit Unit } ;
                         DROP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         AMOUNT ;
                         COMPARE ;
                         GE ;
                         IF {}
                            { PUSH string "The amount you pay is not enough to mint a NFT." ; FAILWITH } ;
                         PUSH address "KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM" ;
                         CONTRACT %mint_artist
                           (pair (pair (nat %collection_id) (nat %editions))
                                 (pair (bytes %metadata_cid) (address %target))) ;
                         IF_NONE
                           { PUSH string "not a correct contract for mint a NFT" ; FAILWITH }
                           {} ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         GET ;
                         IF_NONE { PUSH string "Received empty record from big_map" ; FAILWITH } {} ;
                         SENDER ;
                         SWAP ;
                         PAIR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CAR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PUSH mutez 0 ;
                         DIG 2 ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CDR ;
                         PUSH nat 1 ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         CAR ;
                         ADD ;
                         PAIR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR } } }
               { IF_LEFT
                   { IF_LEFT
                       { DROP ;
                         DUP ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         SENDER ;
                         CONTRACT unit ;
                         IF_NONE { PUSH string "Address does not exist." ; FAILWITH } {} ;
                         BALANCE ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         SWAP ;
                         NIL operation ;
                         DIG 2 ;
                         CONS ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         DUP ;
                         CDR ;
                         CDR ;
                         DIG 4 ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         DIG 3 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         PAIR ;
                         SWAP ;
                         CAR ;
                         PAIR ;
                         DIG 3 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "Access denied." ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         CAR ;
                         CDR ;
                         CDR ;
                         DIG 2 ;
                         PAIR ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR ;
                         NIL operation ;
                         PAIR } } } }
           { SWAP ;
             DUP ;
             DUG 2 ;
             CAR ;
             CDR ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF {} { PUSH string "Access denied." ; FAILWITH } ;
             SWAP ;
             DUP ;
             DUG 2 ;
             CDR ;
             DUP ;
             CDR ;
             DUG 2 ;
             CAR ;
             CAR ;
             PAIR ;
             PAIR ;
             SWAP ;
             CAR ;
             PAIR ;
             NIL operation ;
             PAIR } } }
