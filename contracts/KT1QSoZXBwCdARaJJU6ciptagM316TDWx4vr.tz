{ storage
    (pair (pair (address %admin) (mutez %cost))
          (pair (address %idstore)
                (pair (set %kycPlatforms string) (map %updateProofCache address (map string string))))) ;
  parameter
    (or (or (or (or (unit %default) (unit %enableKYC))
                (or (string %enableKYCPlatform) (string %registerProof)))
            (or (or (address %removeIdentity)
                    (pair %removeProof (address %address) (string %prooftype)))
                (or (pair %renameProof
                       (address %address)
                       (pair (string %newProofType) (string %oldProofType)))
                    (or (pair %send (mutez %amount) (address %receiverAddress)) (address %setAdmin)))))
        (or (or (or (option %setBaker key_hash) (mutez %setCost))
                (or (set %setKycPlatforms string)
                    (or (pair %setProofMeta
                           (pair (address %address) (string %key))
                           (pair (string %prooftype) (string %value)))
                        (address %setStore))))
            (or (or (address %setStoreAdmin) (option %setStoreBaker key_hash))
                (or (pair %storeSend (mutez %amount) (address %receiverAddress))
                    (or (pair %updateProofCallback
                           (address %address)
                           (map %proofs
                              string
                              (pair (map %meta string string) (pair (timestamp %register_date) (bool %verified)))))
                        (pair %verifyProof (address %address) (string %prooftype))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { DROP ; NIL operation }
                       { DROP ;
                         DUP ;
                         GET 6 ;
                         PUSH (option (map string string)) (Some { Elt "operation" "kyc" ; Elt "prooftype" "gov" }) ;
                         SENDER ;
                         UPDATE ;
                         UPDATE 6 ;
                         DUP ;
                         GET 3 ;
                         CONTRACT %getProofs (pair address address) ;
                         IF_NONE { PUSH int 207 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         SELF %updateProofCallback ;
                         ADDRESS ;
                         SENDER ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { PUSH bool False ;
                         DUP 3 ;
                         GET 5 ;
                         DUP 3 ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "KYC platform not supported" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         GET 6 ;
                         PUSH (map string string) { Elt "operation" "kycplatform" ; Elt "prooftype" "gov" } ;
                         DIG 3 ;
                         SOME ;
                         PUSH string "platform" ;
                         UPDATE ;
                         SOME ;
                         SENDER ;
                         UPDATE ;
                         UPDATE 6 ;
                         DUP ;
                         GET 3 ;
                         CONTRACT %getProofs (pair address address) ;
                         IF_NONE { PUSH int 220 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         SELF %updateProofCallback ;
                         ADDRESS ;
                         SENDER ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CDR ;
                         AMOUNT ;
                         COMPARE ;
                         LT ;
                         IF { PUSH string "Amount too low" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         GET 6 ;
                         EMPTY_MAP string string ;
                         DIG 3 ;
                         SOME ;
                         PUSH string "prooftype" ;
                         UPDATE ;
                         PUSH (option string) (Some "register") ;
                         PUSH string "operation" ;
                         UPDATE ;
                         SOME ;
                         SENDER ;
                         UPDATE ;
                         UPDATE 6 ;
                         DUP ;
                         GET 3 ;
                         CONTRACT %getProofs (pair address address) ;
                         IF_NONE { PUSH int 197 ; FAILWITH } {} ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         SELF %updateProofCallback ;
                         ADDRESS ;
                         SENDER ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can removeIdentity" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %removeIdentity address ;
                         IF_NONE { PUSH int 332 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can removeProof" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %delProof (pair address string) ;
                         IF_NONE { PUSH int 325 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can renameProof" ; FAILWITH } {} ;
                         SWAP ;
                         DUP ;
                         GET 6 ;
                         EMPTY_MAP string string ;
                         DUP 4 ;
                         GET 4 ;
                         SOME ;
                         PUSH string "prooftype" ;
                         UPDATE ;
                         DUP 4 ;
                         GET 3 ;
                         SOME ;
                         PUSH string "newtype" ;
                         UPDATE ;
                         PUSH (option string) (Some "rename") ;
                         PUSH string "operation" ;
                         UPDATE ;
                         SOME ;
                         DUP 4 ;
                         CAR ;
                         UPDATE ;
                         UPDATE 6 ;
                         SWAP ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %getProofs (pair address address) ;
                         IF_NONE { PUSH int 318 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         SELF %updateProofCallback ;
                         ADDRESS ;
                         DIG 4 ;
                         CAR ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only admin can send" ; FAILWITH } {} ;
                             DUP ;
                             CDR ;
                             CONTRACT unit ;
                             IF_NONE { PUSH int 147 ; FAILWITH } {} ;
                             NIL operation ;
                             SWAP ;
                             DIG 2 ;
                             CAR ;
                             UNIT ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only admin can setAdmin" ; FAILWITH } {} ;
                             SWAP ;
                             UNPAIR ;
                             CDR ;
                             DIG 2 ;
                             PAIR ;
                             PAIR ;
                             NIL operation } } } } }
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can setBaker" ; FAILWITH } {} ;
                         SET_DELEGATE ;
                         NIL operation ;
                         SWAP ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can setCost" ; FAILWITH } {} ;
                         SWAP ;
                         UNPAIR ;
                         CAR ;
                         DIG 2 ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         NIL operation } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can setKycPlatforms" ; FAILWITH } {} ;
                         UPDATE 5 ;
                         NIL operation }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only admin can setProofMeta" ; FAILWITH } {} ;
                             SWAP ;
                             DUP ;
                             GET 6 ;
                             EMPTY_MAP string string ;
                             DUP 4 ;
                             GET 3 ;
                             SOME ;
                             PUSH string "prooftype" ;
                             UPDATE ;
                             PUSH (option string) (Some "meta") ;
                             PUSH string "operation" ;
                             UPDATE ;
                             DUP 4 ;
                             CAR ;
                             CDR ;
                             SOME ;
                             PUSH string "key" ;
                             UPDATE ;
                             DUP 4 ;
                             GET 4 ;
                             SOME ;
                             PUSH string "value" ;
                             UPDATE ;
                             SOME ;
                             DUP 4 ;
                             CAR ;
                             CAR ;
                             UPDATE ;
                             UPDATE 6 ;
                             SWAP ;
                             NIL operation ;
                             DUP 3 ;
                             GET 3 ;
                             CONTRACT %getProofs (pair address address) ;
                             IF_NONE { PUSH int 305 ; FAILWITH } {} ;
                             PUSH mutez 0 ;
                             SELF %updateProofCallback ;
                             ADDRESS ;
                             DIG 4 ;
                             CAR ;
                             CAR ;
                             PAIR ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only admin can setStore" ; FAILWITH } {} ;
                             UPDATE 3 ;
                             NIL operation } } } }
               { IF_LEFT
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can setStoreAdmin" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %setAdmin address ;
                         IF_NONE { PUSH int 168 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can setStoreBaker" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %setBaker (option key_hash) ;
                         IF_NONE { PUSH int 175 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS } }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         NEQ ;
                         IF { PUSH string "Only admin can storeSend" ; FAILWITH } {} ;
                         NIL operation ;
                         DUP 3 ;
                         GET 3 ;
                         CONTRACT %send (pair mutez address) ;
                         IF_NONE { PUSH int 182 ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only idstore can call getProofsCallback" ; FAILWITH } {} ;
                             PUSH bool False ;
                             DUP 3 ;
                             GET 6 ;
                             DUP 3 ;
                             CAR ;
                             MEM ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH string "No cache entry for address" ; FAILWITH } {} ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 6 ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             GET ;
                             IF_NONE { PUSH int 231 ; FAILWITH } {} ;
                             PUSH bool False ;
                             NOW ;
                             EMPTY_MAP string string ;
                             PAIR 3 ;
                             DUP 3 ;
                             CDR ;
                             DUP 3 ;
                             PUSH string "prooftype" ;
                             GET ;
                             IF_NONE { PUSH int 232 ; FAILWITH } {} ;
                             MEM ;
                             IF { DROP ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  CDR ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  PUSH string "prooftype" ;
                                  GET ;
                                  IF_NONE { PUSH int 232 ; FAILWITH } {} ;
                                  GET ;
                                  IF_NONE { PUSH int 242 ; FAILWITH } {} }
                                {} ;
                             PUSH bool False ;
                             PUSH (set string) { "kyc" ; "kycplatform" ; "meta" ; "register" ; "rename" ; "verify" } ;
                             DUP 4 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             MEM ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH string "Unsupported updateProof operation" ; FAILWITH } {} ;
                             PUSH bool False ;
                             DUP 4 ;
                             CDR ;
                             DUP 4 ;
                             PUSH string "prooftype" ;
                             GET ;
                             IF_NONE { PUSH int 232 ; FAILWITH } {} ;
                             MEM ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH string "register" ;
                                  DUP 3 ;
                                  PUSH string "operation" ;
                                  GET ;
                                  IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { PUSH string "Cannot update non-existing proof" ; FAILWITH } {} }
                                {} ;
                             PUSH string "register" ;
                             DUP 3 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool False ; UPDATE 4 ; NOW ; UPDATE 3 } {} ;
                             PUSH string "verify" ;
                             DUP 3 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { PUSH bool True ; UPDATE 4 } {} ;
                             PUSH string "kyc" ;
                             DUP 3 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { DUP ;
                                  CAR ;
                                  PUSH (option string) (Some "true") ;
                                  PUSH string "kyc" ;
                                  UPDATE ;
                                  UPDATE 1 ;
                                  PUSH bool False ;
                                  UPDATE 4 }
                                {} ;
                             PUSH string "kycplatform" ;
                             DUP 3 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { DUP ;
                                  CAR ;
                                  PUSH (option string) (Some "true") ;
                                  DUP 4 ;
                                  PUSH string "platform" ;
                                  GET ;
                                  IF_NONE { PUSH int 265 ; FAILWITH } {} ;
                                  UPDATE ;
                                  UPDATE 1 }
                                {} ;
                             PUSH string "meta" ;
                             DUP 3 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { DUP ;
                                  CAR ;
                                  DUP 3 ;
                                  PUSH string "value" ;
                                  GET ;
                                  IF_NONE { PUSH int 270 ; FAILWITH } {} ;
                                  SOME ;
                                  DUP 4 ;
                                  PUSH string "key" ;
                                  GET ;
                                  IF_NONE { PUSH int 269 ; FAILWITH } {} ;
                                  UPDATE ;
                                  UPDATE 1 }
                                {} ;
                             SWAP ;
                             DUP ;
                             DUG 2 ;
                             PUSH string "prooftype" ;
                             GET ;
                             IF_NONE { PUSH int 232 ; FAILWITH } {} ;
                             PUSH string "rename" ;
                             DUP 4 ;
                             PUSH string "operation" ;
                             GET ;
                             IF_NONE { PUSH int 233 ; FAILWITH } {} ;
                             COMPARE ;
                             EQ ;
                             IF { DROP ;
                                  SWAP ;
                                  PUSH string "newtype" ;
                                  GET ;
                                  IF_NONE { PUSH int 276 ; FAILWITH } {} }
                                { DIG 2 ; DROP } ;
                             DIG 3 ;
                             DUP ;
                             GET 6 ;
                             NONE (map string string) ;
                             DUP 6 ;
                             CAR ;
                             UPDATE ;
                             UPDATE 6 ;
                             DUG 3 ;
                             NIL operation ;
                             DUP 5 ;
                             GET 3 ;
                             CONTRACT %setProof
                               (pair address (pair (pair (map string string) (pair timestamp bool)) string)) ;
                             IF_NONE { PUSH int 279 ; FAILWITH } {} ;
                             PUSH mutez 0 ;
                             DIG 3 ;
                             DIG 4 ;
                             DIG 5 ;
                             CAR ;
                             PAIR 3 ;
                             TRANSFER_TOKENS ;
                             CONS }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             CAR ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { PUSH string "Only admin can verifyProof" ; FAILWITH } {} ;
                             SWAP ;
                             DUP ;
                             GET 6 ;
                             EMPTY_MAP string string ;
                             DUP 4 ;
                             CDR ;
                             SOME ;
                             PUSH string "prooftype" ;
                             UPDATE ;
                             PUSH (option string) (Some "verify") ;
                             PUSH string "operation" ;
                             UPDATE ;
                             SOME ;
                             DUP 4 ;
                             CAR ;
                             UPDATE ;
                             UPDATE 6 ;
                             SWAP ;
                             NIL operation ;
                             DUP 3 ;
                             GET 3 ;
                             CONTRACT %getProofs (pair address address) ;
                             IF_NONE { PUSH int 291 ; FAILWITH } {} ;
                             PUSH mutez 0 ;
                             SELF %updateProofCallback ;
                             ADDRESS ;
                             DIG 4 ;
                             CAR ;
                             PAIR ;
                             TRANSFER_TOKENS ;
                             CONS } } } } } ;
         PAIR } }
