#!/bin/sh

cat *.tz | tr ' ' '\n' | sed -e 's/[()]//g' | sort | uniq -c | sort -r -n
